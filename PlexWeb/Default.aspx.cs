﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool bolField = false;
        string strName = "";
        string strType = "";
        int intTop = 0;
        int intLeft = 0;
        int intWidth = 0;
        int intHeight = 0;
        string strValue = "";
        string[] astrParts = { };
        string strOutput = "";
        string strFontName = "";
        int intFontSize = 0;

        // read the form
        List<string> lstRecords = clsFormRoutines.ReadFile(".\\App_Data\\iFloorDealer.frm");

        List<string> lstXMLRecords = clsFormRoutines.ConvertXMLToNameValuePairs(lstRecords);

        // walk the xml records and create the 
        foreach (string strXMLRecord in lstXMLRecords)
        {

            astrParts = (strXMLRecord + "=").Split('=');

            switch (astrParts[0].ToUpper())
            {
                case "FIELD":
                    bolField = true;
                    strOutput = "";
                    strName = "";
                    strType = "";
                    intTop = 0;
                    intLeft = 0;
                    intWidth = 0;
                    intHeight = 0;
                    strValue = "";
                    strFontName = "";
                    intFontSize = 0;
                    break;

                case "/FIELD":
                    bolField = false;
                    // print the text
                    // display a text 
                    strOutput = "<div ";
                    strOutput = strOutput + "style=\"position: absolute; ";
                    strOutput = strOutput + "left: " + intLeft + "px;";
                    strOutput = strOutput + "top: " + intTop + "px;";
                    strOutput = strOutput + "width: " + intWidth + "px;";
                    strOutput = strOutput + "font-family: " + strFontName + ";";
                    strOutput = strOutput + "font-size: " + intFontSize + "pt;";
                    strOutput = strOutput + "padding: 1em;\">";
                    strOutput = strOutput + strValue;
                    strOutput = strOutput + "</div> ";
                    Response.Write(strOutput);
                    break;

                case "NAME":
                    strName = astrParts[1];
                    break;

                case "WIDTH":
                    if (clsCommonRtns.IsNumeric(astrParts[1]))
                    {
                        intWidth = clsCommonRtns.ConvertToInt(astrParts[1]);
                    }
                    else
                    {
                        intWidth = -1;
                    }
                    break;

                case "TOP":
                    if (clsCommonRtns.IsNumeric(astrParts[1]))
                    {
                        intTop = clsCommonRtns.ConvertToInt(astrParts[1]);
                    }
                    else
                    {
                        intTop = -1;
                    }
                    break;

                case "LEFT":
                    if (clsCommonRtns.IsNumeric(astrParts[1]))
                    {
                        intLeft = clsCommonRtns.ConvertToInt(astrParts[1]);
                    }
                    else
                    {
                        intLeft = -1;
                    }
                    break;

                case "HEIGHT":
                    if (clsCommonRtns.IsNumeric(astrParts[1]))
                    {
                        intHeight = clsCommonRtns.ConvertToInt(astrParts[1]);
                    }
                    else
                    {
                        intHeight = -1;
                    }
                    break;

                case "TYPE":
                    strType = astrParts[1];
                    break;

                case "VALUE":
                    //strValue = astrParts[1];
                    strValue = strName;
                    break;

                case "FONTNAME":
                    strFontName = astrParts[1];
                    break;

                case "FONTSIZE":
                    if (clsCommonRtns.IsNumeric(astrParts[1]))
                    {
                        intFontSize = clsCommonRtns.ConvertToInt(astrParts[1]);
                    }
                    else
                    {
                        intFontSize = -1;
                    }
                    break;

            }

        }


    }
}
