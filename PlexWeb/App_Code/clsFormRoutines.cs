﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for clsFormRoutines
/// </summary>
public class clsFormRoutines
{



    /// <summary>
    /// ConvertXMLToNameValuePairs - convert the xml records to name=value pair records
    /// </summary>
    /// <param name="arglstRecords"></param>
    /// <returns></returns>
    public static List<string> ConvertXMLToNameValuePairs(List<string> arglstRecords)
    {
        List<string> lstXMLRecords = new List<string>();
        string strEntity = "";
        string strTag = "";
        string strValue = "";
        int intStart = 0;
        int intEnd = 0;
        int intLength = 0;

        // walk the list
        foreach (string strRecord in arglstRecords)
        {
            // check for the header...
            if (strRecord.ToUpper().IndexOf("<?XML") >= 0)
            {
                // skip
            }
            else
            {
                // check for the start of the tag
                intStart = strRecord.IndexOf('<');
                // if not a <, get the next record
                if (intStart < 0)
                {
                    continue;
                }
                while(intStart < strRecord.Length)
                {
                    if (intStart >= 0)
                    {
                        intEnd = strRecord.IndexOf('>', intStart);
                        if (intEnd > intStart)
                        {
                            intLength = intEnd - intStart - 1;
                            // get the tag
                            strTag = strRecord.Substring(intStart + 1, intLength).ToUpper();
                            strEntity = strTag;
                            intStart = intEnd;
                            // get the value
                            intEnd = strRecord.IndexOf('<', intStart);
                            if (intEnd > intStart)
                            {
                                intLength = intEnd - intStart - 1;
                                strValue = strRecord.Substring(intStart + 1, intLength).ToUpper();
                                strEntity = strEntity + "=" + strValue;
                            }
                            lstXMLRecords.Add(strEntity);
                            strTag = "";
                            strValue = "";
                            if (intEnd > 0)
                            {
                                intStart = intEnd;
                                // now find the end of the record
                                intEnd = strRecord.IndexOf('>', intStart);
                                if (intEnd > intStart)
                                {
                                    intStart = intEnd;
                                }
                            }
                        }
                        else
                        {
                            intStart++;
                        }
                    }
                }
            }
        }

        return lstXMLRecords;

    }


    //*********************************************
    //* Read a file in and return an array of records
    //*********************************************
    public static List<string> ReadFile(String argstrFileName)
    {
        List<string> lstRecords = new List<string>();

        lstRecords = ReadFile(argstrFileName, 0);

        return lstRecords;
    }


    //*********************************************
    //* Read a file in and return an array of records
    //*********************************************
    public static List<string> ReadFile(String argstrFileName, int argintNumberOfLines)
    {
        String strFileName = "";
        List<string> lstRecords = new List<string>();
        int intNumberOfLinesRead = 0;

        strFileName = argstrFileName;

        // check for a path.  if it starts with a . put the current folder in front of it
        string strCurrentPath = AppDomain.CurrentDomain.BaseDirectory;
        strFileName = strFileName.Replace(".\\", strCurrentPath);

        // clear the array

        // Check to see if the file exists
        if (System.IO.File.Exists(strFileName))
        {
            // Open the file and read the records.
            try
            {
                using (System.IO.StreamReader objStreamReader = System.IO.File.OpenText(strFileName))
                {
                    String strBuffer = "";
                    while ((strBuffer = objStreamReader.ReadLine()) != null)
                    {
                        if (strBuffer.Trim().Length > 0)
                        {
                            intNumberOfLinesRead++;
                            lstRecords.Add(strBuffer);
                            if (argintNumberOfLines > 0 && intNumberOfLinesRead >= argintNumberOfLines)
                            {
                                break;
                            }
                        }

                    }

                    objStreamReader.Close();
                }
            }
            catch (Exception objException)
            {
                lstRecords.Clear();
            }
        }
        else
        {
            // no file specified
        }

        return lstRecords;

    }


    
    
}
