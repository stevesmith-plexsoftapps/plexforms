﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsCommonRtns
/// </summary>
public class clsCommonRtns
{

    //*********************************************
    //* Convert the passed value to a decimal
    //*********************************************
    public static decimal ConvertToDecimal(String argstrValue)
    {
        decimal decValue;

        try
        {
            decValue = Convert.ToDecimal(argstrValue);
        }
        catch (Exception objException)
        {
            decValue = 0;
        }

        return decValue;

    }




    //*********************************************
    //* Convert the passed value to a cased value
    //*********************************************
    public static string ConvertCase(String argstrValue)
    {
        string strValue = "";
        bool bolUpper = true;

        for (int n = 0; n < argstrValue.Length; n++)
        {
            string strChar = argstrValue.Substring(n, 1).ToLower();
            if (strChar == "_")
            {
                // set upper
                bolUpper = true;
            }
            else
            {
                if (bolUpper)
                {
                    strChar = strChar.ToUpper();
                    bolUpper = false;
                }
                strValue = strValue + strChar;
            }

        }

        return strValue;

    }


    //*********************************************
    //* Convert the passed value to a date
    //*********************************************
    public static DateTime ConvertToDate(String argstrValue)
    {
        DateTime dteValue;

        try
        {
            dteValue = Convert.ToDateTime(argstrValue);
        }
        catch (Exception objException)
        {
            dteValue = DateTime.Today;
        }

        return dteValue;

    }



    //*********************************************
    //* Convert the passed value to a string
    //*********************************************
    public static string ConvertToString(object argobjValue)
    {
        string strValue = "";

        if (argobjValue == null)
        {
            strValue = "";
        }
        else
        {
            strValue = argobjValue.ToString();
        }

        return strValue;

    }



    //*********************************************
    //* Convert the passed value to a double
    //*********************************************
    public static double ConvertToDouble(String argstrValue)
    {
        double dblValue;

        try
        {
            dblValue = Convert.ToDouble(argstrValue);
        }
        catch (Exception objException)
        {
            dblValue = 0.00;
        }

        return dblValue;

    }

    //*********************************************
    //* Convert the passed value to an int
    //*********************************************
    public static int ConvertToInt(String argstrValue)
    {
        int intValue;

        try
        {
            intValue = Convert.ToInt32(argstrValue);
        }
        catch (Exception objException)
        {
            intValue = 0;
        }

        return intValue;

    }


    // ******************************************************************
    // Indicates whether the supplied date string is a valid date
    //
    // argstrDate: The date to be checked as a string.
    //
    // Return Value: Returns true if valid date else false.
    // ******************************************************************
    public static bool IsDate(String argstrDate)
    {
        DateTime dteDate;

        // check it...
        try
        {
            dteDate = Convert.ToDateTime(argstrDate);
            return true;
        }
        catch (System.Exception objException)
        {
            return false;
        }

    }


    // ******************************************************************
    // Indicates whether the supplied value is numeric
    //
    // argstrValue: The value to be checked
    //
    // Return Value: Returns true if numeric false if not
    // ******************************************************************
    public static bool IsNumeric(String argstrValue)
    {
        Decimal decValue = 0;

        // check it...
        try
        {
            decValue = Convert.ToDecimal(argstrValue);
            return true;
        }
        catch (Exception objException)
        {
            return false;
        }

    }



}
