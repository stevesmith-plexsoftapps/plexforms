﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Net;
using System.IO;

namespace PlexWS
{
    class clsPlexWS
    {


        public string SendEmail(string argstrTo, string argstrFrom, string argstrSubject, string argstrMessage)
        {
            String strResult = "";
            string strTo = argstrTo;
            string strFrom = argstrFrom;
            string strSubject = argstrSubject;
            string strMessage = argstrMessage;

            try
            {
                PlexWS.PlexWebservice.WebServiceSoapClient objPlexWS = new PlexWS.PlexWebservice.WebServiceSoapClient();

                strResult = objPlexWS.SendEmail(strTo, strFrom, strSubject, strMessage);

            }
            catch (Exception ex)
            {
                strResult = "Error in call to webservice: " + ex.Message;
            }

            return strResult;


        }

        
        
        
        public string UploadForm(string argstrCustomerName, string argstrFormName, string argstrFormXML)
        {
            string strResponse = "";

            try
            {
                PlexWS.PlexWebservice.WebServiceSoapClient objPlexWS = new PlexWS.PlexWebservice.WebServiceSoapClient();

                strResponse = "Past definition";

                objPlexWS.Open();

                strResponse = "Past open";

                int intCustId = 0;
                string strCustomerName = argstrCustomerName.Trim();
                string strFormName = argstrFormName.Trim();
                string strFormXML = argstrFormXML.Trim();

                strResponse = "Calling method";

                strResponse = objPlexWS.UploadForm(argstrCustomerName, argstrFormName, argstrFormXML);

                objPlexWS.Close();

            }
            catch (Exception ex)
            {
                strResponse = "Error: " + ex.Message;
            }

            return strResponse;

        }



    }
}
