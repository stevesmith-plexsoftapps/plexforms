<?xml version="1.0" encoding="utf-8" ?>
<Forms>
    <Group>IFLOORDEALER</Group>
    <Form>
        <Name>ORDER_ENTRY</Name>
        <Image>C:\AAZytax\MyDocuments\Personal\Programming\PlexForms2010\PlexForms\bin\Debug\formfiles\images\ORDER_FORM.PNG</Image>
        <ImageHeight>1544</ImageHeight>
        <ImageWidth>1200</ImageWidth>
        <Tag></Tag>
        <Fields>
            <Field>
                <Name>DATE</Name>
                <Type>TextBox</Type>
                <Top>149</Top>
                <Left>188</Left>
                <Width>149</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>0</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>REPRESENTATIVE</Name>
                <Type>TextBox</Type>
                <Top>149</Top>
                <Left>581</Left>
                <Width>400</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>1</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>LASTNAME</Name>
                <Type>TextBox</Type>
                <Top>211</Top>
                <Left>251</Left>
                <Width>400</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>2</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>FIRSTNAME</Name>
                <Type>TextBox</Type>
                <Top>211</Top>
                <Left>810</Left>
                <Width>343</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>3</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ADDRESS</Name>
                <Type>TextBox</Type>
                <Top>243</Top>
                <Left>225</Left>
                <Width>800</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>4</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>STATE</Name>
                <Type>ComboBox</Type>
                <Top>273</Top>
                <Left>737</Left>
                <Width>57</Width>
                <Height>28</Height>
                <Tag>FILE=%CURRENTPATH%\FORMFILES\DATA\STATES.LST</Tag>
                <TabIndex>5</TabIndex>
                <List>FILE=%CURRENTPATH%\FORMFILES\DATA\STATES.LST</List>
                <Value>WI</Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>LOGO</Name>
                <Type>PictureBox</Type>
                <Top>9</Top>
                <Left>22</Left>
                <Width>150</Width>
                <Height>100</Height>
                <Tag></Tag>
                <TabIndex>6</TabIndex>
                <Image>C:\AAZytax\MyDocuments\Personal\Programming\PlexForms2010\PlexForms\bin\Debug\formfiles\images\BOBSCARPET.PNG</Image>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>CITY</Name>
                <Type>TextBox</Type>
                <Top>277</Top>
                <Left>180</Left>
                <Width>475</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>7</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ZIPCODE</Name>
                <Type>TextBox</Type>
                <Top>271</Top>
                <Left>940</Left>
                <Width>135</Width>
                <Height>28</Height>
                <Tag></Tag>
                <TabIndex>8</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>HOMEPHONE</Name>
                <Type>TextBox</Type>
                <Top>310</Top>
                <Left>294</Left>
                <Width>140</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>9</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>CELLPHONE</Name>
                <Type>TextBox</Type>
                <Top>309</Top>
                <Left>494</Left>
                <Width>140</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>10</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>EMAIL</Name>
                <Type>TextBox</Type>
                <Top>310</Top>
                <Left>738</Left>
                <Width>310</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>11</TabIndex>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-1</Name>
                <Type>ComboBox</Type>
                <Top>419</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>FILE=%CURRENTPATH%\FORMFILES\DATA\ROOMS.LST</Tag>
                <TabIndex>12</TabIndex>
                <List>FILE=%CURRENTPATH%\FORMFILES\DATA\ROOMS.LST</List>
                <Value>Bedroom1</Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-1</Name>
                <Type>ComboBox</Type>
                <Top>420</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>FILE=.\FORMFILES\DATA\PRODUCTS.LST</Tag>
                <TabIndex>13</TabIndex>
                <List>FILE=.\FORMFILES\DATA\PRODUCTS.LST</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
            </Rules>
            <Rules>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>SIZE-1</Name>
                <Type>TextBox</Type>
                <Top>421</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>14</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>PRICE-1</Name>
                <Type>TextBox</Type>
                <Top>422</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>15</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-1</Name>
                <Type>TextBox</Type>
                <Top>420</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>16</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-2</Name>
                <Type>ComboBox</Type>
                <Top>460</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>17</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ROOM-3</Name>
                <Type>ComboBox</Type>
                <Top>500</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>18</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ROOM-4</Name>
                <Type>ComboBox</Type>
                <Top>539</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>19</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-5</Name>
                <Type>ComboBox</Type>
                <Top>580</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>20</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-6</Name>
                <Type>ComboBox</Type>
                <Top>621</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>21</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ROOM-7</Name>
                <Type>ComboBox</Type>
                <Top>661</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>22</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ROOM-8</Name>
                <Type>ComboBox</Type>
                <Top>699</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>23</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ROOM-9</Name>
                <Type>ComboBox</Type>
                <Top>739</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>24</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-10</Name>
                <Type>ComboBox</Type>
                <Top>779</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>25</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-11</Name>
                <Type>ComboBox</Type>
                <Top>820</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>26</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-12</Name>
                <Type>ComboBox</Type>
                <Top>859</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>27</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-13</Name>
                <Type>ComboBox</Type>
                <Top>896</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>28</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-14</Name>
                <Type>ComboBox</Type>
                <Top>939</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>29</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-15</Name>
                <Type>ComboBox</Type>
                <Top>978</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>30</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-16</Name>
                <Type>ComboBox</Type>
                <Top>1018</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>31</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-17</Name>
                <Type>ComboBox</Type>
                <Top>1057</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>32</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-18</Name>
                <Type>ComboBox</Type>
                <Top>1099</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>33</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-19</Name>
                <Type>ComboBox</Type>
                <Top>1137</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>34</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-20</Name>
                <Type>ComboBox</Type>
                <Top>1175</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>35</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-21</Name>
                <Type>ComboBox</Type>
                <Top>1216</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>36</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ROOM-22</Name>
                <Type>ComboBox</Type>
                <Top>1257</Top>
                <Left>109</Left>
                <Width>200</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>37</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>SIZE-2</Name>
                <Type>TextBox</Type>
                <Top>460</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>38</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-3</Name>
                <Type>TextBox</Type>
                <Top>500</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>39</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-4</Name>
                <Type>TextBox</Type>
                <Top>543</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>40</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-5</Name>
                <Type>TextBox</Type>
                <Top>581</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>41</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-6</Name>
                <Type>TextBox</Type>
                <Top>619</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>42</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-7</Name>
                <Type>TextBox</Type>
                <Top>662</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>43</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-8</Name>
                <Type>TextBox</Type>
                <Top>702</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>44</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-9</Name>
                <Type>TextBox</Type>
                <Top>742</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>45</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-10</Name>
                <Type>TextBox</Type>
                <Top>784</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>46</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-11</Name>
                <Type>TextBox</Type>
                <Top>819</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>47</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-12</Name>
                <Type>TextBox</Type>
                <Top>859</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>48</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-13</Name>
                <Type>TextBox</Type>
                <Top>899</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>49</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-14</Name>
                <Type>TextBox</Type>
                <Top>939</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>50</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-15</Name>
                <Type>TextBox</Type>
                <Top>981</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>51</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-16</Name>
                <Type>TextBox</Type>
                <Top>1022</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>52</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-17</Name>
                <Type>TextBox</Type>
                <Top>1060</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>53</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-18</Name>
                <Type>TextBox</Type>
                <Top>1097</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>54</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-19</Name>
                <Type>TextBox</Type>
                <Top>1139</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>55</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-20</Name>
                <Type>TextBox</Type>
                <Top>1182</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>56</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-21</Name>
                <Type>TextBox</Type>
                <Top>1220</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>57</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-22</Name>
                <Type>TextBox</Type>
                <Top>1261</Top>
                <Left>323</Left>
                <Width>135</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>58</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-2</Name>
                <Type>ComboBox</Type>
                <Top>459</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>59</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-3</Name>
                <Type>ComboBox</Type>
                <Top>500</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>60</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-4</Name>
                <Type>ComboBox</Type>
                <Top>540</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>61</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-5</Name>
                <Type>ComboBox</Type>
                <Top>580</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>62</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-6</Name>
                <Type>ComboBox</Type>
                <Top>622</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>63</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-7</Name>
                <Type>ComboBox</Type>
                <Top>660</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>64</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-8</Name>
                <Type>ComboBox</Type>
                <Top>698</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>65</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-9</Name>
                <Type>ComboBox</Type>
                <Top>739</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>66</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-10</Name>
                <Type>ComboBox</Type>
                <Top>779</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>67</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-11</Name>
                <Type>ComboBox</Type>
                <Top>818</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>68</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-12</Name>
                <Type>ComboBox</Type>
                <Top>858</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>69</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-13</Name>
                <Type>ComboBox</Type>
                <Top>899</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>70</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-14</Name>
                <Type>ComboBox</Type>
                <Top>938</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>71</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-15</Name>
                <Type>ComboBox</Type>
                <Top>982</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>72</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-16</Name>
                <Type>ComboBox</Type>
                <Top>1019</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>73</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-17</Name>
                <Type>ComboBox</Type>
                <Top>1057</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>74</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-18</Name>
                <Type>ComboBox</Type>
                <Top>1098</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>75</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>ITEM-19</Name>
                <Type>ComboBox</Type>
                <Top>1138</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>76</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-20</Name>
                <Type>ComboBox</Type>
                <Top>1179</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>77</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-21</Name>
                <Type>ComboBox</Type>
                <Top>1219</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>78</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>ITEM-22</Name>
                <Type>ComboBox</Type>
                <Top>1259</Top>
                <Left>469</Left>
                <Width>420</Width>
                <Height>28</Height>
                <Tag>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</Tag>
                <TabIndex>79</TabIndex>
                <List>36;SBN (DETAIL)~76;WELD ROD (DETAIL)~77;ADHESIVES (DETAIL)~79;COVERING (DETAIL)~81;PAD (DETAIL)~82;TRIM (DETAIL)~62;DECOS (DETAIL)~61;LISTELLOS (DETAIL)~60;INSERTS (DETAIL)~59;GROUT (DETAIL)~153;BINDING (DETAIL)~75;TRIM (DETAIL)~37;FIELD (DETAIL)~35;S/E (DETAIL)~34;B/S (DETAIL)~31;CAULK (DETAIL)~32;KEY/CODE (DETAIL)~38;OSC (DETAIL)~160;NO SELECTION (INSTRUCTION)~89;CUTS (INSTRUCTION)~83;GROUT & CAULK (INSTRUCTION)~91;MAIN BATH - TILE B/S (ROOM)~109;PAD - MULTIROOM (ROOM)~101;CARPET - MULTI ROOM (ROOM)~78;PAD - OPT 1 (ROOM)~97;KITCHEN - B/S WOOD (ROOM)~96;MASTER BATH - WOOD B/S (ROOM)~95;MAIN BATH - WOOD B/S (ROOM)~94;KITCHEN - B/S TILE (LF) (ROOM)~92;MASTER BATH (ROOM)~84;KITCHEN - LAMINATE (ROOM)~90;MAIN BATH (ROOM)~85;DINING - LAMINATE (ROOM)~86;ENTRY - LAMINATE (ROOM)~176;KITCHEN/BATH (ROOM)~88;PAD - UPGRADE (ROOM)~66;CARPET-LESS DINING (ROOM)~93;MASTER BATH - TILE B/S (ROOM)~56;WOOD (ROOM)~2;CARPET - STANDARD (ROOM)~3;UPGRADES (ROOM)~4;PAD (ROOM)~5;KITCHEN (ROOM)~6;UTILITY (ROOM)~7;POWDER (ROOM)~8;MAIN (ROOM)~9;MASTER (ROOM)~10;ENTRY (ROOM)~11;FIREPLACE-1 SURROUND/2 HEARTH (ROOM)~68;ENTRY-BACK (ROOM)~27;FIREPLACE-1-SURROUND/1-HEARTH (ROOM)~74;VINYL (ROOM)~57;LAUNDRY (ROOM)~58;KITCHEN/NOOK (ROOM)~65;KIT/NOOK/DINING (ROOM)~67;UTIL/PDR (ROOM)~113;KITCHEN - FLOOR (ROOM)~69;ENTRY-FRONT (ROOM)~70;KIT/UTIL/PDR (ROOM)~71;COUNTRY KITCHEN (ROOM)~72;COUNTRY KIT/UTIL/PDR (ROOM)~73;KIT/NOOK/PDR (ROOM)~12;MISC (ROOM)~148;KITCHEN - B/S LAMINATE (ROOM)~122;KITCHEN FLOOR - HARDWOOD (ROOM)~162;MASTER SOAKER TUB (ROOM)~124;KITCHEN FLOOR - OPTIONS (ROOM)~125;KITCHEN COUNTER - OPTIONS (ROOM)~126;KITCHEN COUNTER - LAMINTE (ROOM)~150;MASTER TUB B/S (ROOM)~127;KITCHEN COUNTER - TILE (ROOM)~132;KITCHEN - B/S TILE / FH (LF) (ROOM)~157;MUD PAN (ROOM)~121;KITCHEN FLOOR - LAMINATE (ROOM)~131;KITCHEN - B/S OPTIONS (ROOM)~154;FLOOR (ROOM)~158;SHOWER WALLS (ROOM)~147;KITCHEN - ISLAND - SLAB GRANIT (ROOM)~146;KITCHEN - ISLAND - TILE (ROOM)~164;MASTER BATH - SHOWER (ROOM)~145;KITCHEN - ISLAND - LAMINATE (ROOM)~144;KITCHEN - ISLAND (ROOM)~128;KITCHEN COUNTER - GRANITE SLAB (ROOM)~167;B/S - 5 LF (ROOM)~175;CARPET (ROOM)~172;GROUT & CAULK (ROOM)~171;POWDER - TILE B/S (ROOM)~123;KITCHEN FLOOR - TILE (ROOM)~168;BATH - UP (ROOM)~120;KITCHEN FLOOR - VINYL (ROOM)~166;KITCHEN/DINING (ROOM)~87;CARPET - UPGRADE (ROOM)~156;KITCHEN/BUTLER (ROOM)~114;KITCHEN - COUNTER (ROOM)~151;MASTER TUB DECK-FACE-B/S (ROOM)~169;BATH - DOWN (ROOM)~42;B/S - 8 LF (SURFACE)~41;B/S - 6 LF (SURFACE)~40;B/S - 4 LF (SURFACE)~39;ISLAND 2 (SURFACE)~43;B/S - 9 LF (SURFACE)~159;COUNTER - BUTLER (SURFACE)~44;B/S - 10 LF (SURFACE)~45;B/S - 12 LF (SURFACE)~161;B/S - 5 LF (SURFACE)~33;UPGRADE (SURFACE)~20;COUNTER (SURFACE)~174;FLOOR TO CEILING (SURFACE)~173;B/S - FULL HEIGHT (SURFACE)~170;PDR - B/S (SURFACE)~165;B/S - 30 LF (SURFACE)~13;THRU-OUT (SURFACE)~14;FAMILY ROOM (SURFACE)~15;DINING ROOM (SURFACE)~16;BONUS (SURFACE)~17;DEN (SURFACE)~24;VANITY(S) (SURFACE)~19;FLOOR (SURFACE)~163;MASTER BATH - MUD PAN (SURFACE)~21;ISLAND (SURFACE)~22;TUB-FULL SOAK (SURFACE)~23;TUB B/S ONLY (SURFACE)~46;B/S - 14 LF (SURFACE)~25;MATERIAL (SURFACE)~26;LOCATION (SURFACE)~28;1-S/2-H (SURFACE)~29;FIREPLACE-1 SURROUND-ONLY (SURFACE)~30;H/B (SURFACE)~18;QUALITY (SURFACE)~103;MASTER BEDROOM (SURFACE)~130;WOOD (SURFACE)~129;OPTIONS (SURFACE)~1;LIVING ROOM (SURFACE)~119;GRANITE SLAB (SURFACE)~118;TILE (SURFACE)~117;HARDWOOD (SURFACE)~98;B/S (SURFACE)~99;B/S - TILE (SURFACE)~100;B/S - WOOD (SURFACE)~155;LAMINATE FLOORING (SURFACE)~102;ROOM (SURFACE)~134;OSC (SURFACE)~104;BEDROOM 1 (SURFACE)~105;BEDROOM 2 (SURFACE)~106;BEDROOM 3 (SURFACE)~107;BEDROOM 4 (SURFACE)~108;PAD - MULTI ROOM (SURFACE)~115;VINYL (SURFACE)~110;FIELD TILE (SURFACE)~111;GROUT (SURFACE)~112;CAULK (SURFACE)~116;LAMINATE (SURFACE)~64;B/S - 11 LF (SURFACE)~48;B/S - 16 LF (SURFACE)~49;B/S - 18 LF (SURFACE)~50;B/S - 19 LF (SURFACE)~51;B/S - 20 LF (SURFACE)~52;B/S - 21 LF (SURFACE)~53;B/S - 22 LF (SURFACE)~54;B/S - 24 LF (SURFACE)~55;B/S - 26 LF (SURFACE)~152;COVE BASE (SURFACE)~80;SHOWER WALL (SURFACE)~63;B/S - 32 LF (SURFACE)~133;SBN (SURFACE)~143;GRANITE (SURFACE)~142;COVERING (SURFACE)~141;VENTS (SURFACE)~140;TRIM (SURFACE)~139;PAD (SURFACE)~138;INSERTS (SURFACE)~137;OTHER (SURFACE)~136;LISTELLO (SURFACE)~135;DECO (SURFACE)~47;B/S - 15 LF (SURFACE)~149;PEDESTAL (SURFACE)~</List>
                <Value></Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>SUB-TOTAL</Name>
                <Type>TextBox</Type>
                <Top>1341</Top>
                <Left>1040</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>80</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SALES-TAX</Name>
                <Type>TextBox</Type>
                <Top>1380</Top>
                <Left>1041</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>81</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>LABOR-COST</Name>
                <Type>TextBox</Type>
                <Top>1420</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>82</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>DISCOUNT-PCT</Name>
                <Type>TextBox</Type>
                <Top>1457</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>83</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>GRAND-TOTAL</Name>
                <Type>TextBox</Type>
                <Top>1499</Top>
                <Left>1041</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>84</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Rules>
                <Rule></Rule>
            </Rules>
            <Field>
                <Name>SIZE-2</Name>
                <Type>TextBox</Type>
                <Top>461</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>85</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-3</Name>
                <Type>TextBox</Type>
                <Top>499</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>86</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-4</Name>
                <Type>TextBox</Type>
                <Top>541</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>87</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-5</Name>
                <Type>TextBox</Type>
                <Top>578</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>88</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-6</Name>
                <Type>TextBox</Type>
                <Top>620</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>89</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-7</Name>
                <Type>TextBox</Type>
                <Top>658</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>90</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-8</Name>
                <Type>TextBox</Type>
                <Top>698</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>91</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-9</Name>
                <Type>TextBox</Type>
                <Top>738</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>92</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-10</Name>
                <Type>TextBox</Type>
                <Top>779</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>93</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-11</Name>
                <Type>TextBox</Type>
                <Top>818</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>94</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-12</Name>
                <Type>TextBox</Type>
                <Top>859</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>95</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-13</Name>
                <Type>TextBox</Type>
                <Top>899</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>96</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-14</Name>
                <Type>TextBox</Type>
                <Top>938</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>97</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-15</Name>
                <Type>TextBox</Type>
                <Top>979</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>98</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-16</Name>
                <Type>TextBox</Type>
                <Top>1019</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>99</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-17</Name>
                <Type>TextBox</Type>
                <Top>1058</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>100</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-18</Name>
                <Type>TextBox</Type>
                <Top>1100</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>101</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-19</Name>
                <Type>TextBox</Type>
                <Top>1139</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>102</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-20</Name>
                <Type>TextBox</Type>
                <Top>1180</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>103</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-21</Name>
                <Type>TextBox</Type>
                <Top>1219</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>104</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>SIZE-22</Name>
                <Type>TextBox</Type>
                <Top>1258</Top>
                <Left>913</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>105</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-2</Name>
                <Type>TextBox</Type>
                <Top>460</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>106</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-3</Name>
                <Type>TextBox</Type>
                <Top>497</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>107</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-4</Name>
                <Type>TextBox</Type>
                <Top>539</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>108</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-5</Name>
                <Type>TextBox</Type>
                <Top>579</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>109</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-6</Name>
                <Type>TextBox</Type>
                <Top>621</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>110</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-7</Name>
                <Type>TextBox</Type>
                <Top>661</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>111</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-8</Name>
                <Type>TextBox</Type>
                <Top>697</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>112</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-9</Name>
                <Type>TextBox</Type>
                <Top>738</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>113</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-10</Name>
                <Type>TextBox</Type>
                <Top>779</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>114</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-11</Name>
                <Type>TextBox</Type>
                <Top>822</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>115</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-12</Name>
                <Type>TextBox</Type>
                <Top>858</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>116</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-13</Name>
                <Type>TextBox</Type>
                <Top>899</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>117</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-14</Name>
                <Type>TextBox</Type>
                <Top>938</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>118</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-15</Name>
                <Type>TextBox</Type>
                <Top>981</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>119</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-16</Name>
                <Type>TextBox</Type>
                <Top>1019</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>120</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-17</Name>
                <Type>TextBox</Type>
                <Top>1060</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>121</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-18</Name>
                <Type>TextBox</Type>
                <Top>1098</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>122</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-19</Name>
                <Type>TextBox</Type>
                <Top>1139</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>123</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-20</Name>
                <Type>TextBox</Type>
                <Top>1176</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>124</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-21</Name>
                <Type>TextBox</Type>
                <Top>1219</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>125</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
            <Field>
                <Name>TOTAL-22</Name>
                <Type>TextBox</Type>
                <Top>1256</Top>
                <Left>1042</Left>
                <Width>120</Width>
                <Height>24</Height>
                <Tag></Tag>
                <TabIndex>126</TabIndex>
                <Value> </Value>
                <Font>
                    <FontName>Microsoft Sans Serif</FontName>
                    <FontSize>12</FontSize>
                    <FontBold>False</FontBold>
                    <FontItalic>False</FontItalic>
                </Font>
            </Field>
        </Fields>
    </Form>
</Forms>
