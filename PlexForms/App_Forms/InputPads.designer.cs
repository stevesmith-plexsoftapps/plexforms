﻿namespace PlexForms
{
    partial class InputPads
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlKeyPad = new System.Windows.Forms.Panel();
            this.button120 = new System.Windows.Forms.Button();
            this.button100 = new System.Windows.Forms.Button();
            this.button99 = new System.Windows.Forms.Button();
            this.button98 = new System.Windows.Forms.Button();
            this.button97 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.btnNormalColor = new System.Windows.Forms.Button();
            this.btnReversedColor = new System.Windows.Forms.Button();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.pnlCalculator = new System.Windows.Forms.Panel();
            this.button91 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.grpUnits = new System.Windows.Forms.GroupBox();
            this.optMeters = new System.Windows.Forms.RadioButton();
            this.optInches = new System.Windows.Forms.RadioButton();
            this.optFeet = new System.Windows.Forms.RadioButton();
            this.optMG = new System.Windows.Forms.RadioButton();
            this.optKG = new System.Windows.Forms.RadioButton();
            this.optLBS = new System.Windows.Forms.RadioButton();
            this.pnlPositions = new System.Windows.Forms.Panel();
            this.button103 = new System.Windows.Forms.Button();
            this.button102 = new System.Windows.Forms.Button();
            this.button101 = new System.Windows.Forms.Button();
            this.btnKeyboard = new System.Windows.Forms.Button();
            this.btnPositions = new System.Windows.Forms.Button();
            this.btnCalculator = new System.Windows.Forms.Button();
            this.btnSymbols = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlGCS = new System.Windows.Forms.Panel();
            this.button79 = new System.Windows.Forms.Button();
            this.button3006 = new System.Windows.Forms.Button();
            this.button3005 = new System.Windows.Forms.Button();
            this.button2005 = new System.Windows.Forms.Button();
            this.button3001 = new System.Windows.Forms.Button();
            this.button3002 = new System.Windows.Forms.Button();
            this.button3003 = new System.Windows.Forms.Button();
            this.button3004 = new System.Windows.Forms.Button();
            this.button2001 = new System.Windows.Forms.Button();
            this.button2002 = new System.Windows.Forms.Button();
            this.button2003 = new System.Windows.Forms.Button();
            this.button2004 = new System.Windows.Forms.Button();
            this.button1001 = new System.Windows.Forms.Button();
            this.button1002 = new System.Windows.Forms.Button();
            this.button1003 = new System.Windows.Forms.Button();
            this.button1004 = new System.Windows.Forms.Button();
            this.button104 = new System.Windows.Forms.Button();
            this.btnGCS = new System.Windows.Forms.Button();
            this.pnlCalendar = new System.Windows.Forms.Panel();
            this.mnuCalendar = new System.Windows.Forms.MonthCalendar();
            this.btnCalendar = new System.Windows.Forms.Button();
            this.pnlBloodPressure = new System.Windows.Forms.Panel();
            this.button76 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.txtDiastolic = new System.Windows.Forms.TextBox();
            this.txtSystolic = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button81 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.pnlDosage = new System.Windows.Forms.Panel();
            this.lstDosageLog = new System.Windows.Forms.ListBox();
            this.button93 = new System.Windows.Forms.Button();
            this.txtDoseInMg = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMgPerKg = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWtInKG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWtInLbs = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.button95 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.button105 = new System.Windows.Forms.Button();
            this.button106 = new System.Windows.Forms.Button();
            this.button107 = new System.Windows.Forms.Button();
            this.button108 = new System.Windows.Forms.Button();
            this.button109 = new System.Windows.Forms.Button();
            this.button110 = new System.Windows.Forms.Button();
            this.button111 = new System.Windows.Forms.Button();
            this.button112 = new System.Windows.Forms.Button();
            this.pnlKeyPad.SuspendLayout();
            this.pnlCalculator.SuspendLayout();
            this.grpUnits.SuspendLayout();
            this.pnlPositions.SuspendLayout();
            this.pnlGCS.SuspendLayout();
            this.pnlCalendar.SuspendLayout();
            this.pnlBloodPressure.SuspendLayout();
            this.pnlDosage.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlKeyPad
            // 
            this.pnlKeyPad.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlKeyPad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlKeyPad.Controls.Add(this.button120);
            this.pnlKeyPad.Controls.Add(this.button100);
            this.pnlKeyPad.Controls.Add(this.button99);
            this.pnlKeyPad.Controls.Add(this.button98);
            this.pnlKeyPad.Controls.Add(this.button97);
            this.pnlKeyPad.Controls.Add(this.button55);
            this.pnlKeyPad.Controls.Add(this.button56);
            this.pnlKeyPad.Controls.Add(this.button57);
            this.pnlKeyPad.Controls.Add(this.button58);
            this.pnlKeyPad.Controls.Add(this.button59);
            this.pnlKeyPad.Controls.Add(this.button53);
            this.pnlKeyPad.Controls.Add(this.button52);
            this.pnlKeyPad.Controls.Add(this.button50);
            this.pnlKeyPad.Controls.Add(this.button49);
            this.pnlKeyPad.Controls.Add(this.button46);
            this.pnlKeyPad.Controls.Add(this.button47);
            this.pnlKeyPad.Controls.Add(this.button48);
            this.pnlKeyPad.Controls.Add(this.button43);
            this.pnlKeyPad.Controls.Add(this.button44);
            this.pnlKeyPad.Controls.Add(this.button45);
            this.pnlKeyPad.Controls.Add(this.button40);
            this.pnlKeyPad.Controls.Add(this.button41);
            this.pnlKeyPad.Controls.Add(this.button42);
            this.pnlKeyPad.Controls.Add(this.button23);
            this.pnlKeyPad.Controls.Add(this.button31);
            this.pnlKeyPad.Controls.Add(this.button32);
            this.pnlKeyPad.Controls.Add(this.button33);
            this.pnlKeyPad.Controls.Add(this.button34);
            this.pnlKeyPad.Controls.Add(this.button35);
            this.pnlKeyPad.Controls.Add(this.button36);
            this.pnlKeyPad.Controls.Add(this.button37);
            this.pnlKeyPad.Controls.Add(this.button38);
            this.pnlKeyPad.Controls.Add(this.button39);
            this.pnlKeyPad.Controls.Add(this.button22);
            this.pnlKeyPad.Controls.Add(this.button24);
            this.pnlKeyPad.Controls.Add(this.button25);
            this.pnlKeyPad.Controls.Add(this.button26);
            this.pnlKeyPad.Controls.Add(this.button27);
            this.pnlKeyPad.Controls.Add(this.button28);
            this.pnlKeyPad.Controls.Add(this.button29);
            this.pnlKeyPad.Controls.Add(this.button30);
            this.pnlKeyPad.Controls.Add(this.button13);
            this.pnlKeyPad.Controls.Add(this.button14);
            this.pnlKeyPad.Controls.Add(this.button15);
            this.pnlKeyPad.Controls.Add(this.button16);
            this.pnlKeyPad.Controls.Add(this.button17);
            this.pnlKeyPad.Controls.Add(this.button18);
            this.pnlKeyPad.Controls.Add(this.button19);
            this.pnlKeyPad.Controls.Add(this.button20);
            this.pnlKeyPad.Controls.Add(this.button21);
            this.pnlKeyPad.Controls.Add(this.button11);
            this.pnlKeyPad.Controls.Add(this.button12);
            this.pnlKeyPad.Controls.Add(this.button8);
            this.pnlKeyPad.Controls.Add(this.button9);
            this.pnlKeyPad.Controls.Add(this.button6);
            this.pnlKeyPad.Controls.Add(this.button7);
            this.pnlKeyPad.Controls.Add(this.button4);
            this.pnlKeyPad.Controls.Add(this.button5);
            this.pnlKeyPad.Controls.Add(this.button2);
            this.pnlKeyPad.Controls.Add(this.button3);
            this.pnlKeyPad.Controls.Add(this.button1);
            this.pnlKeyPad.Controls.Add(this.button10);
            this.pnlKeyPad.Controls.Add(this.btnNormalColor);
            this.pnlKeyPad.Controls.Add(this.btnReversedColor);
            this.pnlKeyPad.Location = new System.Drawing.Point(264, 43);
            this.pnlKeyPad.Name = "pnlKeyPad";
            this.pnlKeyPad.Size = new System.Drawing.Size(915, 233);
            this.pnlKeyPad.TabIndex = 8;
            // 
            // button120
            // 
            this.button120.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button120.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button120.ForeColor = System.Drawing.Color.Black;
            this.button120.Location = new System.Drawing.Point(580, 186);
            this.button120.Name = "button120";
            this.button120.Size = new System.Drawing.Size(65, 34);
            this.button120.TabIndex = 90;
            this.button120.Tag = "BUTTON";
            this.button120.Text = "Insert";
            this.button120.UseVisualStyleBackColor = false;
            // 
            // button100
            // 
            this.button100.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button100.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button100.Location = new System.Drawing.Point(846, 22);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(55, 35);
            this.button100.TabIndex = 89;
            this.button100.Tag = "BUTTON";
            this.button100.Text = ">";
            this.button100.UseVisualStyleBackColor = false;
            // 
            // button99
            // 
            this.button99.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button99.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button99.ForeColor = System.Drawing.Color.Black;
            this.button99.Location = new System.Drawing.Point(785, 22);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(55, 35);
            this.button99.TabIndex = 88;
            this.button99.Tag = "BUTTON";
            this.button99.Text = "<";
            this.button99.UseVisualStyleBackColor = false;
            // 
            // button98
            // 
            this.button98.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button98.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button98.ForeColor = System.Drawing.Color.Black;
            this.button98.Location = new System.Drawing.Point(724, 22);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(55, 35);
            this.button98.TabIndex = 87;
            this.button98.Tag = "BUTTON";
            this.button98.Text = ",";
            this.button98.UseVisualStyleBackColor = false;
            // 
            // button97
            // 
            this.button97.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button97.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button97.ForeColor = System.Drawing.Color.Black;
            this.button97.Location = new System.Drawing.Point(663, 22);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(55, 35);
            this.button97.TabIndex = 86;
            this.button97.Tag = "BUTTON";
            this.button97.Text = ";";
            this.button97.UseVisualStyleBackColor = false;
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button55.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button55.ForeColor = System.Drawing.Color.Black;
            this.button55.Location = new System.Drawing.Point(509, 187);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(65, 34);
            this.button55.TabIndex = 85;
            this.button55.Tag = "BUTTON";
            this.button55.Text = "Delete";
            this.button55.UseVisualStyleBackColor = false;
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button56.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button56.Location = new System.Drawing.Point(846, 187);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(55, 35);
            this.button56.TabIndex = 83;
            this.button56.Tag = "BUTTON";
            this.button56.Text = "-";
            this.button56.UseVisualStyleBackColor = false;
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button57.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button57.Location = new System.Drawing.Point(846, 144);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(55, 35);
            this.button57.TabIndex = 82;
            this.button57.Tag = "BUTTON";
            this.button57.Text = "+";
            this.button57.UseVisualStyleBackColor = false;
            // 
            // button58
            // 
            this.button58.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button58.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button58.Location = new System.Drawing.Point(846, 104);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(55, 35);
            this.button58.TabIndex = 81;
            this.button58.Tag = "BUTTON";
            this.button58.Text = "/";
            this.button58.UseVisualStyleBackColor = false;
            // 
            // button59
            // 
            this.button59.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button59.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button59.Location = new System.Drawing.Point(846, 61);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(55, 35);
            this.button59.TabIndex = 80;
            this.button59.Tag = "BUTTON";
            this.button59.Text = "=";
            this.button59.UseVisualStyleBackColor = false;
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button53.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button53.ForeColor = System.Drawing.Color.Black;
            this.button53.Location = new System.Drawing.Point(64, 187);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(65, 34);
            this.button53.TabIndex = 74;
            this.button53.Tag = "BUTTON";
            this.button53.Text = "Clear";
            this.button53.UseVisualStyleBackColor = false;
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button52.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button52.ForeColor = System.Drawing.Color.Black;
            this.button52.Location = new System.Drawing.Point(133, 187);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(65, 34);
            this.button52.TabIndex = 73;
            this.button52.Tag = "BUTTON";
            this.button52.Text = "BS";
            this.button52.UseVisualStyleBackColor = false;
            // 
            // button50
            // 
            this.button50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button50.Location = new System.Drawing.Point(204, 186);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(299, 35);
            this.button50.TabIndex = 71;
            this.button50.Tag = "BUTTON";
            this.button50.Text = "Space";
            this.button50.UseVisualStyleBackColor = true;
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button49.ForeColor = System.Drawing.Color.Black;
            this.button49.Location = new System.Drawing.Point(570, 145);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(65, 34);
            this.button49.TabIndex = 70;
            this.button49.Tag = "BUTTON";
            this.button49.Text = "Return";
            this.button49.UseVisualStyleBackColor = false;
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button46.Location = new System.Drawing.Point(785, 186);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(55, 35);
            this.button46.TabIndex = 69;
            this.button46.Tag = "BUTTON";
            this.button46.Text = ".";
            this.button46.UseVisualStyleBackColor = false;
            // 
            // button47
            // 
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button47.Location = new System.Drawing.Point(724, 186);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(55, 35);
            this.button47.TabIndex = 68;
            this.button47.Tag = "BUTTON";
            this.button47.Text = "0";
            this.button47.UseVisualStyleBackColor = true;
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button48.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button48.Location = new System.Drawing.Point(663, 186);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(55, 35);
            this.button48.TabIndex = 67;
            this.button48.Tag = "BUTTON";
            this.button48.Text = "-";
            this.button48.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button43.Location = new System.Drawing.Point(785, 144);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(55, 35);
            this.button43.TabIndex = 66;
            this.button43.Tag = "BUTTON";
            this.button43.Text = "9";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // button44
            // 
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button44.Location = new System.Drawing.Point(724, 144);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(55, 35);
            this.button44.TabIndex = 65;
            this.button44.Tag = "BUTTON";
            this.button44.Text = "8";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button45.Location = new System.Drawing.Point(663, 144);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(55, 35);
            this.button45.TabIndex = 64;
            this.button45.Tag = "BUTTON";
            this.button45.Text = "7";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // button40
            // 
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button40.Location = new System.Drawing.Point(785, 104);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(55, 35);
            this.button40.TabIndex = 63;
            this.button40.Tag = "BUTTON";
            this.button40.Text = "6";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button41.Location = new System.Drawing.Point(724, 104);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(55, 35);
            this.button41.TabIndex = 62;
            this.button41.Tag = "BUTTON";
            this.button41.Text = "5";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // button42
            // 
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button42.Location = new System.Drawing.Point(663, 104);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(55, 35);
            this.button42.TabIndex = 61;
            this.button42.Tag = "BUTTON";
            this.button42.Text = "4";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Location = new System.Drawing.Point(602, 21);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(55, 35);
            this.button23.TabIndex = 60;
            this.button23.Tag = "BUTTON";
            this.button23.Text = ")";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.ForeColor = System.Drawing.Color.Black;
            this.button31.Location = new System.Drawing.Point(541, 21);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(55, 35);
            this.button31.TabIndex = 59;
            this.button31.Tag = "BUTTON";
            this.button31.Text = "(";
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button32.ForeColor = System.Drawing.Color.Black;
            this.button32.Location = new System.Drawing.Point(480, 21);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(55, 35);
            this.button32.TabIndex = 58;
            this.button32.Tag = "BUTTON";
            this.button32.Text = "*";
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.ForeColor = System.Drawing.Color.Black;
            this.button33.Location = new System.Drawing.Point(419, 21);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(55, 35);
            this.button33.TabIndex = 57;
            this.button33.Tag = "BUTTON";
            this.button33.Text = "&&";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button34.ForeColor = System.Drawing.Color.Black;
            this.button34.Location = new System.Drawing.Point(358, 21);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(55, 35);
            this.button34.TabIndex = 56;
            this.button34.Tag = "BUTTON";
            this.button34.Text = "^";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button35.ForeColor = System.Drawing.Color.Black;
            this.button35.Location = new System.Drawing.Point(297, 21);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(55, 35);
            this.button35.TabIndex = 55;
            this.button35.Tag = "BUTTON";
            this.button35.Text = "%";
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Location = new System.Drawing.Point(236, 21);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(55, 35);
            this.button36.TabIndex = 54;
            this.button36.Tag = "BUTTON";
            this.button36.Text = "$";
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Location = new System.Drawing.Point(175, 21);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(55, 35);
            this.button37.TabIndex = 53;
            this.button37.Tag = "BUTTON";
            this.button37.Text = "#";
            this.button37.UseVisualStyleBackColor = false;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Location = new System.Drawing.Point(114, 21);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(55, 35);
            this.button38.TabIndex = 52;
            this.button38.Tag = "BUTTON";
            this.button38.Text = "@";
            this.button38.UseVisualStyleBackColor = false;
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button39.ForeColor = System.Drawing.Color.Black;
            this.button39.Location = new System.Drawing.Point(53, 21);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(55, 35);
            this.button39.TabIndex = 51;
            this.button39.Tag = "BUTTON";
            this.button39.Text = "!";
            this.button39.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Location = new System.Drawing.Point(11, 105);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(65, 34);
            this.button22.TabIndex = 50;
            this.button22.Tag = "BUTTON";
            this.button22.Text = "Caps";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(509, 145);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(55, 35);
            this.button24.TabIndex = 49;
            this.button24.Tag = "LETTER";
            this.button24.Text = "m";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.Location = new System.Drawing.Point(448, 145);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(55, 35);
            this.button25.TabIndex = 48;
            this.button25.Tag = "LETTER";
            this.button25.Text = "n";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.Location = new System.Drawing.Point(387, 145);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(55, 35);
            this.button26.TabIndex = 47;
            this.button26.Tag = "LETTER";
            this.button26.Text = "b";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.Location = new System.Drawing.Point(326, 145);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(55, 35);
            this.button27.TabIndex = 46;
            this.button27.Tag = "LETTER";
            this.button27.Text = "v";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button28.Location = new System.Drawing.Point(265, 145);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(55, 35);
            this.button28.TabIndex = 45;
            this.button28.Tag = "LETTER";
            this.button28.Text = "c";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.Location = new System.Drawing.Point(204, 145);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(55, 35);
            this.button29.TabIndex = 44;
            this.button29.Tag = "LETTER";
            this.button29.Text = "x";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.Location = new System.Drawing.Point(143, 145);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(55, 35);
            this.button30.TabIndex = 43;
            this.button30.Tag = "LETTER";
            this.button30.Text = "z";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(570, 104);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(55, 35);
            this.button13.TabIndex = 42;
            this.button13.Tag = "LETTER";
            this.button13.Text = "l";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(509, 104);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(55, 35);
            this.button14.TabIndex = 41;
            this.button14.Tag = "LETTER";
            this.button14.Text = "k";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(448, 104);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(55, 35);
            this.button15.TabIndex = 40;
            this.button15.Tag = "LETTER";
            this.button15.Text = "j";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(387, 104);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(55, 35);
            this.button16.TabIndex = 39;
            this.button16.Tag = "LETTER";
            this.button16.Text = "h";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(326, 104);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(55, 35);
            this.button17.TabIndex = 38;
            this.button17.Tag = "LETTER";
            this.button17.Text = "g";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(265, 104);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(55, 35);
            this.button18.TabIndex = 37;
            this.button18.Tag = "LETTER";
            this.button18.Text = "f";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(204, 105);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(55, 35);
            this.button19.TabIndex = 36;
            this.button19.Tag = "LETTER";
            this.button19.Text = "d";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(143, 104);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(55, 35);
            this.button20.TabIndex = 35;
            this.button20.Tag = "LETTER";
            this.button20.Text = "s";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Location = new System.Drawing.Point(82, 104);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(55, 35);
            this.button21.TabIndex = 34;
            this.button21.Tag = "LETTER";
            this.button21.Text = "a";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(785, 61);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(55, 35);
            this.button11.TabIndex = 33;
            this.button11.Tag = "BUTTON";
            this.button11.Text = "3";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(724, 61);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(55, 35);
            this.button12.TabIndex = 32;
            this.button12.Tag = "BUTTON";
            this.button12.Text = "2";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(602, 63);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(55, 35);
            this.button8.TabIndex = 31;
            this.button8.Tag = "LETTER";
            this.button8.Text = "p";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(541, 63);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(55, 35);
            this.button9.TabIndex = 30;
            this.button9.Tag = "LETTER";
            this.button9.Text = "o";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(480, 63);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(55, 35);
            this.button6.TabIndex = 29;
            this.button6.Tag = "LETTER";
            this.button6.Text = "i";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(419, 63);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(55, 35);
            this.button7.TabIndex = 28;
            this.button7.Tag = "LETTER";
            this.button7.Text = "u";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(358, 63);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(55, 35);
            this.button4.TabIndex = 27;
            this.button4.Tag = "LETTER";
            this.button4.Text = "y";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(297, 63);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(55, 35);
            this.button5.TabIndex = 26;
            this.button5.Tag = "LETTER";
            this.button5.Text = "t";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(236, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 35);
            this.button2.TabIndex = 25;
            this.button2.Tag = "LETTER";
            this.button2.Text = "r";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(175, 63);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 35);
            this.button3.TabIndex = 24;
            this.button3.Tag = "LETTER";
            this.button3.Text = "e";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(114, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 35);
            this.button1.TabIndex = 23;
            this.button1.Tag = "LETTER";
            this.button1.Text = "w";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(663, 61);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(55, 35);
            this.button10.TabIndex = 22;
            this.button10.Tag = "BUTTON";
            this.button10.Text = "1";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // btnNormalColor
            // 
            this.btnNormalColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNormalColor.Location = new System.Drawing.Point(53, 63);
            this.btnNormalColor.Name = "btnNormalColor";
            this.btnNormalColor.Size = new System.Drawing.Size(55, 35);
            this.btnNormalColor.TabIndex = 11;
            this.btnNormalColor.Tag = "LETTER";
            this.btnNormalColor.Text = "q";
            this.btnNormalColor.UseVisualStyleBackColor = true;
            // 
            // btnReversedColor
            // 
            this.btnReversedColor.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnReversedColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReversedColor.ForeColor = System.Drawing.Color.Black;
            this.btnReversedColor.Location = new System.Drawing.Point(72, 145);
            this.btnReversedColor.Name = "btnReversedColor";
            this.btnReversedColor.Size = new System.Drawing.Size(65, 34);
            this.btnReversedColor.TabIndex = 12;
            this.btnReversedColor.Tag = "BUTTON";
            this.btnReversedColor.Text = "Shift";
            this.btnReversedColor.UseVisualStyleBackColor = false;
            // 
            // txtDisplay
            // 
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(78, 5);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(591, 26);
            this.txtDisplay.TabIndex = 84;
            // 
            // btnDone
            // 
            this.btnDone.BackColor = System.Drawing.Color.LightGray;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDone.ForeColor = System.Drawing.Color.Black;
            this.btnDone.Location = new System.Drawing.Point(12, 5);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(55, 26);
            this.btnDone.TabIndex = 23;
            this.btnDone.Tag = "";
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // pnlCalculator
            // 
            this.pnlCalculator.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlCalculator.Controls.Add(this.button91);
            this.pnlCalculator.Controls.Add(this.button92);
            this.pnlCalculator.Controls.Add(this.button74);
            this.pnlCalculator.Controls.Add(this.button62);
            this.pnlCalculator.Controls.Add(this.button63);
            this.pnlCalculator.Controls.Add(this.button64);
            this.pnlCalculator.Controls.Add(this.button65);
            this.pnlCalculator.Controls.Add(this.button66);
            this.pnlCalculator.Controls.Add(this.button67);
            this.pnlCalculator.Controls.Add(this.button68);
            this.pnlCalculator.Controls.Add(this.button69);
            this.pnlCalculator.Controls.Add(this.button70);
            this.pnlCalculator.Controls.Add(this.button71);
            this.pnlCalculator.Controls.Add(this.button72);
            this.pnlCalculator.Controls.Add(this.button73);
            this.pnlCalculator.Controls.Add(this.button51);
            this.pnlCalculator.Controls.Add(this.button54);
            this.pnlCalculator.Controls.Add(this.button60);
            this.pnlCalculator.Controls.Add(this.button61);
            this.pnlCalculator.Controls.Add(this.lblDisplay);
            this.pnlCalculator.Controls.Add(this.grpUnits);
            this.pnlCalculator.Location = new System.Drawing.Point(516, 308);
            this.pnlCalculator.Name = "pnlCalculator";
            this.pnlCalculator.Size = new System.Drawing.Size(322, 326);
            this.pnlCalculator.TabIndex = 25;
            // 
            // button91
            // 
            this.button91.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button91.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button91.ForeColor = System.Drawing.Color.Black;
            this.button91.Location = new System.Drawing.Point(248, 194);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(65, 34);
            this.button91.TabIndex = 92;
            this.button91.Tag = "BUTTON";
            this.button91.Text = "Clear";
            this.button91.UseVisualStyleBackColor = false;
            // 
            // button92
            // 
            this.button92.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button92.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button92.ForeColor = System.Drawing.Color.Black;
            this.button92.Location = new System.Drawing.Point(248, 151);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(65, 34);
            this.button92.TabIndex = 91;
            this.button92.Tag = "BUTTON";
            this.button92.Text = "BS";
            this.button92.UseVisualStyleBackColor = false;
            // 
            // button74
            // 
            this.button74.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button74.Location = new System.Drawing.Point(248, 239);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(65, 77);
            this.button74.TabIndex = 80;
            this.button74.Tag = "=";
            this.button74.Text = "=";
            this.button74.UseVisualStyleBackColor = true;
            // 
            // button62
            // 
            this.button62.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button62.Location = new System.Drawing.Point(69, 279);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(54, 37);
            this.button62.TabIndex = 75;
            this.button62.Tag = "NUMBER";
            this.button62.Text = ".";
            this.button62.UseVisualStyleBackColor = true;
            // 
            // button63
            // 
            this.button63.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button63.Location = new System.Drawing.Point(9, 279);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(54, 37);
            this.button63.TabIndex = 74;
            this.button63.Tag = "NUMBER";
            this.button63.Text = "0";
            this.button63.UseVisualStyleBackColor = true;
            // 
            // button64
            // 
            this.button64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button64.Location = new System.Drawing.Point(128, 236);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(54, 37);
            this.button64.TabIndex = 73;
            this.button64.Tag = "NUMBER";
            this.button64.Text = "3";
            this.button64.UseVisualStyleBackColor = true;
            // 
            // button65
            // 
            this.button65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button65.Location = new System.Drawing.Point(68, 236);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(54, 37);
            this.button65.TabIndex = 72;
            this.button65.Tag = "NUMBER";
            this.button65.Text = "2";
            this.button65.UseVisualStyleBackColor = true;
            // 
            // button66
            // 
            this.button66.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button66.Location = new System.Drawing.Point(8, 236);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(54, 37);
            this.button66.TabIndex = 71;
            this.button66.Tag = "NUMBER";
            this.button66.Text = "1";
            this.button66.UseVisualStyleBackColor = true;
            // 
            // button67
            // 
            this.button67.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button67.Location = new System.Drawing.Point(128, 193);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(54, 37);
            this.button67.TabIndex = 70;
            this.button67.Tag = "NUMBER";
            this.button67.Text = "6";
            this.button67.UseVisualStyleBackColor = true;
            // 
            // button68
            // 
            this.button68.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button68.Location = new System.Drawing.Point(68, 193);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(54, 37);
            this.button68.TabIndex = 69;
            this.button68.Tag = "NUMBER";
            this.button68.Text = "5";
            this.button68.UseVisualStyleBackColor = true;
            // 
            // button69
            // 
            this.button69.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button69.Location = new System.Drawing.Point(8, 193);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(54, 37);
            this.button69.TabIndex = 68;
            this.button69.Tag = "NUMBER";
            this.button69.Text = "4";
            this.button69.UseVisualStyleBackColor = true;
            // 
            // button70
            // 
            this.button70.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button70.Location = new System.Drawing.Point(128, 150);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(54, 37);
            this.button70.TabIndex = 67;
            this.button70.Tag = "NUMBER";
            this.button70.Text = "9";
            this.button70.UseVisualStyleBackColor = true;
            // 
            // button71
            // 
            this.button71.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button71.Location = new System.Drawing.Point(68, 150);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(54, 37);
            this.button71.TabIndex = 66;
            this.button71.Tag = "NUMBER";
            this.button71.Text = "8";
            this.button71.UseVisualStyleBackColor = true;
            // 
            // button72
            // 
            this.button72.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button72.Location = new System.Drawing.Point(8, 150);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(54, 37);
            this.button72.TabIndex = 65;
            this.button72.Tag = "NUMBER";
            this.button72.Text = "7";
            this.button72.UseVisualStyleBackColor = true;
            // 
            // button73
            // 
            this.button73.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button73.Location = new System.Drawing.Point(129, 279);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(54, 37);
            this.button73.TabIndex = 64;
            this.button73.Tag = "CHANGESIGN";
            this.button73.Text = "+/-";
            this.button73.UseVisualStyleBackColor = true;
            // 
            // button51
            // 
            this.button51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button51.Location = new System.Drawing.Point(188, 279);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(54, 37);
            this.button51.TabIndex = 63;
            this.button51.Tag = "+";
            this.button51.Text = "+";
            this.button51.UseVisualStyleBackColor = true;
            // 
            // button54
            // 
            this.button54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button54.Location = new System.Drawing.Point(188, 236);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(54, 37);
            this.button54.TabIndex = 62;
            this.button54.Tag = "-";
            this.button54.Text = "-";
            this.button54.UseVisualStyleBackColor = true;
            // 
            // button60
            // 
            this.button60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button60.Location = new System.Drawing.Point(188, 193);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(54, 37);
            this.button60.TabIndex = 61;
            this.button60.Tag = "*";
            this.button60.Text = "*";
            this.button60.UseVisualStyleBackColor = true;
            // 
            // button61
            // 
            this.button61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button61.Location = new System.Drawing.Point(188, 150);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(54, 37);
            this.button61.TabIndex = 60;
            this.button61.Tag = "/";
            this.button61.Text = "/";
            this.button61.UseVisualStyleBackColor = true;
            // 
            // lblDisplay
            // 
            this.lblDisplay.BackColor = System.Drawing.Color.White;
            this.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplay.Location = new System.Drawing.Point(7, 9);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(306, 39);
            this.lblDisplay.TabIndex = 59;
            this.lblDisplay.Text = "0";
            this.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpUnits
            // 
            this.grpUnits.Controls.Add(this.optMeters);
            this.grpUnits.Controls.Add(this.optInches);
            this.grpUnits.Controls.Add(this.optFeet);
            this.grpUnits.Controls.Add(this.optMG);
            this.grpUnits.Controls.Add(this.optKG);
            this.grpUnits.Controls.Add(this.optLBS);
            this.grpUnits.Location = new System.Drawing.Point(6, 53);
            this.grpUnits.Name = "grpUnits";
            this.grpUnits.Size = new System.Drawing.Size(307, 91);
            this.grpUnits.TabIndex = 58;
            this.grpUnits.TabStop = false;
            this.grpUnits.Text = "Units";
            // 
            // optMeters
            // 
            this.optMeters.AutoSize = true;
            this.optMeters.Location = new System.Drawing.Point(159, 52);
            this.optMeters.Name = "optMeters";
            this.optMeters.Size = new System.Drawing.Size(57, 17);
            this.optMeters.TabIndex = 5;
            this.optMeters.TabStop = true;
            this.optMeters.Tag = "METERS";
            this.optMeters.Text = "Meters";
            this.optMeters.UseVisualStyleBackColor = true;
            // 
            // optInches
            // 
            this.optInches.AutoSize = true;
            this.optInches.Location = new System.Drawing.Point(78, 52);
            this.optInches.Name = "optInches";
            this.optInches.Size = new System.Drawing.Size(57, 17);
            this.optInches.TabIndex = 4;
            this.optInches.TabStop = true;
            this.optInches.Tag = "INCHES";
            this.optInches.Text = "Inches";
            this.optInches.UseVisualStyleBackColor = true;
            // 
            // optFeet
            // 
            this.optFeet.AutoSize = true;
            this.optFeet.Location = new System.Drawing.Point(10, 53);
            this.optFeet.Name = "optFeet";
            this.optFeet.Size = new System.Drawing.Size(46, 17);
            this.optFeet.TabIndex = 3;
            this.optFeet.TabStop = true;
            this.optFeet.Tag = "FEET";
            this.optFeet.Text = "Feet";
            this.optFeet.UseVisualStyleBackColor = true;
            // 
            // optMG
            // 
            this.optMG.AutoSize = true;
            this.optMG.Location = new System.Drawing.Point(159, 19);
            this.optMG.Name = "optMG";
            this.optMG.Size = new System.Drawing.Size(70, 17);
            this.optMG.TabIndex = 2;
            this.optMG.TabStop = true;
            this.optMG.Tag = "MG";
            this.optMG.Text = "Milligrams";
            this.optMG.UseVisualStyleBackColor = true;
            // 
            // optKG
            // 
            this.optKG.AutoSize = true;
            this.optKG.Location = new System.Drawing.Point(79, 19);
            this.optKG.Name = "optKG";
            this.optKG.Size = new System.Drawing.Size(72, 17);
            this.optKG.TabIndex = 1;
            this.optKG.TabStop = true;
            this.optKG.Tag = "KG";
            this.optKG.Text = "Killograms";
            this.optKG.UseVisualStyleBackColor = true;
            // 
            // optLBS
            // 
            this.optLBS.AutoSize = true;
            this.optLBS.Checked = true;
            this.optLBS.Location = new System.Drawing.Point(10, 19);
            this.optLBS.Name = "optLBS";
            this.optLBS.Size = new System.Drawing.Size(61, 17);
            this.optLBS.TabIndex = 0;
            this.optLBS.TabStop = true;
            this.optLBS.Tag = "LBS";
            this.optLBS.Text = "Pounds";
            this.optLBS.UseVisualStyleBackColor = true;
            // 
            // pnlPositions
            // 
            this.pnlPositions.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlPositions.Controls.Add(this.button103);
            this.pnlPositions.Controls.Add(this.button102);
            this.pnlPositions.Controls.Add(this.button101);
            this.pnlPositions.Location = new System.Drawing.Point(12, 97);
            this.pnlPositions.Name = "pnlPositions";
            this.pnlPositions.Size = new System.Drawing.Size(230, 333);
            this.pnlPositions.TabIndex = 26;
            this.pnlPositions.Visible = false;
            // 
            // button103
            // 
            this.button103.BackColor = System.Drawing.Color.White;
            this.button103.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button103.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button103.Location = new System.Drawing.Point(7, 211);
            this.button103.Name = "button103";
            this.button103.Size = new System.Drawing.Size(216, 117);
            this.button103.TabIndex = 94;
            this.button103.Tag = "Position";
            this.button103.Text = "Laying";
            this.button103.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button103.UseVisualStyleBackColor = false;
            // 
            // button102
            // 
            this.button102.BackColor = System.Drawing.Color.White;
            this.button102.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button102.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button102.Image = global::PlexForms.Properties.Resources.person_sitting;
            this.button102.Location = new System.Drawing.Point(115, 3);
            this.button102.Name = "button102";
            this.button102.Size = new System.Drawing.Size(108, 207);
            this.button102.TabIndex = 93;
            this.button102.Tag = "Position";
            this.button102.Text = "Sitting";
            this.button102.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button102.UseVisualStyleBackColor = false;
            // 
            // button101
            // 
            this.button101.AutoSize = true;
            this.button101.BackColor = System.Drawing.Color.White;
            this.button101.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button101.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button101.Image = global::PlexForms.Properties.Resources.person_standing;
            this.button101.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button101.Location = new System.Drawing.Point(7, 3);
            this.button101.Name = "button101";
            this.button101.Size = new System.Drawing.Size(108, 207);
            this.button101.TabIndex = 92;
            this.button101.Tag = "Position";
            this.button101.Text = "Standing";
            this.button101.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button101.UseVisualStyleBackColor = false;
            // 
            // btnKeyboard
            // 
            this.btnKeyboard.BackColor = System.Drawing.Color.LightGray;
            this.btnKeyboard.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnKeyboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKeyboard.ForeColor = System.Drawing.Color.Black;
            this.btnKeyboard.Location = new System.Drawing.Point(681, 5);
            this.btnKeyboard.Name = "btnKeyboard";
            this.btnKeyboard.Size = new System.Drawing.Size(30, 26);
            this.btnKeyboard.TabIndex = 28;
            this.btnKeyboard.Tag = "Keyboard";
            this.btnKeyboard.Text = "K";
            this.btnKeyboard.UseVisualStyleBackColor = false;
            this.btnKeyboard.Visible = false;
            this.btnKeyboard.Click += new System.EventHandler(this.btnKeyboard_Click);
            // 
            // btnPositions
            // 
            this.btnPositions.BackColor = System.Drawing.Color.LightGray;
            this.btnPositions.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPositions.ForeColor = System.Drawing.Color.Black;
            this.btnPositions.Location = new System.Drawing.Point(717, 5);
            this.btnPositions.Name = "btnPositions";
            this.btnPositions.Size = new System.Drawing.Size(30, 26);
            this.btnPositions.TabIndex = 29;
            this.btnPositions.Tag = "Positions";
            this.btnPositions.Text = "P";
            this.btnPositions.UseVisualStyleBackColor = false;
            this.btnPositions.Visible = false;
            this.btnPositions.Click += new System.EventHandler(this.btnPositions_Click);
            // 
            // btnCalculator
            // 
            this.btnCalculator.BackColor = System.Drawing.Color.LightGray;
            this.btnCalculator.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculator.ForeColor = System.Drawing.Color.Black;
            this.btnCalculator.Location = new System.Drawing.Point(753, 5);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.Size = new System.Drawing.Size(30, 26);
            this.btnCalculator.TabIndex = 30;
            this.btnCalculator.Tag = "Calculator";
            this.btnCalculator.Text = "C";
            this.btnCalculator.UseVisualStyleBackColor = false;
            this.btnCalculator.Visible = false;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // btnSymbols
            // 
            this.btnSymbols.BackColor = System.Drawing.Color.LightGray;
            this.btnSymbols.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSymbols.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSymbols.ForeColor = System.Drawing.Color.Black;
            this.btnSymbols.Location = new System.Drawing.Point(861, 5);
            this.btnSymbols.Name = "btnSymbols";
            this.btnSymbols.Size = new System.Drawing.Size(30, 26);
            this.btnSymbols.TabIndex = 31;
            this.btnSymbols.Tag = "Symbols";
            this.btnSymbols.Text = "S";
            this.btnSymbols.UseVisualStyleBackColor = false;
            this.btnSymbols.Visible = false;
            this.btnSymbols.Click += new System.EventHandler(this.btnSymbols_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightGray;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.Location = new System.Drawing.Point(1287, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 26);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Tag = "";
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // pnlGCS
            // 
            this.pnlGCS.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlGCS.Controls.Add(this.button79);
            this.pnlGCS.Controls.Add(this.button3006);
            this.pnlGCS.Controls.Add(this.button3005);
            this.pnlGCS.Controls.Add(this.button2005);
            this.pnlGCS.Controls.Add(this.button3001);
            this.pnlGCS.Controls.Add(this.button3002);
            this.pnlGCS.Controls.Add(this.button3003);
            this.pnlGCS.Controls.Add(this.button3004);
            this.pnlGCS.Controls.Add(this.button2001);
            this.pnlGCS.Controls.Add(this.button2002);
            this.pnlGCS.Controls.Add(this.button2003);
            this.pnlGCS.Controls.Add(this.button2004);
            this.pnlGCS.Controls.Add(this.button1001);
            this.pnlGCS.Controls.Add(this.button1002);
            this.pnlGCS.Controls.Add(this.button1003);
            this.pnlGCS.Controls.Add(this.button1004);
            this.pnlGCS.Controls.Add(this.button104);
            this.pnlGCS.Location = new System.Drawing.Point(20, 450);
            this.pnlGCS.Name = "pnlGCS";
            this.pnlGCS.Size = new System.Drawing.Size(478, 364);
            this.pnlGCS.TabIndex = 33;
            this.pnlGCS.Visible = false;
            this.pnlGCS.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlEyes_Paint);
            // 
            // button79
            // 
            this.button79.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button79.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button79.ForeColor = System.Drawing.Color.Black;
            this.button79.Location = new System.Drawing.Point(402, 323);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(65, 34);
            this.button79.TabIndex = 111;
            this.button79.Tag = "BUTTON";
            this.button79.Text = "Return";
            this.button79.UseVisualStyleBackColor = false;
            // 
            // button3006
            // 
            this.button3006.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3006.Location = new System.Drawing.Point(406, 80);
            this.button3006.Name = "button3006";
            this.button3006.Size = new System.Drawing.Size(30, 29);
            this.button3006.TabIndex = 110;
            this.button3006.Tag = "NUMBER";
            this.button3006.Text = "6";
            this.button3006.UseVisualStyleBackColor = true;
            // 
            // button3005
            // 
            this.button3005.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3005.Location = new System.Drawing.Point(406, 114);
            this.button3005.Name = "button3005";
            this.button3005.Size = new System.Drawing.Size(30, 29);
            this.button3005.TabIndex = 109;
            this.button3005.Tag = "NUMBER";
            this.button3005.Text = "5";
            this.button3005.UseVisualStyleBackColor = true;
            // 
            // button2005
            // 
            this.button2005.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2005.Location = new System.Drawing.Point(255, 88);
            this.button2005.Name = "button2005";
            this.button2005.Size = new System.Drawing.Size(30, 29);
            this.button2005.TabIndex = 108;
            this.button2005.Tag = "NUMBER";
            this.button2005.Text = "5";
            this.button2005.UseVisualStyleBackColor = true;
            // 
            // button3001
            // 
            this.button3001.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3001.Location = new System.Drawing.Point(406, 244);
            this.button3001.Name = "button3001";
            this.button3001.Size = new System.Drawing.Size(30, 29);
            this.button3001.TabIndex = 107;
            this.button3001.Tag = "NUMBER";
            this.button3001.Text = "1";
            this.button3001.UseVisualStyleBackColor = true;
            // 
            // button3002
            // 
            this.button3002.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3002.Location = new System.Drawing.Point(406, 212);
            this.button3002.Name = "button3002";
            this.button3002.Size = new System.Drawing.Size(30, 29);
            this.button3002.TabIndex = 106;
            this.button3002.Tag = "NUMBER";
            this.button3002.Text = "2";
            this.button3002.UseVisualStyleBackColor = true;
            // 
            // button3003
            // 
            this.button3003.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3003.Location = new System.Drawing.Point(406, 179);
            this.button3003.Name = "button3003";
            this.button3003.Size = new System.Drawing.Size(30, 29);
            this.button3003.TabIndex = 105;
            this.button3003.Tag = "NUMBER";
            this.button3003.Text = "3";
            this.button3003.UseVisualStyleBackColor = true;
            // 
            // button3004
            // 
            this.button3004.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3004.Location = new System.Drawing.Point(406, 147);
            this.button3004.Name = "button3004";
            this.button3004.Size = new System.Drawing.Size(30, 29);
            this.button3004.TabIndex = 104;
            this.button3004.Tag = "NUMBER";
            this.button3004.Text = "4";
            this.button3004.UseVisualStyleBackColor = true;
            // 
            // button2001
            // 
            this.button2001.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2001.Location = new System.Drawing.Point(255, 221);
            this.button2001.Name = "button2001";
            this.button2001.Size = new System.Drawing.Size(30, 29);
            this.button2001.TabIndex = 103;
            this.button2001.Tag = "NUMBER";
            this.button2001.Text = "1";
            this.button2001.UseVisualStyleBackColor = true;
            // 
            // button2002
            // 
            this.button2002.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2002.Location = new System.Drawing.Point(255, 188);
            this.button2002.Name = "button2002";
            this.button2002.Size = new System.Drawing.Size(30, 29);
            this.button2002.TabIndex = 102;
            this.button2002.Tag = "NUMBER";
            this.button2002.Text = "2";
            this.button2002.UseVisualStyleBackColor = true;
            // 
            // button2003
            // 
            this.button2003.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2003.Location = new System.Drawing.Point(255, 154);
            this.button2003.Name = "button2003";
            this.button2003.Size = new System.Drawing.Size(30, 29);
            this.button2003.TabIndex = 101;
            this.button2003.Tag = "NUMBER";
            this.button2003.Text = "3";
            this.button2003.UseVisualStyleBackColor = true;
            // 
            // button2004
            // 
            this.button2004.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2004.Location = new System.Drawing.Point(255, 121);
            this.button2004.Name = "button2004";
            this.button2004.Size = new System.Drawing.Size(30, 29);
            this.button2004.TabIndex = 100;
            this.button2004.Tag = "NUMBER";
            this.button2004.Text = "4";
            this.button2004.UseVisualStyleBackColor = true;
            // 
            // button1001
            // 
            this.button1001.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1001.Location = new System.Drawing.Point(115, 188);
            this.button1001.Name = "button1001";
            this.button1001.Size = new System.Drawing.Size(30, 29);
            this.button1001.TabIndex = 99;
            this.button1001.Tag = "NUMBER";
            this.button1001.Text = "1";
            this.button1001.UseVisualStyleBackColor = true;
            // 
            // button1002
            // 
            this.button1002.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1002.Location = new System.Drawing.Point(115, 155);
            this.button1002.Name = "button1002";
            this.button1002.Size = new System.Drawing.Size(30, 29);
            this.button1002.TabIndex = 98;
            this.button1002.Tag = "NUMBER";
            this.button1002.Text = "2";
            this.button1002.UseVisualStyleBackColor = true;
            // 
            // button1003
            // 
            this.button1003.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1003.Location = new System.Drawing.Point(115, 121);
            this.button1003.Name = "button1003";
            this.button1003.Size = new System.Drawing.Size(30, 29);
            this.button1003.TabIndex = 97;
            this.button1003.Tag = "NUMBER";
            this.button1003.Text = "3";
            this.button1003.UseVisualStyleBackColor = true;
            // 
            // button1004
            // 
            this.button1004.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1004.Location = new System.Drawing.Point(115, 88);
            this.button1004.Name = "button1004";
            this.button1004.Size = new System.Drawing.Size(30, 29);
            this.button1004.TabIndex = 96;
            this.button1004.Tag = "NUMBER";
            this.button1004.Text = "4";
            this.button1004.UseVisualStyleBackColor = true;
            // 
            // button104
            // 
            this.button104.AutoSize = true;
            this.button104.BackColor = System.Drawing.Color.White;
            this.button104.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button104.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button104.Image = global::PlexForms.Properties.Resources.InitialGCS;
            this.button104.Location = new System.Drawing.Point(11, 11);
            this.button104.Name = "button104";
            this.button104.Size = new System.Drawing.Size(456, 306);
            this.button104.TabIndex = 95;
            this.button104.Tag = "Image";
            this.button104.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button104.UseVisualStyleBackColor = false;
            // 
            // btnGCS
            // 
            this.btnGCS.BackColor = System.Drawing.Color.LightGray;
            this.btnGCS.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGCS.ForeColor = System.Drawing.Color.Black;
            this.btnGCS.Location = new System.Drawing.Point(789, 5);
            this.btnGCS.Name = "btnGCS";
            this.btnGCS.Size = new System.Drawing.Size(30, 26);
            this.btnGCS.TabIndex = 34;
            this.btnGCS.Tag = "GCS";
            this.btnGCS.Text = "G";
            this.btnGCS.UseVisualStyleBackColor = false;
            this.btnGCS.Visible = false;
            this.btnGCS.Click += new System.EventHandler(this.btnImages_Click);
            // 
            // pnlCalendar
            // 
            this.pnlCalendar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlCalendar.Controls.Add(this.mnuCalendar);
            this.pnlCalendar.Location = new System.Drawing.Point(1151, 40);
            this.pnlCalendar.Name = "pnlCalendar";
            this.pnlCalendar.Size = new System.Drawing.Size(300, 184);
            this.pnlCalendar.TabIndex = 35;
            // 
            // mnuCalendar
            // 
            this.mnuCalendar.Location = new System.Drawing.Point(34, 9);
            this.mnuCalendar.Name = "mnuCalendar";
            this.mnuCalendar.TabIndex = 0;
            this.mnuCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.mnuCalendar_DateChanged);
            // 
            // btnCalendar
            // 
            this.btnCalendar.BackColor = System.Drawing.Color.LightGray;
            this.btnCalendar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCalendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalendar.ForeColor = System.Drawing.Color.Black;
            this.btnCalendar.Location = new System.Drawing.Point(825, 5);
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.Size = new System.Drawing.Size(30, 26);
            this.btnCalendar.TabIndex = 36;
            this.btnCalendar.Tag = "Calendar";
            this.btnCalendar.Text = "D";
            this.btnCalendar.UseVisualStyleBackColor = false;
            this.btnCalendar.Visible = false;
            this.btnCalendar.Click += new System.EventHandler(this.btnCalendar_Click);
            // 
            // pnlBloodPressure
            // 
            this.pnlBloodPressure.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlBloodPressure.Controls.Add(this.button76);
            this.pnlBloodPressure.Controls.Add(this.button80);
            this.pnlBloodPressure.Controls.Add(this.button75);
            this.pnlBloodPressure.Controls.Add(this.txtDiastolic);
            this.pnlBloodPressure.Controls.Add(this.txtSystolic);
            this.pnlBloodPressure.Controls.Add(this.label3);
            this.pnlBloodPressure.Controls.Add(this.label2);
            this.pnlBloodPressure.Controls.Add(this.label1);
            this.pnlBloodPressure.Controls.Add(this.button81);
            this.pnlBloodPressure.Controls.Add(this.button82);
            this.pnlBloodPressure.Controls.Add(this.button83);
            this.pnlBloodPressure.Controls.Add(this.button84);
            this.pnlBloodPressure.Controls.Add(this.button85);
            this.pnlBloodPressure.Controls.Add(this.button86);
            this.pnlBloodPressure.Controls.Add(this.button87);
            this.pnlBloodPressure.Controls.Add(this.button88);
            this.pnlBloodPressure.Controls.Add(this.button89);
            this.pnlBloodPressure.Controls.Add(this.button90);
            this.pnlBloodPressure.Location = new System.Drawing.Point(938, 226);
            this.pnlBloodPressure.Name = "pnlBloodPressure";
            this.pnlBloodPressure.Size = new System.Drawing.Size(310, 210);
            this.pnlBloodPressure.TabIndex = 85;
            // 
            // button76
            // 
            this.button76.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button76.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button76.ForeColor = System.Drawing.Color.Black;
            this.button76.Location = new System.Drawing.Point(8, 171);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(65, 34);
            this.button76.TabIndex = 89;
            this.button76.Tag = "BUTTON";
            this.button76.Text = "Clear";
            this.button76.UseVisualStyleBackColor = false;
            // 
            // button80
            // 
            this.button80.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button80.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button80.ForeColor = System.Drawing.Color.Black;
            this.button80.Location = new System.Drawing.Point(77, 171);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(65, 34);
            this.button80.TabIndex = 88;
            this.button80.Tag = "BUTTON";
            this.button80.Text = "BS";
            this.button80.UseVisualStyleBackColor = false;
            // 
            // button75
            // 
            this.button75.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button75.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button75.ForeColor = System.Drawing.Color.Black;
            this.button75.Location = new System.Drawing.Point(236, 172);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(65, 34);
            this.button75.TabIndex = 87;
            this.button75.Tag = "BUTTON";
            this.button75.Text = "Return";
            this.button75.UseVisualStyleBackColor = false;
            // 
            // txtDiastolic
            // 
            this.txtDiastolic.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiastolic.Location = new System.Drawing.Point(167, 50);
            this.txtDiastolic.Name = "txtDiastolic";
            this.txtDiastolic.Size = new System.Drawing.Size(110, 29);
            this.txtDiastolic.TabIndex = 86;
            this.txtDiastolic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSystolic
            // 
            this.txtSystolic.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSystolic.Location = new System.Drawing.Point(20, 50);
            this.txtSystolic.Name = "txtSystolic";
            this.txtSystolic.Size = new System.Drawing.Size(110, 29);
            this.txtSystolic.TabIndex = 85;
            this.txtSystolic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(151, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 34);
            this.label3.TabIndex = 84;
            this.label3.Text = "Diastolic";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 34);
            this.label2.TabIndex = 83;
            this.label2.Text = "Systolic";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(134, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 34);
            this.label1.TabIndex = 82;
            this.label1.Text = "/";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button81
            // 
            this.button81.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button81.Location = new System.Drawing.Point(247, 127);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(54, 37);
            this.button81.TabIndex = 74;
            this.button81.Tag = "NUMBER";
            this.button81.Text = "0";
            this.button81.UseVisualStyleBackColor = true;
            // 
            // button82
            // 
            this.button82.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button82.Location = new System.Drawing.Point(127, 85);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(54, 37);
            this.button82.TabIndex = 73;
            this.button82.Tag = "NUMBER";
            this.button82.Text = "3";
            this.button82.UseVisualStyleBackColor = true;
            // 
            // button83
            // 
            this.button83.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button83.Location = new System.Drawing.Point(67, 85);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(54, 37);
            this.button83.TabIndex = 72;
            this.button83.Tag = "NUMBER";
            this.button83.Text = "2";
            this.button83.UseVisualStyleBackColor = true;
            // 
            // button84
            // 
            this.button84.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button84.Location = new System.Drawing.Point(7, 85);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(54, 37);
            this.button84.TabIndex = 71;
            this.button84.Tag = "NUMBER";
            this.button84.Text = "1";
            this.button84.UseVisualStyleBackColor = true;
            // 
            // button85
            // 
            this.button85.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button85.Location = new System.Drawing.Point(7, 127);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(54, 37);
            this.button85.TabIndex = 70;
            this.button85.Tag = "NUMBER";
            this.button85.Text = "6";
            this.button85.UseVisualStyleBackColor = true;
            // 
            // button86
            // 
            this.button86.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button86.Location = new System.Drawing.Point(247, 85);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(54, 37);
            this.button86.TabIndex = 69;
            this.button86.Tag = "NUMBER";
            this.button86.Text = "5";
            this.button86.UseVisualStyleBackColor = true;
            // 
            // button87
            // 
            this.button87.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button87.Location = new System.Drawing.Point(187, 85);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(54, 37);
            this.button87.TabIndex = 68;
            this.button87.Tag = "NUMBER";
            this.button87.Text = "4";
            this.button87.UseVisualStyleBackColor = true;
            // 
            // button88
            // 
            this.button88.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button88.Location = new System.Drawing.Point(187, 126);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(54, 37);
            this.button88.TabIndex = 67;
            this.button88.Tag = "NUMBER";
            this.button88.Text = "9";
            this.button88.UseVisualStyleBackColor = true;
            // 
            // button89
            // 
            this.button89.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button89.Location = new System.Drawing.Point(129, 126);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(54, 37);
            this.button89.TabIndex = 66;
            this.button89.Tag = "NUMBER";
            this.button89.Text = "8";
            this.button89.UseVisualStyleBackColor = true;
            // 
            // button90
            // 
            this.button90.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button90.Location = new System.Drawing.Point(69, 126);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(54, 37);
            this.button90.TabIndex = 65;
            this.button90.Tag = "NUMBER";
            this.button90.Text = "7";
            this.button90.UseVisualStyleBackColor = true;
            // 
            // pnlDosage
            // 
            this.pnlDosage.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlDosage.Controls.Add(this.lstDosageLog);
            this.pnlDosage.Controls.Add(this.button93);
            this.pnlDosage.Controls.Add(this.txtDoseInMg);
            this.pnlDosage.Controls.Add(this.label7);
            this.pnlDosage.Controls.Add(this.txtMgPerKg);
            this.pnlDosage.Controls.Add(this.label6);
            this.pnlDosage.Controls.Add(this.txtWtInKG);
            this.pnlDosage.Controls.Add(this.label5);
            this.pnlDosage.Controls.Add(this.txtWtInLbs);
            this.pnlDosage.Controls.Add(this.label4);
            this.pnlDosage.Controls.Add(this.button77);
            this.pnlDosage.Controls.Add(this.button78);
            this.pnlDosage.Controls.Add(this.button94);
            this.pnlDosage.Controls.Add(this.button95);
            this.pnlDosage.Controls.Add(this.button96);
            this.pnlDosage.Controls.Add(this.button105);
            this.pnlDosage.Controls.Add(this.button106);
            this.pnlDosage.Controls.Add(this.button107);
            this.pnlDosage.Controls.Add(this.button108);
            this.pnlDosage.Controls.Add(this.button109);
            this.pnlDosage.Controls.Add(this.button110);
            this.pnlDosage.Controls.Add(this.button111);
            this.pnlDosage.Controls.Add(this.button112);
            this.pnlDosage.Location = new System.Drawing.Point(877, 442);
            this.pnlDosage.Name = "pnlDosage";
            this.pnlDosage.Size = new System.Drawing.Size(322, 409);
            this.pnlDosage.TabIndex = 86;
            // 
            // lstDosageLog
            // 
            this.lstDosageLog.FormattingEnabled = true;
            this.lstDosageLog.Location = new System.Drawing.Point(11, 14);
            this.lstDosageLog.Name = "lstDosageLog";
            this.lstDosageLog.Size = new System.Drawing.Size(302, 69);
            this.lstDosageLog.TabIndex = 102;
            // 
            // button93
            // 
            this.button93.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button93.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button93.ForeColor = System.Drawing.Color.Black;
            this.button93.Location = new System.Drawing.Point(251, 356);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(65, 34);
            this.button93.TabIndex = 101;
            this.button93.Tag = "BUTTON";
            this.button93.Text = "Return";
            this.button93.UseVisualStyleBackColor = false;
            // 
            // txtDoseInMg
            // 
            this.txtDoseInMg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDoseInMg.Location = new System.Drawing.Point(112, 211);
            this.txtDoseInMg.Name = "txtDoseInMg";
            this.txtDoseInMg.Size = new System.Drawing.Size(110, 29);
            this.txtDoseInMg.TabIndex = 100;
            this.txtDoseInMg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDoseInMg.TextChanged += new System.EventHandler(this.txtDoseInMg_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 20);
            this.label7.TabIndex = 99;
            this.label7.Text = "Dosage(MG):";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMgPerKg
            // 
            this.txtMgPerKg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMgPerKg.Location = new System.Drawing.Point(112, 175);
            this.txtMgPerKg.Name = "txtMgPerKg";
            this.txtMgPerKg.Size = new System.Drawing.Size(110, 29);
            this.txtMgPerKg.TabIndex = 98;
            this.txtMgPerKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMgPerKg.TextChanged += new System.EventHandler(this.txtMgPerKg_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 20);
            this.label6.TabIndex = 97;
            this.label6.Text = "MG/KG:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWtInKG
            // 
            this.txtWtInKG.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWtInKG.Location = new System.Drawing.Point(112, 140);
            this.txtWtInKG.Name = "txtWtInKG";
            this.txtWtInKG.Size = new System.Drawing.Size(110, 29);
            this.txtWtInKG.TabIndex = 96;
            this.txtWtInKG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWtInKG.TextChanged += new System.EventHandler(this.txtWtInKG_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 95;
            this.label5.Text = "Wt in KG:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWtInLbs
            // 
            this.txtWtInLbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWtInLbs.Location = new System.Drawing.Point(112, 105);
            this.txtWtInLbs.Name = "txtWtInLbs";
            this.txtWtInLbs.Size = new System.Drawing.Size(110, 29);
            this.txtWtInLbs.TabIndex = 94;
            this.txtWtInLbs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWtInLbs.TextChanged += new System.EventHandler(this.txtWtInLbs_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 93;
            this.label4.Text = "Wt in LBS:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button77
            // 
            this.button77.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button77.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button77.ForeColor = System.Drawing.Color.Black;
            this.button77.Location = new System.Drawing.Point(251, 313);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(65, 34);
            this.button77.TabIndex = 92;
            this.button77.Tag = "BUTTON";
            this.button77.Text = "Clear";
            this.button77.UseVisualStyleBackColor = false;
            // 
            // button78
            // 
            this.button78.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.button78.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button78.ForeColor = System.Drawing.Color.Black;
            this.button78.Location = new System.Drawing.Point(251, 270);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(65, 34);
            this.button78.TabIndex = 91;
            this.button78.Tag = "BUTTON";
            this.button78.Text = "BS";
            this.button78.UseVisualStyleBackColor = false;
            // 
            // button94
            // 
            this.button94.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button94.Location = new System.Drawing.Point(133, 355);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(54, 37);
            this.button94.TabIndex = 75;
            this.button94.Tag = "NUMBER";
            this.button94.Text = ".";
            this.button94.UseVisualStyleBackColor = true;
            // 
            // button95
            // 
            this.button95.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button95.Location = new System.Drawing.Point(72, 355);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(54, 37);
            this.button95.TabIndex = 74;
            this.button95.Tag = "NUMBER";
            this.button95.Text = "0";
            this.button95.UseVisualStyleBackColor = true;
            // 
            // button96
            // 
            this.button96.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button96.Location = new System.Drawing.Point(131, 269);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(54, 37);
            this.button96.TabIndex = 73;
            this.button96.Tag = "NUMBER";
            this.button96.Text = "3";
            this.button96.UseVisualStyleBackColor = true;
            // 
            // button105
            // 
            this.button105.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button105.Location = new System.Drawing.Point(71, 269);
            this.button105.Name = "button105";
            this.button105.Size = new System.Drawing.Size(54, 37);
            this.button105.TabIndex = 72;
            this.button105.Tag = "NUMBER";
            this.button105.Text = "2";
            this.button105.UseVisualStyleBackColor = true;
            // 
            // button106
            // 
            this.button106.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button106.Location = new System.Drawing.Point(11, 269);
            this.button106.Name = "button106";
            this.button106.Size = new System.Drawing.Size(54, 37);
            this.button106.TabIndex = 71;
            this.button106.Tag = "NUMBER";
            this.button106.Text = "1";
            this.button106.UseVisualStyleBackColor = true;
            // 
            // button107
            // 
            this.button107.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button107.Location = new System.Drawing.Point(71, 312);
            this.button107.Name = "button107";
            this.button107.Size = new System.Drawing.Size(54, 37);
            this.button107.TabIndex = 70;
            this.button107.Tag = "NUMBER";
            this.button107.Text = "6";
            this.button107.UseVisualStyleBackColor = true;
            // 
            // button108
            // 
            this.button108.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button108.Location = new System.Drawing.Point(11, 312);
            this.button108.Name = "button108";
            this.button108.Size = new System.Drawing.Size(54, 37);
            this.button108.TabIndex = 69;
            this.button108.Tag = "NUMBER";
            this.button108.Text = "5";
            this.button108.UseVisualStyleBackColor = true;
            // 
            // button109
            // 
            this.button109.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button109.Location = new System.Drawing.Point(191, 269);
            this.button109.Name = "button109";
            this.button109.Size = new System.Drawing.Size(54, 37);
            this.button109.TabIndex = 68;
            this.button109.Tag = "NUMBER";
            this.button109.Text = "4";
            this.button109.UseVisualStyleBackColor = true;
            // 
            // button110
            // 
            this.button110.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button110.Location = new System.Drawing.Point(11, 355);
            this.button110.Name = "button110";
            this.button110.Size = new System.Drawing.Size(54, 37);
            this.button110.TabIndex = 67;
            this.button110.Tag = "NUMBER";
            this.button110.Text = "9";
            this.button110.UseVisualStyleBackColor = true;
            // 
            // button111
            // 
            this.button111.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button111.Location = new System.Drawing.Point(191, 311);
            this.button111.Name = "button111";
            this.button111.Size = new System.Drawing.Size(54, 37);
            this.button111.TabIndex = 66;
            this.button111.Tag = "NUMBER";
            this.button111.Text = "8";
            this.button111.UseVisualStyleBackColor = true;
            // 
            // button112
            // 
            this.button112.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button112.Location = new System.Drawing.Point(131, 311);
            this.button112.Name = "button112";
            this.button112.Size = new System.Drawing.Size(54, 37);
            this.button112.TabIndex = 65;
            this.button112.Tag = "NUMBER";
            this.button112.Text = "7";
            this.button112.UseVisualStyleBackColor = true;
            // 
            // InputPads
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1450, 879);
            this.Controls.Add(this.pnlDosage);
            this.Controls.Add(this.pnlBloodPressure);
            this.Controls.Add(this.btnCalendar);
            this.Controls.Add(this.pnlCalendar);
            this.Controls.Add(this.btnGCS);
            this.Controls.Add(this.pnlGCS);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.btnSymbols);
            this.Controls.Add(this.btnCalculator);
            this.Controls.Add(this.pnlKeyPad);
            this.Controls.Add(this.btnPositions);
            this.Controls.Add(this.pnlCalculator);
            this.Controls.Add(this.btnKeyboard);
            this.Controls.Add(this.pnlPositions);
            this.Controls.Add(this.btnDone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "InputPads";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.InputPads_Load);
            this.pnlKeyPad.ResumeLayout(false);
            this.pnlCalculator.ResumeLayout(false);
            this.grpUnits.ResumeLayout(false);
            this.grpUnits.PerformLayout();
            this.pnlPositions.ResumeLayout(false);
            this.pnlPositions.PerformLayout();
            this.pnlGCS.ResumeLayout(false);
            this.pnlGCS.PerformLayout();
            this.pnlCalendar.ResumeLayout(false);
            this.pnlBloodPressure.ResumeLayout(false);
            this.pnlBloodPressure.PerformLayout();
            this.pnlDosage.ResumeLayout(false);
            this.pnlDosage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlKeyPad;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button btnNormalColor;
        private System.Windows.Forms.Button btnReversedColor;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Panel pnlCalculator;
        private System.Windows.Forms.GroupBox grpUnits;
        private System.Windows.Forms.RadioButton optMG;
        private System.Windows.Forms.RadioButton optKG;
        private System.Windows.Forms.RadioButton optLBS;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.Panel pnlPositions;
        private System.Windows.Forms.Button button101;
        private System.Windows.Forms.Button button103;
        private System.Windows.Forms.Button button102;
        private System.Windows.Forms.Button btnKeyboard;
        private System.Windows.Forms.Button btnPositions;
        private System.Windows.Forms.Button btnCalculator;
        private System.Windows.Forms.Button btnSymbols;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlGCS;
        private System.Windows.Forms.Button button104;
        private System.Windows.Forms.Button btnGCS;
        private System.Windows.Forms.Panel pnlCalendar;
        private System.Windows.Forms.MonthCalendar mnuCalendar;
        private System.Windows.Forms.Button btnCalendar;
        private System.Windows.Forms.Button button3006;
        private System.Windows.Forms.Button button3005;
        private System.Windows.Forms.Button button2005;
        private System.Windows.Forms.Button button3001;
        private System.Windows.Forms.Button button3002;
        private System.Windows.Forms.Button button3003;
        private System.Windows.Forms.Button button3004;
        private System.Windows.Forms.Button button2001;
        private System.Windows.Forms.Button button2002;
        private System.Windows.Forms.Button button2003;
        private System.Windows.Forms.Button button2004;
        private System.Windows.Forms.Button button1001;
        private System.Windows.Forms.Button button1002;
        private System.Windows.Forms.Button button1003;
        private System.Windows.Forms.Button button1004;
        private System.Windows.Forms.Button button120;
        private System.Windows.Forms.Panel pnlBloodPressure;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDiastolic;
        private System.Windows.Forms.TextBox txtSystolic;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.RadioButton optMeters;
        private System.Windows.Forms.RadioButton optInches;
        private System.Windows.Forms.RadioButton optFeet;
        private System.Windows.Forms.Panel pnlDosage;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button button105;
        private System.Windows.Forms.Button button106;
        private System.Windows.Forms.Button button107;
        private System.Windows.Forms.Button button108;
        private System.Windows.Forms.Button button109;
        private System.Windows.Forms.Button button110;
        private System.Windows.Forms.Button button111;
        private System.Windows.Forms.Button button112;
        private System.Windows.Forms.TextBox txtWtInLbs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDoseInMg;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMgPerKg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtWtInKG;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.ListBox lstDosageLog;
    }
}