﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class OpenForms : Form
    {
        private int intFileNo = 0;


        public string strFormSelected = "";

        public OpenForms()
        {
            InitializeComponent();

            this.Load += new EventHandler(OpenForms_Load);
        }




        private void OpenForms_Load(object sender, EventArgs e)
        {

            SelectForm();

        }


        private void SelectForm()
        {
            strFormSelected = "";

            // get a list of files in the input parm
            string strPath = clsCommonRtns.strLastPath;
            if (strPath == "")
            {
                strPath = clsCommonRtns.strCurrentPath;
                if (strPath == "")
                {
                    // get app path
                    strPath = Application.ExecutablePath;
                    // remove the exe name
                    int intOffset = strPath.LastIndexOf("\\");
                    if (intOffset >= 0)
                    {
                        strPath = strPath.Substring(0, intOffset);
                    }
                    // add formfiles to the end
                    strPath = strPath + "\\formfiles";
                }
            }

            // get all the files in this path
            if (strPath.Length > 0)
            {
                int intMaxPerLine = (this.Width / (picFileTemplate.Width + 30));
                int intLine = 0;
                string strFormName = "";

                clsCommonRtns.strLastPath = strPath;
                List<string> lstFiles = clsCommonRtns.GetDirectoryFiles(strPath, "*.frm");  // + "\\formfiles");
                int intLeft = 0;
                int intTop = 0;
                int intFileNo = 0;
                intLine = 1;
                // now show the files
                foreach (string strFileName in lstFiles)
                {
                    PictureBox objPic = new PictureBox();
                    // open the file and get the image out of it...\
                    string strImageFileName = GetFormImage(strFileName);
                    if (strImageFileName.Length > 0)
                    {
                        // display the image
                        if (System.IO.File.Exists(strImageFileName))
                        {
                            objPic.Load(strImageFileName);
                            objPic.SizeMode = PictureBoxSizeMode.StretchImage;
                            objPic.Refresh();
                        }
                    }
                    // set the location
                    intFileNo++;
                    if (intFileNo > intMaxPerLine)
                    {
                        intLine++;
                        intFileNo = 1;
                    }
                    intTop = ((intLine - 1) * (picFileTemplate.Height + 5)) + 12;
                    intLeft = ((intFileNo - 1) * (picFileTemplate.Width + 30)) + 12;
                    objPic.Left = intLeft;
                    objPic.Top = intTop;
                    objPic.Width = picFileTemplate.Width;
                    objPic.Height = picFileTemplate.Height;
                    objPic.Tag = strFileName;
                    objPic.Visible = true;
                    objPic.Parent = pnlImages;
                    objPic.Click += new EventHandler(objPic_Click);
                    objPic.DoubleClick += new EventHandler(objPic_DoubleClick);
                    // put the label below the pic
                    int intOffset = strFileName.LastIndexOf("\\");
                    strFormName = strFileName.Substring(intOffset+1);
                    Label objLabel = new Label();
                    objLabel.Left = objPic.Left;
                    objLabel.Top = objPic.Top + objPic.Height + 1;
                    objLabel.Width = objPic.Width;
                    objLabel.Height = 16;
                    objLabel.Text = strFormName;
                    objLabel.Parent = pnlImages;
                    objLabel.Visible = true;
                }
            }

        }





        void objPic_DoubleClick(object sender, EventArgs e)
        {
            PictureBox objPic = (PictureBox)sender;
        
            // return the file to the calling routine
            strFormSelected = objPic.Tag.ToString();

            Hide();

        }



        void objPic_Click(object sender, EventArgs e)
        {

        }

        
        
        
        
        private string GetFormImage(string argstrFormFile)
        {
            string strImageFileName = "";
            int intStart = 0;
            int intEnd = 0;

            // it should be in the first 20 records...
            List<string> lstRecords = clsCommonRtns.ReadFile(argstrFormFile, 20);

            // look for the <image> tag...
            for (int n = 0; n < lstRecords.Count; n++)
            {
                intStart = lstRecords[n].ToUpper().IndexOf("<IMAGE>");
                if (intStart >= 0)
                {
                    // strip out the file
                    intEnd = lstRecords[n].ToUpper().IndexOf("</IMAGE>");
                    if (intEnd > intStart)
                    {
                        // strip it out
                        strImageFileName = lstRecords[n].Substring(intStart + 7, intEnd - intStart - 7);
                        // now strip out the path and put the path of the file
                        intStart = strImageFileName.LastIndexOf("\\");
                        if (intStart >= 0)
                        {
                            intEnd = argstrFormFile.LastIndexOf("\\");
                            if (intEnd >= 0)
                            {
                                strImageFileName = argstrFormFile.Substring(0,intEnd) + "\\images\\" + strImageFileName.Substring(intStart+1);
                            }
                        }
                    }
                    else
                    {
                        // take from start to end
                        strImageFileName = lstRecords[n].Substring(intStart + 7);
                    }
                    break;
                }
            }
            return strImageFileName;
        }




        private void btnBrowse_Click(object sender, EventArgs e)
        {

            // set the folder to load from
            if (clsCommonRtns.GetDirectoryPath() != "")
            {

                SelectForm();

            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            Close();

        }



    }
}
