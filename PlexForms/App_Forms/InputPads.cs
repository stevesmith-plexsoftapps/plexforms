﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class InputPads : Form
    {
        private bool bolFirstTime = true;
        private bool bolCapsOn = false;
        private bool bolShiftUp = false;
        private string strSelectedPad = "";
        private Control objControlSelected = null;

        private Control objFieldSelected;
        private int intCharPosition = -1;

        private string strBPPortionSelected = "S";

        public Control FieldSelected
        {
            get { return objFieldSelected; }
            set { objFieldSelected = value; }
        }


        public int CharPosition
        {
            get { return intCharPosition; }
            set { intCharPosition = value; }
        }



        public string Value
        {
            get { return txtDisplay.Text; }
            
        }




        public InputPads()
        {
            InitializeComponent();

        }




        public InputPads(string argstrInputPadType)
        {
            InitializeComponent();

            SetInputPad(argstrInputPadType);

        }



        public void SetInputPad(string argstrInputPadType)
        {

            if (bolFirstTime)
            {
                this.KeyDown += new KeyEventHandler(InputPads_KeyDown);
                this.KeyPress += new KeyPressEventHandler(InputPads_KeyPress);
                this.KeyPreview = true;
            }

            strSelectedPad = argstrInputPadType.ToUpper();

            ShowInputPad(strSelectedPad);

        }




        void InputPads_KeyPress(object sender, KeyPressEventArgs e)
        {
            int intKey = Convert.ToInt32(e.KeyChar);

            // check for special keys
            switch (intKey)
            {
                case 13:
                    ReturnToCaller();
                    break;

            }
        }


        
        
        
        
        void InputPads_KeyDown(object sender, KeyEventArgs e)
        {

            // check for special keys
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.SuppressKeyPress = true;
                    ReturnToCaller();
                    break;

            }
        }


        //********************************************************************
        //* Routine:        cmdButton_Click
        //* Description:    Process the click on the keypad button controls
        //* Arguments:      sender - the control that caused the event
        //*                 e - EventArgs object
        //* Returns:        Nothing
        //********************************************************************
        private void objButton_Click(object sender, EventArgs e)
        {
            Button objButton = (Button)sender;
            string strName = objButton.Name.ToUpper();
            string strKey = objButton.Text.ToString();
            string strTag = objButton.Tag.ToString().ToUpper();
            DataGridView objGrid = null;

            if (objFieldSelected != null)
            {

                if (objFieldSelected.GetType().Name.ToUpper() == "DATAGRIDVIEW")
                {
                    objGrid = (DataGridView)objFieldSelected;
                }

                txtDisplay.Text = objFieldSelected.Text;
                
                switch (strKey.ToUpper())
                {
                    case "BS":
                        ProcessBackSpace();
                        break;

                    case "SPACE":
                        txtDisplay.Text = txtDisplay.Text.Insert(intCharPosition, " ");
                        intCharPosition++;
                        break;

                    case "CLEAR":
                        ProcessClear();
                        break;

                    case "RETURN":
                        ReturnToCaller();
                        break;

                    case "TRANSFER TO FORM":
                        objFieldSelected.Text = txtDisplay.Text.Substring(0,txtDisplay.Text.Length - 1);
                        Close();
                        break;

                    case "CAPS":
                        for (int m = 0; m < pnlKeyPad.Controls.Count; m++)
                        {
                            if (pnlKeyPad.Controls[m].GetType().Name.ToUpper() == "BUTTON")
                            {
                                if (pnlKeyPad.Controls[m].Tag.ToString().ToUpper() == "LETTER")
                                {
                                    if (bolCapsOn)
                                    {
                                        // lower the letters
                                        pnlKeyPad.Controls[m].Text = pnlKeyPad.Controls[m].Text.ToLower();
                                    }
                                    else
                                    {
                                        pnlKeyPad.Controls[m].Text = pnlKeyPad.Controls[m].Text.ToUpper();
                                    }
                                }
                            }
                        }
                        bolCapsOn = !bolCapsOn;
                        break;

                    case "SHIFT":
                        for (int m = 0; m < pnlKeyPad.Controls.Count; m++)
                        {
                            if (pnlKeyPad.Controls[m].GetType().Name.ToUpper() == "BUTTON")
                            {
                                if (pnlKeyPad.Controls[m].Tag.ToString().ToUpper() == "LETTER")
                                {
                                    pnlKeyPad.Controls[m].Text = pnlKeyPad.Controls[m].Text.ToUpper();
                                }
                            }
                        }
                        bolShiftUp = true;
                        break;

                    default:
                        //if (intCharPosition > objFieldSelected.Text.Length)
                        //{
                        //    intCharPosition = objFieldSelected.Text.Length;
                        //}
                        //objFieldSelected.Text = objFieldSelected.Text.Insert(intCharPosition, strKey);
                        if (strTag == "POSITION")
                        {
                            txtDisplay.Text = strKey;
                            intCharPosition = strKey.Length;
                            ReturnToCaller();
                        }
                        else
                        {
                            // process a normal key
                            ProcessKey(strKey, strName);
                        }

                        if (strTag == "LETTER")
                        {
                            if (bolShiftUp)
                            {
                                for (int m = 0; m < pnlKeyPad.Controls.Count; m++)
                                {
                                    if (pnlKeyPad.Controls[m].GetType().Name.ToUpper() == "BUTTON")
                                    {
                                        if (pnlKeyPad.Controls[m].Tag.ToString().ToUpper() == "LETTER")
                                        {
                                            pnlKeyPad.Controls[m].Text = pnlKeyPad.Controls[m].Text.ToLower();
                                        }
                                    }
                                }
                            }
                            bolShiftUp = false;
                        }
                        break;
                }

            }

            if (objGrid != null)
            {
                objGrid.CurrentCell.Value = txtDisplay.Text;
            }
            else
            {
                objFieldSelected.Text = txtDisplay.Text;   //.Substring(0, txtDisplay.Text.Length - 1);
            }

        }



        private void ProcessKey(string argstrKey, string argstrName)
        {
            string strName = argstrName.ToUpper();
            string strKey = argstrKey;

            switch (strSelectedPad)
            {
                case "GCS":
                    // determine the positions
                    string strPosition = strName.Substring(6, 1);
                    string strValue = txtDisplay.Text + "   ";
                    switch (strPosition)
                    {
                        case "1":
                            strValue = strKey + strValue.Substring(1);
                            break;

                        case "2":
                            strValue = strValue.Substring(0, 1) + strKey + strValue.Substring(2);
                            break;

                        case "3":
                            strValue = strValue.Substring(0, 2) + strKey;
                            break;

                    }
                    txtDisplay.Text = strValue.Trim();
                    break;

                case "BLOODPRESSURE":
                    switch (strBPPortionSelected)
                    {
                        case "S":
                            // systolic field
                            txtSystolic.Text = txtSystolic.Text + strKey;
                            break;

                        case "D":
                            // diastolic field
                            txtDiastolic.Text = txtDiastolic.Text + strKey;
                            break;
                    }
                    txtDisplay.Text = txtSystolic.Text + "/" + txtDiastolic.Text;
                    break;


                case "DOSAGECALCULATOR":
                    // check to see what field has focus...
                    switch (strName)
                    {
                        case "TXTWTINLBS":
                            // calculate the kg for the lbs entered
                            txtWtInKG.Text = "";
                            break;

                        case "TXTWTINKG":
                            // calculate the lbs for the kg entered
                            break;

                        case "TXTMGPERKG":
                            // calculate the dose
                            break;

                    }

                    break;

                default:
                    txtDisplay.Text = txtDisplay.Text.Insert(intCharPosition, strKey);
                    intCharPosition += strKey.Length;
                    break;

            }

        }

     

        private void ProcessClear()
        {

            switch (strSelectedPad)
            {
                case "GCS":
                    break;

                case "BLOODPRESSURE":
                    switch (strBPPortionSelected)
                    {
                        case "S":
                            // systolic field
                            txtSystolic.Text = "";
                            break;

                        case "D":
                            // diastolic field
                            txtDiastolic.Text = "";
                            break;
                    }
                    txtDisplay.Text = txtSystolic.Text + "/" + txtDiastolic.Text;
                    break;

                default:
                    txtDisplay.Text = "|";
                    intCharPosition = 0;
                    break;
            }

        }




        private void ProcessBackSpace()
        {

            switch (strSelectedPad)
            {
                case "BLOODPRESSURE":
                    switch (strBPPortionSelected)
                    {
                        case "S":
                            // systolic field
                            if (txtSystolic.Text.Length > 1)
                            {
                                txtSystolic.Text = txtSystolic.Text.Substring(0, txtSystolic.Text.Length - 1);
                            }
                            else
                            {
                                txtSystolic.Text = "";
                            }
                            break;

                        case "D":
                            // diastolic field
                            if (txtDiastolic.Text.Length > 1)
                            {
                                txtDiastolic.Text = txtDiastolic.Text.Substring(0, txtDiastolic.Text.Length - 1);
                            }
                            else
                            {
                                txtDiastolic.Text = "";
                            }
                            break;
                    }
                    txtDisplay.Text = txtSystolic.Text + "/" + txtDiastolic.Text;
                    break;

                default:
                    if (intCharPosition > 0)
                    {
                        intCharPosition--;
                        txtDisplay.Text = txtDisplay.Text.Remove(intCharPosition, 1);
                    }
                    break;
            }

        }



        //********************************************************************
        //* Routine:        InputPads_Load
        //* Description:    Process the load event for the form
        //* Arguments:      sender - the control that caused the event
        //*                 e - EventArgs object
        //* Returns:        Nothing
        //********************************************************************
        private void InputPads_Load(object sender, EventArgs e)
        {
            string strKey = "";

            this.KeyPreview = true;

            intCharPosition = 0;
            txtDisplay.Text = "";

            if (objFieldSelected != null)
            {
                txtDisplay.Text = objFieldSelected.Text;
                intCharPosition = objFieldSelected.Text.Length;
                // set the BP values if this is a bloodpressure pad
                if (strSelectedPad == "BLOODPRESSURE")
                {
                    // split the value
                    string[] strBPValues = txtDisplay.Text.Split('/');
                    if (strBPValues.Length > 1)
                    {
                        txtSystolic.Text = strBPValues[0];
                        txtDiastolic.Text = strBPValues[1];
                    }
                    else
                    {
                        txtSystolic.Text = strBPValues[0];
                        txtDiastolic.Text = "";
                    }

                }

            }

            if (!bolFirstTime)
            {
                return;
            }


            // assign the buttonclick routine to all the buttons
            for (int n = 0; n < pnlKeyPad.Controls.Count; n++)
            {
                strKey = pnlKeyPad.Controls[n].GetType().Name.ToUpper();
                if (strKey == "BUTTON")
                {
                    // assign the click routine
                    pnlKeyPad.Controls[n].Click += new EventHandler(objButton_Click);
                }
            }

            // assign the buttonclick routine to all the buttons
            for (int n = 0; n < pnlPositions.Controls.Count; n++)
            {
                strKey = pnlPositions.Controls[n].GetType().Name.ToUpper();
                if (strKey == "BUTTON")
                {
                    // assign the click routine
                    pnlPositions.Controls[n].Click += new EventHandler(objButton_Click);
                }
            }


            // assign the buttonclick routine to all the buttons
            for (int n = 0; n < pnlCalculator.Controls.Count; n++)
            {
                strKey = pnlCalculator.Controls[n].GetType().Name.ToUpper();
                if (strKey == "BUTTON")
                {
                    // assign the click routine
                    pnlCalculator.Controls[n].Click += new EventHandler(objButton_Click);
                }
            }

            // assign the buttonclick routine to all the buttons
            for (int n = 0; n < pnlGCS.Controls.Count; n++)
            {
                strKey = pnlGCS.Controls[n].GetType().Name.ToUpper();
                if (strKey == "BUTTON")
                {
                    // assign the click routine
                    pnlGCS.Controls[n].Click += new EventHandler(objButton_Click);
                }
            }

            // assign the buttonclick routine to all the buttons
            txtDiastolic.Click += new EventHandler(txtDiastolic_Click);
            txtSystolic.Click += new EventHandler(txtSystolic_Click);
            for (int n = 0; n < pnlBloodPressure.Controls.Count; n++)
            {
                strKey = pnlBloodPressure.Controls[n].GetType().Name.ToUpper();
                if (strKey == "BUTTON")
                {
                    // assign the click routine
                    pnlBloodPressure.Controls[n].Click += new EventHandler(objButton_Click);
                }
            }

            bolFirstTime = false;

        }



        void txtSystolic_Click(object sender, EventArgs e)
        {
            // set this field with focus
            strBPPortionSelected = "S";
        }



        void txtDiastolic_Click(object sender, EventArgs e)
        {

            // set this field with focus
            strBPPortionSelected = "D";

        }




        private void txtDisplay_TextChanged(object sender, EventArgs e)
        {

            // set this field with focus
            strBPPortionSelected = "S";

        }



        private void btnDone_Click(object sender, EventArgs e)
        {

            ReturnToCaller();

        }



        private void ReturnToCaller()
        {
            // set the input field to the txtdisplay value
            if (objFieldSelected != null)
            {
                // set the BP values if this is a bloodpressure pad
                if (strSelectedPad == "BLOODPRESSURE")
                {
                    txtDisplay.Text = txtSystolic.Text + "/" + txtDiastolic.Text;
                }
                objFieldSelected.Text = txtDisplay.Text;
            }
            Close();
        }




        private void ShowInputPad(string argstrInputPad)
        {
            Panel objPanel = null;

            txtDisplay.Visible = false;

            switch (argstrInputPad.ToUpper())
            {
                case "KEYPAD":
                    objPanel = pnlKeyPad;
                    txtDisplay.Visible = true;
                    break;

                case "CALCULATOR":
                    objPanel = pnlCalculator;
                    txtDisplay.Visible = true;
                    break;

                case "DOSAGECALCULATOR":
                    objPanel = pnlDosage;
                    txtDisplay.Visible = false;
                    break;

                case "POSITIONS":
                    objPanel = pnlPositions;
                    txtDisplay.Visible = false;
                    break;

                case "SYMBOLS":
                    break;

                case "GCS":
                    objPanel = pnlGCS;
                    txtDisplay.Visible = true;
                    break;

                case "BLOODPRESSURE":
                    objPanel = pnlBloodPressure;
                    txtDisplay.Visible = false;
                    break;

                case "CALENDAR":
                    objPanel = pnlCalendar;
                    txtDisplay.Visible = false;
                    break;

                default:
                    objPanel = null;
                    break;

            }

            if (objPanel != null)
            {
                HideInputPads();
                objPanel.Visible = true;
                objPanel.Left = 2;
                objPanel.Top = btnDone.Top + btnDone.Height + 2;
                this.Width = objPanel.Width + 8;
                this.Height = objPanel.Top + objPanel.Height + 20;
                txtDisplay.Width = this.Width - txtDisplay.Left - 20;
                //this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height - 10;
                //this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
    
        }



        private void HideInputPads()
        {
            // hide all the panels
            for (int n = 0; n < this.Controls.Count; n++)
            {
                if (this.Controls[n].GetType().Name.ToUpper() == "PANEL")
                {
                    this.Controls[n].Visible = false;
                }
            }
        }




        private void btnKeyboard_Click(object sender, EventArgs e)
        {

            ShowInputPad("KEYPAD");

        }

        private void btnPositions_Click(object sender, EventArgs e)
        {

            ShowInputPad("Positions");

        }

        private void btnCalculator_Click(object sender, EventArgs e)
        {

            ShowInputPad("Calculator");

        }

        private void btnSymbols_Click(object sender, EventArgs e)
        {

            ShowInputPad("Symbols");

        }

        private void pnlEyes_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnImages_Click(object sender, EventArgs e)
        {

            ShowInputPad("GCS");

        }

        private void btnCalendar_Click(object sender, EventArgs e)
        {

            ShowInputPad("Calendar");

        }

        private void txtWtInLbs_TextChanged(object sender, EventArgs e)
        {
            objControlSelected = txtWtInLbs;
        }

        private void txtWtInKG_TextChanged(object sender, EventArgs e)
        {
            objControlSelected = txtWtInKG;
        }

        private void txtMgPerKg_TextChanged(object sender, EventArgs e)
        {
            objControlSelected = txtMgPerKg;
        }

        private void txtDoseInMg_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void mnuCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            // set the display textbox
            txtDisplay.Text = mnuCalendar.SelectionStart.ToShortDateString();
            ReturnToCaller();

        }


    }
}
