﻿namespace PlexForms
{
    partial class FieldProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabFieldProperties = new System.Windows.Forms.TabControl();
            this.tabPageFields = new System.Windows.Forms.TabPage();
            this.tabPageRules = new System.Windows.Forms.TabPage();
            this.btnEditRules = new System.Windows.Forms.Button();
            this.txtRules = new System.Windows.Forms.RichTextBox();
            this.grdAttributes = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFormName = new System.Windows.Forms.TextBox();
            this.tbrMain = new System.Windows.Forms.ToolStrip();
            this.tbrMainExit = new System.Windows.Forms.ToolStripButton();
            this.tbrMainSave = new System.Windows.Forms.ToolStripButton();
            this.tabFieldProperties.SuspendLayout();
            this.tabPageFields.SuspendLayout();
            this.tabPageRules.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).BeginInit();
            this.tbrMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabFieldProperties
            // 
            this.tabFieldProperties.Controls.Add(this.tabPageFields);
            this.tabFieldProperties.Controls.Add(this.tabPageRules);
            this.tabFieldProperties.Location = new System.Drawing.Point(10, 70);
            this.tabFieldProperties.Name = "tabFieldProperties";
            this.tabFieldProperties.SelectedIndex = 0;
            this.tabFieldProperties.Size = new System.Drawing.Size(785, 372);
            this.tabFieldProperties.TabIndex = 0;
            // 
            // tabPageFields
            // 
            this.tabPageFields.Controls.Add(this.grdAttributes);
            this.tabPageFields.Location = new System.Drawing.Point(4, 22);
            this.tabPageFields.Name = "tabPageFields";
            this.tabPageFields.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFields.Size = new System.Drawing.Size(777, 346);
            this.tabPageFields.TabIndex = 0;
            this.tabPageFields.Text = "Fields";
            this.tabPageFields.UseVisualStyleBackColor = true;
            // 
            // tabPageRules
            // 
            this.tabPageRules.Controls.Add(this.btnEditRules);
            this.tabPageRules.Controls.Add(this.txtRules);
            this.tabPageRules.Location = new System.Drawing.Point(4, 22);
            this.tabPageRules.Name = "tabPageRules";
            this.tabPageRules.Size = new System.Drawing.Size(777, 374);
            this.tabPageRules.TabIndex = 2;
            this.tabPageRules.Text = "Rules";
            this.tabPageRules.UseVisualStyleBackColor = true;
            // 
            // btnEditRules
            // 
            this.btnEditRules.Location = new System.Drawing.Point(521, 344);
            this.btnEditRules.Name = "btnEditRules";
            this.btnEditRules.Size = new System.Drawing.Size(61, 28);
            this.btnEditRules.TabIndex = 1;
            this.btnEditRules.Text = "Edit";
            this.btnEditRules.UseVisualStyleBackColor = true;
            this.btnEditRules.Click += new System.EventHandler(this.btnEditRules_Click);
            // 
            // txtRules
            // 
            this.txtRules.AcceptsTab = true;
            this.txtRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRules.Location = new System.Drawing.Point(3, 3);
            this.txtRules.Name = "txtRules";
            this.txtRules.Size = new System.Drawing.Size(592, 331);
            this.txtRules.TabIndex = 0;
            this.txtRules.Text = "";
            // 
            // grdAttributes
            // 
            this.grdAttributes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAttributes.Location = new System.Drawing.Point(9, 6);
            this.grdAttributes.Name = "grdAttributes";
            this.grdAttributes.Size = new System.Drawing.Size(765, 318);
            this.grdAttributes.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Form Name:";
            // 
            // txtFormName
            // 
            this.txtFormName.Location = new System.Drawing.Point(77, 44);
            this.txtFormName.Name = "txtFormName";
            this.txtFormName.Size = new System.Drawing.Size(333, 20);
            this.txtFormName.TabIndex = 4;
            // 
            // tbrMain
            // 
            this.tbrMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tbrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbrMainSave,
            this.tbrMainExit});
            this.tbrMain.Location = new System.Drawing.Point(0, 0);
            this.tbrMain.Name = "tbrMain";
            this.tbrMain.Size = new System.Drawing.Size(809, 38);
            this.tbrMain.TabIndex = 5;
            this.tbrMain.Text = "toolStrip1";
            // 
            // tbrMainExit
            // 
            this.tbrMainExit.Image = global::PlexForms.Properties.Resources.exit2;
            this.tbrMainExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrMainExit.Name = "tbrMainExit";
            this.tbrMainExit.Size = new System.Drawing.Size(29, 35);
            this.tbrMainExit.Text = "Exit";
            this.tbrMainExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrMainExit.Click += new System.EventHandler(this.tbrMainExit_Click);
            // 
            // tbrMainSave
            // 
            this.tbrMainSave.Image = global::PlexForms.Properties.Resources.Save;
            this.tbrMainSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrMainSave.Name = "tbrMainSave";
            this.tbrMainSave.Size = new System.Drawing.Size(35, 35);
            this.tbrMainSave.Text = "Save";
            this.tbrMainSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrMainSave.Click += new System.EventHandler(this.tbrMainSave_Click);
            // 
            // FieldProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 477);
            this.Controls.Add(this.tbrMain);
            this.Controls.Add(this.txtFormName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabFieldProperties);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FieldProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Properties";
            this.tabFieldProperties.ResumeLayout(false);
            this.tabPageFields.ResumeLayout(false);
            this.tabPageRules.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).EndInit();
            this.tbrMain.ResumeLayout(false);
            this.tbrMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabFieldProperties;
        private System.Windows.Forms.TabPage tabPageFields;
        private System.Windows.Forms.TabPage tabPageRules;
        private System.Windows.Forms.RichTextBox txtRules;
        private System.Windows.Forms.Button btnEditRules;
        private System.Windows.Forms.DataGridView grdAttributes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFormName;
        private System.Windows.Forms.ToolStrip tbrMain;
        private System.Windows.Forms.ToolStripButton tbrMainSave;
        private System.Windows.Forms.ToolStripButton tbrMainExit;
    }
}