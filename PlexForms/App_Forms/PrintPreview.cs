﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class PrintPreview : Form
    {

        private Bitmap objBMP = null;
        public Bitmap PrintPreviewBMP
        {
            get { return objBMP; }
            set { objBMP = value; }
        }


        public PrintPreview(Bitmap argobjBMP)
        {
            InitializeComponent();

            this.Resize += new EventHandler(PrintPreview_Resize);
            picPrintPreview.Image = (Image)argobjBMP;

        }



        void PrintPreview_Resize(object sender, EventArgs e)
        {
            // resize and place the panel
            pnlPrintPreview.Width = this.Width - 20;
            pnlPrintPreview.Left = 0;
            pnlPrintPreview.Top = 10;
            pnlPrintPreview.Height = this.Height - 40;

            picPrintPreview.Left = 0;
            picPrintPreview.Top = 0;

        }

    }
}
