﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace PlexForms
{
    public partial class Configuration : Form
    {

        public Configuration()
        {
            InitializeComponent();

            this.Load += new EventHandler(Configuration_Load);

        }



        void Configuration_Load(object sender, EventArgs e)
        {
            int intRow = 0;

            clsCommonRtns.ReadConfigFile();

            grdSettings.Rows.Clear();
            grdSettings.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            grdSettings.Columns[0].Width = 600;
            //grdSettings.Columns[0].ReadOnly = true;
            grdSettings.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            grdSettings.Columns[1].Width = 600;

            for (int n = 0; n < clsCommonRtns.lstConfigSettings.Count; n++)
            {
                grdSettings.Rows.Add(clsCommonRtns.lstConfigSettings[n].strKey, clsCommonRtns.lstConfigSettings[n].strValue);
            }

        }




        private void btnOK_Click(object sender, EventArgs e)
        {

            // replace the values in the config settings
            clsCommonRtns.lstConfigSettings.Clear();

            for (int n = 0; n < grdSettings.Rows.Count; n++)
            {
                if (grdSettings.Rows[n].Cells[0].Value != null)
                {
                    string strKey = grdSettings.Rows[n].Cells[0].Value.ToString();

                    if (strKey.Trim() != "")
                    {
                        clsCommonRtns.udtConfigSettings objConfigSetting = new clsCommonRtns.udtConfigSettings();
                        objConfigSetting.strKey = strKey;
                        objConfigSetting.strValue = grdSettings.Rows[n].Cells[1].Value.ToString();
                        clsCommonRtns.lstConfigSettings.Add(objConfigSetting);
                    }
                }
            }

            // write the new stuff
            clsCommonRtns.WriteConfigFile();

            Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            Close();

        }

        private void btnDirectory_Click(object sender, EventArgs e)
        {
            int intRow = grdSettings.CurrentCell.RowIndex;
            
            // get the directory
            string strDirectory = clsCommonRtns.GetDirectoryPath();
            if (strDirectory.Length > 0)
            {
                // fill in the value for the current row
                grdSettings.Rows[intRow].Cells[1].Value = strDirectory;
                grdSettings.Refresh();
            }
        }

        private void btnFile_Click(object sender, EventArgs e)
        {

            int intRow = grdSettings.CurrentCell.RowIndex;

            // get the directory
            string strFileName = clsCommonRtns.GetFile();
            if (strFileName.Length > 0)
            {
                // fill in the value for the current row
                grdSettings.Rows[intRow].Cells[1].Value = strFileName;
                grdSettings.Refresh();
            }

        }

    }
}
