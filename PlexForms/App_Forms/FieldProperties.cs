﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class FieldProperties : Form
    {

        private Control _selectedPallet = null;
        public Control SelectedPallet
        {
            get { return _selectedPallet; }
            set { _selectedPallet = value; }
        }

        private Control _selectedControl = null;
        public Control SelectedControl
        {
            get { return _selectedControl; }
            set { _selectedControl = value; }
        }

        private Control _selectedForm = null;
        public Control SelectedForm
        {
            get { return _selectedForm; }
            set { _selectedForm = value; }
        }

        private List<clsFormInputRtns.udtRules> _selectedControlRules = new List<clsFormInputRtns.udtRules>();
        public List<clsFormInputRtns.udtRules> SelectedControlRules
        {
            get { return _selectedControlRules; }
            set { _selectedControlRules = value; }
        }



        public FieldProperties()
        {
            InitializeComponent();

            Initialize();

        }


        private void Initialize()
        {

            this.Resize += new EventHandler(FieldProperties_Resize);
            this.Load += new EventHandler(FieldProperties_Load);

            // create the columns on the grid
            grdAttributes.Columns.Add("Name", "Name");
            grdAttributes.Columns["Name"].Width = 100;
            grdAttributes.Columns.Add("Type", "Type");
            grdAttributes.Columns["Type"].Width = 100;
            grdAttributes.Columns.Add("Length", "Length");
            grdAttributes.Columns["Length"].Width = 100;
            grdAttributes.Columns.Add("Left", "Left");
            grdAttributes.Columns["Left"].Width = 100;
            grdAttributes.Columns.Add("Top", "Top");
            grdAttributes.Columns["Top"].Width = 100;
            grdAttributes.Columns.Add("TabIndex", "TabIndex");
            grdAttributes.Columns["TabIndex"].Width = 100;
            grdAttributes.Columns.Add("Value", "Value");
            grdAttributes.Columns["Value"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
       
        }




        /// <summary>
        /// FieldProperties_Load  - 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FieldProperties_Load(object sender, EventArgs e)
        {
            int intRow = 0;

            tabFieldProperties.SelectedIndex = 0;

            // load the fields with the values from the selected control
            if (SelectedControl != null)
            {
                try
                {
                    txtFormName.Text = SelectedForm.Name;

                    for (int n = 0; n < SelectedPallet.Controls.Count; n++)
                    {
                        // put in the attributes
                        grdAttributes.Rows.Add();

                        SelectedControl = SelectedPallet.Controls[n];

                        grdAttributes.Rows[n].Cells["Name"].Value = SelectedControl.Name;
                        grdAttributes.Rows[n].Cells["Type"].Value = SelectedControl.GetType().Name;
                        grdAttributes.Rows[n].Cells["Length"].Value = SelectedControl.Width;
                        grdAttributes.Rows[n].Cells["Left"].Value = SelectedControl.Left;
                        grdAttributes.Rows[n].Cells["Top"].Value = SelectedControl.Top;
                        grdAttributes.Rows[n].Cells["TabIndex"].Value = SelectedControl.TabIndex.ToString();
                        grdAttributes.Rows[n].Cells["Value"].Value = SelectedControl.Text;
                    }

                    // get any rules for this control
                    List<string> lstRules = clsFormInputRtns.GetFieldRules(SelectedForm.Text, SelectedControl.Name, "");

                    // if any rules are in there for this control, then load them
                    txtRules.Text = "";
                    for (int m = 0; m < lstRules.Count; m++)
                    {
                        txtRules.Text = txtRules.Text + lstRules[m].ToString() + "\r\n";
                    }
                }
                catch (Exception ex)
                {
                    // an error occurred

                }
            }

        }





        void FieldProperties_Resize(object sender, EventArgs e)
        {

            // resize the controls
            tabFieldProperties.Top = txtFormName.Top + txtFormName.Height + 10;
            tabFieldProperties.Left = 10;
            tabFieldProperties.Width = this.Width - (tabFieldProperties.Left * 2);
            tabFieldProperties.Height = this.Height - tabFieldProperties.Top - 40;

            // resize the tab pages
            for (int n = 0; n < tabFieldProperties.TabPages.Count; n++)
            {
                tabFieldProperties.TabPages[n].Width = tabFieldProperties.Width - (tabFieldProperties.TabPages[n].Left * 2);
                tabFieldProperties.TabPages[n].Height = tabFieldProperties.Height - tabFieldProperties.TabPages[n].Top - 30;
            }

            // resize the attributes grid
            grdAttributes.Width = tabFieldProperties.Width - (2 * grdAttributes.Left) - 20;
            grdAttributes.Height = tabFieldProperties.Height - grdAttributes.Top - 30;

            // resize the controls in the tab pages
            txtRules.Width = tabFieldProperties.TabPages["Rules"].Width - (txtRules.Left * 2);
            txtRules.Height = tabFieldProperties.TabPages["Rules"].Height - txtRules.Top - 30;

        }




        private void Save()
        {
            string strType = SelectedControl.GetType().Name.ToUpper();
            DataGridView objGrid = null;

            // the grid is in the order of the controls

            // save the values back to the control
            for (int n = 0; n < grdAttributes.Rows.Count; n++)
            {
                if (grdAttributes.Rows[n].Cells["Name"].Value != null)
                {
                    SelectedPallet.Controls[n].Name = grdAttributes.Rows[n].Cells["Name"].Value.ToString();
                    SelectedPallet.Controls[n].Width = clsCommonRtns.ConvertToInt(grdAttributes.Rows[n].Cells["Length"].Value.ToString());
                    SelectedPallet.Controls[n].Left = clsCommonRtns.ConvertToInt(grdAttributes.Rows[n].Cells["Left"].Value.ToString());
                    SelectedPallet.Controls[n].Top = clsCommonRtns.ConvertToInt(grdAttributes.Rows[n].Cells["Top"].Value.ToString());
                    SelectedPallet.Controls[n].TabIndex = clsCommonRtns.ConvertToInt(grdAttributes.Rows[n].Cells["TabIndex"].Value.ToString());
                    SelectedPallet.Controls[n].Text = grdAttributes.Rows[n].Cells["Value"].Value.ToString();
                }
            }

            // now are there any rules?
            if (txtRules.Text.Trim() != "")
            {
                clsFormInputRtns.SetFieldRules(SelectedForm.Text, SelectedControl.Name, "", txtRules.Text);
            }

            Close();

        }





        private void Cancel()
        {

            Close();

        }

        private void btnEditRules_Click(object sender, EventArgs e)
        {

            EditRules frmEditRules = new EditRules();
            frmEditRules.ShowDialog(this);

        }

        private void tbrMainSave_Click(object sender, EventArgs e)
        {

            Save();

        }

        private void tbrMainExit_Click(object sender, EventArgs e)
        {

            Cancel();

        }

    }
}
