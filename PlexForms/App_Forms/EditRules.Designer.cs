﻿namespace PlexForms
{
    partial class EditRules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboEvents = new System.Windows.Forms.ComboBox();
            this.lstRules = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Event:";
            // 
            // cboEvents
            // 
            this.cboEvents.FormattingEnabled = true;
            this.cboEvents.Items.AddRange(new object[] {
            "On Entry",
            "On Exit",
            "When Clicked",
            "When Double Clicked",
            "Email"});
            this.cboEvents.Location = new System.Drawing.Point(51, 20);
            this.cboEvents.Name = "cboEvents";
            this.cboEvents.Size = new System.Drawing.Size(303, 21);
            this.cboEvents.TabIndex = 1;
            // 
            // lstRules
            // 
            this.lstRules.FormattingEnabled = true;
            this.lstRules.Location = new System.Drawing.Point(10, 58);
            this.lstRules.Name = "lstRules";
            this.lstRules.Size = new System.Drawing.Size(345, 329);
            this.lstRules.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(361, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 380);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(85, 316);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(70, 43);
            this.button13.TabIndex = 13;
            this.button13.Text = "CopyFrom";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Visible = false;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(9, 316);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(70, 43);
            this.button14.TabIndex = 12;
            this.button14.Text = "CopyTo";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Visible = false;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(85, 267);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(70, 43);
            this.button11.TabIndex = 11;
            this.button11.Text = "CopyFrom";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(9, 267);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(70, 43);
            this.button12.TabIndex = 10;
            this.button12.Text = "CopyTo";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Visible = false;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(84, 218);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(70, 43);
            this.button9.TabIndex = 9;
            this.button9.Text = "CopyFrom";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(8, 218);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(70, 43);
            this.button10.TabIndex = 8;
            this.button10.Text = "CopyTo";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(83, 169);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(70, 43);
            this.button7.TabIndex = 7;
            this.button7.Text = "CopyFrom";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(7, 169);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(70, 43);
            this.button8.TabIndex = 6;
            this.button8.Text = "CopyTo";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(83, 117);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(70, 43);
            this.button5.TabIndex = 5;
            this.button5.Text = "Ask";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(7, 117);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(70, 43);
            this.button6.TabIndex = 4;
            this.button6.Text = "Set";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(83, 68);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(70, 43);
            this.button3.TabIndex = 3;
            this.button3.Text = "Calculate";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(7, 68);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(70, 43);
            this.button4.TabIndex = 2;
            this.button4.Text = "Email";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 43);
            this.button2.TabIndex = 1;
            this.button2.Text = "CopyFrom";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 43);
            this.button1.TabIndex = 0;
            this.button1.Text = "CopyTo";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(446, 396);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(70, 30);
            this.button15.TabIndex = 14;
            this.button15.Text = "Done";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(370, 396);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(70, 30);
            this.button16.TabIndex = 15;
            this.button16.Text = "Cancel";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // EditRules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 434);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstRules);
            this.Controls.Add(this.cboEvents);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditRules";
            this.Text = "EditRules";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEvents;
        private System.Windows.Forms.ListBox lstRules;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
    }
}