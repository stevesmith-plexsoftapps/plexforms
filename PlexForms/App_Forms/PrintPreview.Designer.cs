﻿namespace PlexForms
{
    partial class PrintPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPrintPreview = new System.Windows.Forms.Panel();
            this.picPrintPreview = new System.Windows.Forms.PictureBox();
            this.pnlPrintPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrintPreview
            // 
            this.pnlPrintPreview.AutoScroll = true;
            this.pnlPrintPreview.Controls.Add(this.picPrintPreview);
            this.pnlPrintPreview.Location = new System.Drawing.Point(37, 88);
            this.pnlPrintPreview.Name = "pnlPrintPreview";
            this.pnlPrintPreview.Size = new System.Drawing.Size(200, 100);
            this.pnlPrintPreview.TabIndex = 1;
            // 
            // picPrintPreview
            // 
            this.picPrintPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picPrintPreview.Location = new System.Drawing.Point(13, 13);
            this.picPrintPreview.Name = "picPrintPreview";
            this.picPrintPreview.Size = new System.Drawing.Size(100, 50);
            this.picPrintPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPrintPreview.TabIndex = 1;
            this.picPrintPreview.TabStop = false;
            // 
            // PrintPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.pnlPrintPreview);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrintPreview";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPreview";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlPrintPreview.ResumeLayout(false);
            this.pnlPrintPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPrintPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPrintPreview;
        private System.Windows.Forms.PictureBox picPrintPreview;

    }
}