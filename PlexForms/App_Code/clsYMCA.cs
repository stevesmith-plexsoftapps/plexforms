﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public class clsYMCA
    {

        private TabControl tabForms = null;
        private string strSelectedForm = "";
        private string strEvent = "";
        private Control objSelectedControl = null;
        private TabPage objSelectedTabPage = null;
        private PictureBox objSelectedPallet = null;
        private InputPads objInputPads = new InputPads();

        public string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";

            // set the global objects
            tabForms = argtabForms;
            strSelectedForm = argstrForm;
            strEvent = argstrEvent;
            if (strEvent == "LOAD")
            {
                // load event, ignore the other fields
            }
            else
            {
                objSelectedTabPage = tabForms.TabPages[strSelectedForm];
                objSelectedPallet = (PictureBox)objSelectedTabPage.Controls[0];
                objSelectedControl = argobjControl;
            }

            // process for the specified form
            switch (strSelectedForm.ToUpper())
            {
                case "YMCA":
                    // form group level
                    strResult = ProcessYMCA();
                    break;

                case "VISITOR":
                    // white form
                    strResult = ProcessVisitor();
                    break;

                case "ENROLLMENT":
                    // blue form
                    strResult = ProcessEnrollment();
                    break;

            }

            return strResult;

        }



        /// <summary>
        /// Form level rules
        /// </summary>
        /// <returns></returns>
        private string ProcessYMCA()
        {
            string strResult = "";

            return strResult;

        }




        /// <summary>
        /// Visitor page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessVisitor()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    strResult = ProcessVisitor_GetFocus();
                    break;

                case "LOSTFOCUS":
                    strResult = ProcessVisitor_LostFocus();
                    break;

                case "ENTER":
                    break;

            }

            return strResult;
        }


        /// <summary>
        /// ProcessVisitor_GetFocus - Visitor page rules when a control gets focus
        /// </summary>
        /// <returns></returns>
        private string ProcessVisitor_GetFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            CheckBox objCheckBox = null;
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // for text controls, set the pad to keypad
            if (strControlType == "TEXTBOX")
            {
                strPad = "KEYPAD";
            }

            // see what control it is
            switch (strControlName)
            {

                case "VISIT_DATE":
                    strPad = "CALENDAR";
                    break;

                case "NAME":
                    break;

                case "MALE":
                    if (strControlType == "CHECKBOX")
                    {
                        objCheckBox = (CheckBox)objSelectedControl;
                        if (objCheckBox.Checked)
                        {
                            clsFormInputRtns.SetField("VISITOR", "FEMALE", "");
                            clsFormInputRtns.SetField("ENROLLMENT", "GENDER", "M");
                        }
                    }
                    break;

                case "FEMALE":
                    if (strControlType == "CHECKBOX")
                    {
                        objCheckBox = (CheckBox)objSelectedControl;
                        if (objCheckBox.Checked)
                        {
                            clsFormInputRtns.SetField("VISITOR", "MALE", "");
                            clsFormInputRtns.SetField("ENROLLMENT", "GENDER", "F");
                        }
                    }
                    break;

                case "ACTIVITY_STATUS":
                    break;

                case "PREVIOUSLY_ACTIVE_STATUS":
                    break;

                case "FIRST_TIME_STATUS":
                    break;

            }

            if (clsFormInputRtns.strInputStyle != "TOUCH")
            {
                strPad = "";
            }

            if (strPad != "")
            {
                strResult = "";
                // call the inputpad routines
                objInputPads.SetInputPad(strPad);
                objInputPads.FieldSelected = objSelectedControl;
                objInputPads.CharPosition = 0;
                objInputPads.ShowDialog();
                strResult = objInputPads.Value;
            }

            return strResult;

        }



        /// <summary>
        /// ProcessVisitor_LostFocus - Visitor page rules when a control loses focus
        /// </summary>
        /// <returns></returns>
        private string ProcessVisitor_LostFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            CheckBox objCheckBox = null;
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // see which control
            // see what control it is
            switch (strControlName)
            {
                case "NAME":
                    // break up the name
                    // figure the first part is the first name, the middle the middle name and the last the last name
                    // split the name on spaces
                    string[] astrName = (strControlValue + "  ").Split(' ');
                    int intoffset = strControlValue.IndexOf(' ');
                    string strFirstName = astrName[0].Trim();
                    string strMiddleName = "";
                    string strLastName = "";
                    if (astrName.Length > 1)
                    {
                        int intStart = 1;
                        if (astrName[1].IndexOf('.') >= 0)
                        {
                            strMiddleName = astrName[1].Replace(".", "");
                            intStart = 2;
                        }
                        for (int n = intStart; n < astrName.Length; n++)
                        {
                            strLastName = strLastName + astrName[n];
                        }
                    }
                    clsFormInputRtns.SetField("ENROLLMENT", "FIRST_NAME", strFirstName);
                    clsFormInputRtns.SetField("ENROLLMENT", "MIDDLE_INITIAL", strMiddleName);
                    clsFormInputRtns.SetField("ENROLLMENT", "LAST_NAME", strLastName);
                    break;

                case "MALE":
                    break;

                case "FEMALE":
                    break;

                case "DATE_OF_BIRTH":
                    clsFormInputRtns.SetField("ENROLLMENT", "DATE_OF_BIRTH", strControlValue);
                    break;

                case "EMAIL_ADDRESS":
                    clsFormInputRtns.SetField("ENROLLMENT", "EMAIL_ADDRESS", strControlValue);
                    break;

                case "ADDRESS":
                    clsFormInputRtns.SetField("ENROLLMENT", "ADDRESS", strControlValue);
                    break;

                case "CITY":
                    string strState = clsFormInputRtns.GetField("VISITOR", "STATE").ToString();
                    clsFormInputRtns.SetField("ENROLLMENT", "CITY", strControlValue + ", " + strState);
                    break;

                case "ZIPCODE":
                    clsFormInputRtns.SetField("ENROLLMENT", "ZIPCODE", strControlValue);
                    break;

                case "STATE":
                    clsFormInputRtns.SetField("ENROLLMENT", "STATE", strControlValue);
                    break;

                case "ACTIVITY_STATUS":
                    if (strControlValue == "X")
                    {
                        clsFormInputRtns.SetField("VISITOR", "PREVIOUSLY_ACTIVE_STATUS", "");
                        clsFormInputRtns.SetField("VISITOR", "FIRST_TIME_STATUS", "");
                    }
                    break;

                case "PREVIOUSLY_ACTIVE_STATUS":
                    if (strControlValue == "X")
                    {
                        clsFormInputRtns.SetField("VISITOR", "ACTIVE_STATUS", "");
                        clsFormInputRtns.SetField("VISITOR", "FIRST_TIME_STATUS", "");
                    }
                    break;

                case "FIRST_TIME_STATUS":
                    if (strControlValue == "X")
                    {
                        clsFormInputRtns.SetField("VISITOR", "ACTIVE_STATUS", "");
                        clsFormInputRtns.SetField("VISITOR", "PREVIOUSLY_ACTIVE_STATUS", "");
                    }
                    break;

            }

            return strResult;
        }




        /// <summary>
        /// Consent page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessEnrollment()
        {
            string strResult = "";

            return strResult;

        }


    }
}
