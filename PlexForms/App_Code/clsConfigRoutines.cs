﻿using System;
using System.Data;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Xml;

namespace PlexForms
{
    class clsConfigRoutines
    {
        public struct udtConfigSettings
        {
            public string strKey;
            public string strValue;
            public string strOriginalValue;

        }

        public static List<udtConfigSettings> lstConfigSettings = new List<udtConfigSettings>();
        public static udtConfigSettings objConfigSetting = new udtConfigSettings();
        public static List<string> lstConfigRecords = new List<string>();

        // this list will contain the settings for the application
//        public static List<string[]> lstSettings = new List<string[]>();


        /// <summary>
        /// Add an app setting
        /// </summary>
        /// <param name="argstrKey, type=string"></param>
        /// <param name="argstrValue, type=string"></param>
        /// <returns>number of settings added so far, type=int</returns>
        public static int AddSetting(string argstrKey, string argstrValue)
        {
            bool bolFound = false;

            if (SettingExists(argstrKey))
            {
                // replace the value
                SetSetting(argstrKey, argstrValue);
            }
            else 
            {
                // add the setting
                objConfigSetting.strKey = argstrKey;
                objConfigSetting.strValue = argstrValue;
                objConfigSetting.strOriginalValue = argstrValue;
                lstConfigSettings.Add(objConfigSetting);
            }

            return lstConfigSettings.Count;
        }






        //*********************************************
        //* Read the config file in and load the 
        //* configuration records
        //*********************************************

        /// <summary>
        /// Read the config file in and load the configuration records
        /// </summary>
        /// <returns></returns>
        public static int ReadConfigFile()
        {
            String strFileName = "";
            int intNumberOfLinesRead = 0;
            string strKey = "";
            string strValue = "";
            bool bolUserSettings = false;
            bool bolSetting = false;
            int intOffset = 0;
            int intOffset2 = 0;
            string strBuffer = "";
            string strCurrentPath = Application.ExecutablePath;

            // remove the exe name
            intOffset = strCurrentPath.LastIndexOf("\\");
            if (intOffset >= 0)
            {
                strCurrentPath = strCurrentPath.Substring(0, intOffset);
            }

            strFileName = strCurrentPath + "\\" + clsCommonRtns.GetEXEName() + ".config";

            // clear the array
            lstConfigSettings.Clear();

            lstConfigRecords = clsCommonRtns.ReadFile(strFileName);

            for (int n = 0; n < lstConfigRecords.Count; n++)
            {
                strBuffer = lstConfigRecords[n];

                if (strBuffer.Trim().Length > 0)
                {
                    intNumberOfLinesRead++;
                    if (strBuffer.ToUpper().IndexOf("<USERSETTINGS") >= 0)
                    {
                        bolUserSettings = true;
                    }
                    if (strBuffer.ToUpper().IndexOf("</USERSETTINGS") >= 0)
                    {
                        bolUserSettings = false;
                    }
                    if (bolUserSettings)
                    {
                        if (strBuffer.ToUpper().IndexOf("<SETTING") >= 0)
                        {
                            //  get the key
                            bolSetting = true;
                            intOffset = strBuffer.ToUpper().IndexOf("NAME=");
                            if (intOffset > 0)
                            {
                                intOffset += 5;
                                intOffset2 = strBuffer.ToUpper().IndexOf(" ", intOffset);
                                if (intOffset2 > 0)
                                {
                                    strKey = strBuffer.Substring(intOffset, (intOffset2 - intOffset));
                                    // now remove any quotes
                                    strKey = strKey.Replace("\"", "");
                                }
                            }
                        }
                        if (strBuffer.ToUpper().IndexOf("</SETTING") >= 0)
                        {
                            bolSetting = false;
                            // add to the config list
                            udtConfigSettings objConfigSetting = new udtConfigSettings();
                            objConfigSetting.strKey = strKey;
                            objConfigSetting.strValue = strValue;
                            objConfigSetting.strOriginalValue = strValue;
                            lstConfigSettings.Add(objConfigSetting);
                            strKey = "";
                            strValue = "";
                        }
                        if (strBuffer.ToUpper().IndexOf("<VALUE") >= 0)
                        {
                            intOffset = strBuffer.IndexOf(">") + 1;
                            intOffset2 = strBuffer.IndexOf("<", intOffset);
                            strValue = strBuffer.Substring(intOffset, (intOffset2 - intOffset));
                        }
                    }
                }
            }

            return lstConfigSettings.Count;

        }


        //*********************************************
        //* See if the setting is in the config list
        //*********************************************
        public static bool SettingExists(string argstrKey)
        {
            bool bolSettingExists = false;

            for (int n = 0; n < lstConfigSettings.Count; n++)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[n];

                if (objConfigSetting.strKey.ToUpper().Equals(argstrKey.ToUpper()))
                {
                    bolSettingExists = true;
                    break;
                }
            }

            return bolSettingExists;
        }




        //*********************************************
        //* Find the config key and return the value
        //*********************************************
        public static string GetSetting(string argstrKey)
        {
            String strValue = "";

            for (int n = 0; n < lstConfigSettings.Count; n++)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[n];

                if (objConfigSetting.strKey.ToUpper().Equals(argstrKey.ToUpper()))
                {
                    strValue = objConfigSetting.strValue;
                    break;
                }
            }

            return strValue;
        }



        //*********************************************
        //* Find the config key and return the value by index
        //*********************************************
        public static string GetSetting(int argintIndex)
        {
            String strValue = "";

            if (argintIndex >= 0 && argintIndex < lstConfigSettings.Count)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[argintIndex];
                strValue = objConfigSetting.strValue;
            }

            return strValue;
        }




        //*********************************************
        //* Clear the settings 
        //*********************************************
        public static void ClearSettings()
        {

            lstConfigSettings.Clear();

            return;
        }



        //*********************************************
        //* Find the config key and set the value to the passed value
        //*********************************************
        public static string SetSetting(string argstrKey, string argstrValue)
        {
            String strValue = "";

            for (int n = 0; n < lstConfigSettings.Count; n++)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[n];

                if (objConfigSetting.strKey.ToUpper().Equals(argstrKey.ToUpper()))
                {
                    objConfigSetting.strValue = argstrValue;
                    lstConfigSettings[n] = objConfigSetting;
                    break;
                }
            }

            return strValue;
        }



        //*********************************************
        //* Write the config file out
        //*********************************************
        public static int WriteConfigFile()
        {
            String strFileName = "";
            int intOffset = 0;
            List<string> lstRecords = new List<string>();
            string strBuffer = "";
            bool bolUserSection = false;
            bool bolSettings = false;
            bool bolSettingsWritten = false;
            string strCurrentPath = Application.ExecutablePath;

            // remove the exe name
            intOffset = strCurrentPath.LastIndexOf("\\");
            if (intOffset >= 0)
            {
                strCurrentPath = strCurrentPath.Substring(0, intOffset);
            }

            strFileName = strCurrentPath + "\\" + clsCommonRtns.GetEXEName() + ".config";

            lstRecords.Clear();

            // walk the config records and replace the values
            for (int n = 0; n < lstConfigRecords.Count; n++)
            {
                // go until the usersettings section is found
                strBuffer = lstConfigRecords[n];
                if (strBuffer.ToUpper().IndexOf("<USERSETTINGS") >= 0)
                {
                    bolUserSection = true;
                }

                if (strBuffer.ToUpper().IndexOf("</USERSETTINGS") >= 0)
                {
                    bolUserSection = false;
                }

                if (bolUserSection)
                {
                    // output the usersettings tag
                    if (bolSettingsWritten)
                    {
                        // done
                    }
                    else
                    {
                        lstRecords.Add(strBuffer);
                        for (int m = 0; m < lstConfigSettings.Count; m++)
                        {
                            lstRecords.Add("<setting name=\"" + lstConfigSettings[m].strKey + "\" serializeAs=\"String\">");
                            lstRecords.Add("    <value>" + lstConfigSettings[m].strValue + "</value>");
                            lstRecords.Add("</setting>");
                        }
                        bolSettingsWritten = true;
                    }
                }
                else
                {
                    lstRecords.Add(strBuffer);
                }

            }

            // now write out the new file
            clsCommonRtns.WriteFile(strFileName, lstRecords);

            return lstConfigSettings.Count;

        }

    
    }
}
