﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    class clsFormRule
    {

        private InputPads frmInputPads = new InputPads();


        //********************************************************************
        //* Routine:        clsFormRule
        //* Description:    Constructor for the clsFormRule
        //* Arguments:      None
        //* Returns:        Nothing
        //********************************************************************
        public clsFormRule()
        {

        }




        //********************************************************************
        //* Routine:        clsFormRule
        //* Description:    Destructor for the clsFormRule
        //* Arguments:      None
        //* Returns:        Nothing
        //********************************************************************
        ~clsFormRule()
        {

        }

        //********************************************************************
        //* Routine:        ProcessRules
        //* Description:    Process the rules for a specific field
        //* Arguments:      Forms collection
        //*                 Current Form
        //*                 Current Field
        //*                 Event
        //*                 Rules for this event
        //* Returns:        Status of load
        //********************************************************************
        public string ProcessRules(clsForms argobjForms, clsForm argobjForm, 
                    Control argobjField, string argstrEvent, string argstrRules)
        {
            string strValue = "";
            TextBox objTempTextBox = new TextBox();
            string strFieldType = argobjField.GetType().Name.ToUpper();
            bool bolProcess = false;
            int intRow = -1;
            int intCol = -1;
            string strFieldValue = "";
            string strField1 = "";
            string strExpression = "";
            string strResult = "";

            // process by event
            switch (argstrEvent.ToUpper())
            {
                case "GETFOCUS":
                    strValue = ProcessGetFocusEventRules(argobjForms, argobjForm, argobjField, argstrEvent, argstrRules);
                    break;
                    
                default:
                    strValue = "";
                    break;

            }

            #region [ rule based processing ]

            string[] astrRules = argstrRules.Split('!');
            //// find the rules for the passed event
            //// process them and then return the result value
            //for (int n = 0; n < astrRules.Length; n++)
            //{
            //    string[] astrRule = astrRules[n].Split('|');
            //    if (astrRule[0].ToUpper() == argstrEvent.ToUpper())
            //    {
            //        string strCriteria = astrRule[1].ToUpper();
            //        string strRule = astrRule[2].ToUpper();
            //        string[] astrParts = strRule.Split(' ');
            //        // see if this is a grid and check the row/col
            //        if (strFileSelectedType == "DATAGRIDVIEW")
            //        {
            //            // see if the criteria section is there
            //            if (strCriteria != "")
            //            {
            //                string[] astrCriteria = strCriteria.Split('~');
            //                if (astrCriteria.Length > 0)
            //                {
            //                    DataGridView objGrid = (DataGridView)argobjField;
            //                    // row = first, col=second
            //                    if (astrCriteria[0] != "*")
            //                    {
            //                        intRow = Convert.ToInt32(astrCriteria[0]) - 1;
            //                    }
            //                    if (astrCriteria[1] != "*")
            //                    {
            //                        intCol = Convert.ToInt32(astrCriteria[1]) - 1;
            //                    }
            //                    bolProcess = true;
            //                    // is the currentcell this row?
            //                    if (intRow >= 0)
            //                    {
            //                        // check row
            //                        if (objGrid.CurrentCell.RowIndex != intRow)
            //                        {
            //                            bolProcess = false;
            //                        }
            //                    }
            //                    if (intCol >= 0)
            //                    {
            //                        // check row
            //                        if (objGrid.CurrentCell.ColumnIndex != intCol)
            //                        {
            //                            bolProcess = false;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            bolProcess = true;
            //        }

            //        if (bolProcess)
            //        {
            //            // process the rule
            //            switch (astrParts[0].ToUpper())
            //            {
            //                // check for specific commands
            //                case "INPUTPAD":
            //                    // call the input pad requested
            //                    strValue = ShowInputPads(astrParts[1].ToUpper(), argobjField);
            //                    break;

            //                case "IF":
            //                    // process a conditional statement
            //                    for (int i = 0; i < astrParts.Length; i++)
            //                    {
            //                        strExpression = strExpression + " " + astrParts[i];
            //                    }
            //                    break;

            //                // probably an expression
            //                default:
            //                    switch (astrParts[1].ToUpper())
            //                    {
            //                        case "=":
            //                            // first part is the variable, second =, third+ expression
            //                            strField1 = astrParts[0];
            //                            strExpression = "";
            //                            for (int i = 2; i < astrParts.Length; i++)
            //                            {
            //                                strExpression = strExpression + " " + astrParts[i];
            //                            }
            //                            // get the result
            //                            strResult = clsCommonRtns.EvaluateExpression(strExpression);
            //                            break;

            //                        default:
            //                            // assume it is an expression.  evaluate and set the field to the result
            //                            strExpression = "";
            //                            for (int i = 0; i < astrParts.Length; i++)
            //                            {
            //                                strExpression = strExpression + " " + astrParts[i];
            //                            }
            //                            strResult = clsCommonRtns.EvaluateExpression(strExpression);
            //                            break;
            //                    }
            //                    break;
            //            }
            //        }
            //    }
            //}

            #endregion 

            return strValue;
        }


        //********************************************************************
        //* Routine:        ToString
        //* Description:    Returns a string of this object
        //* Arguments:      None
        //* Returns:        String of object
        //********************************************************************
        public override string ToString()
        {
            string strValue = "";


            return strValue;

        }


        //********************************************************************
        //* Routine:        ShowInputPads
        //* Description:    Show the keypad
        //* Arguments:      none
        //* Returns:        Nothing
        //********************************************************************
        private string ShowInputPads(string argstrInputPadType, Control argobjControl)
        {
            Control objControl = new Control();
            string strValue = "";

            if (frmInputPads.Visible)
            {
                // hide it
                frmInputPads.Close();
            }
            else
            {
                if (objControl != null)
                {
                    // show the keypad
                    argobjControl.Focus();
                    frmInputPads = new InputPads(argstrInputPadType);
                    //frmInputPads.StartPosition = FormStartPosition.CenterParent;
                    //frmInputPads.Top = this.Height - frmInputPads.Height - 70;
                    //frmInputPads.Left = (this.Width - frmInputPads.Width) / 2;
                    if (argobjControl.GetType().Name.ToUpper() == "DATAGRIDVIEW")
                    {
                        // position the top next to the row and left next to the column)
                        DataGridView objGrid = (DataGridView)argobjControl;
                        objControl.Text = objGrid.CurrentCell.Value.ToString();
                        //frmInputPads.Top = pnlForms.Top + tabForm1.Top + objControl.Top + (objGrid.CurrentCell.RowIndex * 30) + 35;
                        //frmInputPads.Left = (this.Width - frmInputPads.Width) / 2;
                    }
                    else
                    {
                        objControl = argobjControl;
                        //frmInputPads.Top = pnlForms.Top + tabForm1.Top + objControl.Top + objControl.Height + 35;
                        //frmInputPads.Left = (this.Width - frmInputPads.Width) / 2;
                    }
                    frmInputPads.FieldSelected = objControl;
                    frmInputPads.BringToFront();
                    frmInputPads.ShowDialog();
                    // return the value entered
                    strValue = objControl.Text;
                }
            }

            return strValue;

        }


        //********************************************************************
        //* Routine:        ProcessGetFocusEventRules
        //* Description:    Process the rules for a the GetFocus event
        //* Arguments:      Forms collection
        //*                 Current Form
        //*                 Current Field
        //*                 Event
        //*                 Rules for this event
        //* Returns:        Status of load
        //********************************************************************
        public string ProcessGetFocusEventRules(clsForms argobjForms, clsForm argobjForm,
                    Control argobjField, string argstrEvent, string argstrRules)
        {
            string strValue = "";
            TextBox objTempTextBox = new TextBox();
            string strFieldType = argobjField.GetType().Name.ToUpper();
            bool bolProcess = false;
            int intRow = -1;
            int intCol = -1;
            string strFieldValue = "";
            string strField1 = "";
            string strExpression = "";
            string strResult = "";
            string strFieldName = argobjField.Name.ToUpper().Replace("$", "");

            // process by field
            switch (strFieldName)
            {
                case "PATIENTVITALS":
                    // get the grid row and column
                    DataGridView objGrid = (DataGridView)argobjField;
                    intRow = objGrid.CurrentCell.RowIndex;
                    intCol = objGrid.CurrentCell.ColumnIndex;
                    // see that the first column has a timestamp
                    string strTempValue = objGrid.Rows[0].Cells[intCol].Value.ToString();
                    if (strTempValue.Length == 0)
                    {
                        // set the time
                        strValue = clsCommonRtns.FormatCurrentTime();
                        objGrid.Rows[0].Cells[intCol].Value = strValue;
                    }
                    string strColumnName = objGrid.Rows[intRow].Cells[0].Value.ToString().ToUpper().Replace(" ", "");
                    strFieldValue = objGrid.CurrentCell.Value.ToString();
                    switch (strColumnName)
                    {
                        case "TIME":
                            strValue = clsCommonRtns.FormatCurrentTime();
                            break;

                        case "POSITION":
                            strValue = ShowInputPads("POSITIONS", argobjField);
                            break;

                        case "GCS":
                            strValue = ShowInputPads("GCS", argobjField);
                            break;

                        case "BLOODPRESSURE":
                            strValue = ShowInputPads("BLOODPRESSURE", argobjField);
                            break;

                        default:
                            break;
                    }
                    break;

                case "INCNUMBER":
                    strValue = ShowInputPads("KEYPAD", argobjField);
                    break;

                default:
                    strValue = "";
                    break;

            }

            return strValue;
        }



    }
}
