﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public class clsStLukes
    {

        private TabControl tabForms = null;
        private string strSelectedForm = "";
        private string strEvent = "";
        private Control objSelectedControl = null;
        private TabPage objSelectedTabPage = null;
        private PictureBox objSelectedPallet = null;
        private InputPads objInputPads = new InputPads();

        public string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";

            // set the global objects
            tabForms = argtabForms;
            strSelectedForm = argstrForm;
            objSelectedTabPage = tabForms.TabPages[strSelectedForm];
            objSelectedPallet = (PictureBox)objSelectedTabPage.Controls[0];
            objSelectedControl = argobjControl;
            strEvent = argstrEvent;

            // process for the specified form
            switch (strSelectedForm.ToUpper())
            {
                case "STLUKES":
                    // form group level
                    strResult = ProcessStLukes();
                    break;

                case "ADV_DIR_QUES":
                    // white form
                    strResult = ProcessAdv_Dir_Ques();
                    break;

                case "CONSENT":
                    // blue form
                    strResult = ProcessConsent();
                    break;

            }

            return strResult;

        }



        /// <summary>
        /// Form level rules
        /// </summary>
        /// <returns></returns>
        private string ProcessStLukes()
        {
            string strResult = "";

            return strResult;

        }




        /// <summary>
        /// Adv Dir Ques page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessAdv_Dir_Ques()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    strResult = ProcessAdv_Dir_Ques_GetFocus();
                    break;

                case "LOSTFOCUS":
                    break;

                case "ENTER":
                    break;

            }

            return strResult;
        }


        /// <summary>
        /// ProcessAdv_Dir_Ques_GetFocus - Adv Dir Ques page rules when a control gets focus
        /// </summary>
        /// <returns></returns>
        private string ProcessAdv_Dir_Ques_GetFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // for text controls, set the pad to keypad
            if (strControlType == "TEXTBOX")
            {
                strPad = "KEYPAD";
            }

            // see what control it is
            switch (strControlName)
            {
                case "INFO_PROV_PAT":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else 
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off
                        objSelectedPallet.Controls["INFO_PROV_FAM"].Text = "";
                        objSelectedPallet.Controls["NO_INFO_PROV"].Text = "";
                    }
                    strPad = "";
                    break;

                case "INFO_PROV_FAM":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                        objSelectedPallet.Controls["FAMILY_NAME"].Visible = false;
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off
                        objSelectedPallet.Controls["INFO_PROV_PAT"].Text = "";
                        objSelectedPallet.Controls["NO_INFO_PROV"].Text = "";
                        objSelectedPallet.Controls["FAMILY_NAME"].Visible = true;
                    }
                    strPad = "";
                    break;

                case "NO_INFO_PROV":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off
                        objSelectedPallet.Controls["INFO_PROV_PAT"].Text = "";
                        objSelectedPallet.Controls["INFO_PROV_FAM"].Text = "";
                    }
                    strPad = "";
                    break;

                case "MED_POA_COPY_IN_CHART":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off
                        objSelectedPallet.Controls["MED_POA_YES_COPY_NOT_AVAIL"].Text = "";
                        objSelectedPallet.Controls["MED_POA_NO"].Text = "";
                    }
                    strPad = "";
                    break;

                case "MED_POA_YES_COPY_NOT_AVAIL":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off
                        objSelectedPallet.Controls["MED_POA_COPY_IN_CHART"].Text = "";
                        objSelectedPallet.Controls["MED_POA_NO"].Text = "";
                    }
                    strPad = "";
                    break;

                case "MED_POA_NO":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                        // set the other ones off 
                        objSelectedPallet.Controls["MED_POA_COPY_IN_CHART"].Text = "";
                        objSelectedPallet.Controls["MED_POA_YES_COPY_NOT_AVAIL"].Text = "";
                    }
                    strPad = "";
                    break;

                //DO_NOT_RESUSCITATE
                case "DO_NOT_RESUSCITATE":
                case "POWER_OF_ATTORNEY":
                case "LIVING_WILL":
                    if (strControlValue == "X")
                    {
                        objSelectedControl.Text = "";
                    }
                    else
                    {
                        // set it on
                        objSelectedControl.Text = "X";
                    }
                    strPad = "";
                    break;

                case "INCDATE":
                    strPad = "CALENDAR";
                    break;

                case "REV_ALERT1_NAME":
                    break;


            }

            if (clsFormInputRtns.strInputStyle != "TOUCH")
            {
                strPad = "";
            }

            if (strPad != "")
            {
                strResult = "";
                // call the inputpad routines
                objInputPads.SetInputPad(strPad);
                objInputPads.FieldSelected = objSelectedControl;
                objInputPads.CharPosition = 0;
                objInputPads.ShowDialog();
                strResult = objInputPads.Value;
            }

            return strResult;

        }



        /// <summary>
        /// ProcessAdv_Dir_Ques_LostFocus - Adv Dir Ques page rules when a control loses focus
        /// </summary>
        /// <returns></returns>
        private string ProcessAdv_Dir_Ques_LostFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // see which control
            // see what control it is
            switch (strControlName)
            {
                case "INFO_PROV_PAT":
                    break;

                case "INFO_PROV_FAM":
                    break;

                case "NO_INFO_PROV":
                    break;

                case "MED_POA_COPY_IN_CHART":
                    break;

                case "MED_POA_YES_COPY_NOT_AVAIL":
                    break;

                case "MED_POA_NO":
                    break;

                //DO_NOT_RESUSCITATE
                case "DO_NOT_RESUSCITATE":
                case "POWER_OF_ATTORNEY":
                case "LIVING_WILL":
                    break;

                case "INCDATE":
                    break;

                case "REV_ALERT1_NAME":
                    clsFormInputRtns.SetField("CONSENT", "REV_ALERT1_NAME_CONSENT", strControlValue);
                    break;

            }

            return strResult;
        }




        /// <summary>
        /// Consent page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessConsent()
        {
            string strResult = "";

            return strResult;

        }


    }
}
