﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public class clsEMS10001
    {
        private TabControl tabForms = null;
        private string strSelectedForm = "";
        private Control objSelectedControl = null;
        private string strEvent = "";
        private InputPads objInputPads = new InputPads();
        private string strTypeOfInput = "";

        public string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";

            // set the type of input (type, touch or mixed)
            // type=use the keyboard for all fields
            // touch=use the input pads for all fields
            // mixed=use the keyboard for text fields, input pads for selection fields
            strTypeOfInput = clsFormInputRtns.strInputStyle.ToUpper();
            
            // set the global objects
            tabForms = argtabForms;
            strSelectedForm = argstrForm;
            objSelectedControl = argobjControl;
            strEvent = argstrEvent;

            // process for the specified form
            switch (strSelectedForm.ToUpper())
            {
                case "EMS10001":
                    // form group level
                    strResult = ProcessEMS10001(argstrEvent);
                    break;

                case "EMS10001_WHITE":
                    // white form
                    strResult = ProcessEMS10001_White(argstrEvent);
                    break;

                case "EMS10001_BLUE":
                    // blue form
                    strResult = ProcessEMS10001_Blue(argstrEvent);
                    break;

                case "EMS10001_YELLOW":
                    // yellow form
                    strResult = ProcessEMS10001_Yellow(argstrEvent);
                    break;

            }

            return strResult;

        }



        /// <summary>
        /// Form level rules
        /// </summary>
        /// <returns></returns>
        private string ProcessEMS10001(string argstrEvent)
        {
            string strResult = "";

            return strResult;

        }




        /// <summary>
        /// White page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessEMS10001_White(string argstrEvent)
        {
            string strResult = "";
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlValue = objSelectedControl.Text;
            string strPad = "";

            // for text controls, set the pad to keypad
            if (objSelectedControl.GetType().Name.ToUpper() == "TEXTBOX")
            {
                if (strTypeOfInput == "TOUCH")
                {
                    strPad = "KEYPAD";
                }
            }

            // see what control it is
            switch (strControlName)
            {
                case "":
                    strPad = "";
                    break;

                case "INCDATE":
                    strPad = "CALENDAR";
                    break;

                case "IDCNUMBER":
                    break;

                case "INCNUMBER":
                    break;

                case "SIGNATURE":
                    break;

                case "ENDODOMETER":
                    break;

                case "BEGODOMETER":
                    break;

                case "PRIVATEMD":
                    break;

                case "PATIENTALLERGIES":
                    break;

                case "SMOKER":
                    break;

                case "SMOKERYEARS":
                    break;

                case "NONSMOKER":
                    break;

                case "NONSMOKERYEARS":
                    break;

                case "PATIENTHISTORY":
                    break;

                case "PATIENTMEDICATIONS":
                    break;

                case "PATIENTCOMMENTS":
                    break;

                case "PATIENTDOB":
                    strPad = "CALENDAR";
                    break;

                case "PATIENTAGE":
                    break;

                case "PATIENTPHONE":
                    break;

                case "PATIENTSSN":
                    break;

                case "TRAUMAID":
                    break;

                case "PATIENTNAME":
                    // set the other patient name
                    if (argstrEvent.ToUpper() == "LOSTFOCUS")
                    {
                        clsFormInputRtns.SetField("EMS10001_Blue", "PatientName_Blue", strControlValue);
                    }
                    break;

                case "SAA":
                    break;

                case "PATIENTADDRESS":
                    break;

                case "INCLOCATION":
                    break;

                case "PATIENTGENDER":
                    break;

                case "PATIENTPTNO":
                    break;

                case "PATIENTVITALS":
                    // grid
                    strResult = ProcessPatientVitals(argstrEvent);
                    break;

            }

            if (strPad != "")
            {
                switch (argstrEvent.ToUpper())
                {
                    case "GETFOCUS":
                        if (strTypeOfInput != "TYPE")
                        {
                            strResult = "";
                            // call the inputpad routines
                            objInputPads.SetInputPad(strPad);
                            objInputPads.FieldSelected = objSelectedControl;
                            objInputPads.CharPosition = 0;
                            objInputPads.ShowDialog();
                            strResult = objInputPads.Value;
                        }
                        break;
                }

            }

            return strResult;

        }






        private string ProcessPatientVitals(string argstrEvent)
        {
            string strResult = "";
            DataGridView objGrid = (DataGridView)objSelectedControl;
            DataGridViewCell objCell = objGrid.CurrentCell;
            int intRow = objCell.RowIndex;
            int intCol = objCell.ColumnIndex;

            // process the cell selected
            // the rows have specific needs...  the value in column 0 is the key...
            string strCellKey = clsCommonRtns.ConvertToString(objGrid.Rows[intRow].Cells[0].Value).ToUpper();
            string strCellValue = clsCommonRtns.ConvertToString(objGrid.Rows[intRow].Cells[intCol].Value).ToString();
            string strTimeValue = clsCommonRtns.ConvertToString(objGrid.Rows[0].Cells[intCol].Value).ToString();
            string strPad = "";

            // with all the values, if the time field is not filled in then fill it in
            if (strTimeValue == "")
            {
                // set the value
                objGrid.Rows[0].Cells[intCol].Value = clsCommonRtns.FormatCurrentTime();
            }

            switch (strCellKey)
            {
                case "TIME":
                    break;

                case "POSITION":
                    // get the value from the positions input pad
                    strPad = "POSITIONS";
                    break;

                case "GCS":
                    // get the value from the gcs input pad
                    strPad = "GCS";
                    break;

                case "BLOOD PRESSURE":
                    // get the value from the blood pressure input pad
                    strPad = "BLOODPRESSURE";
                    break;

                case "HEART RATE":
                    // get the value from the heart rate input pad
                    strPad = "KEYPAD";
                    break;

                default:
                    strPad = "KEYPAD";
                    break;

            }


            if (strPad != "") 
            {
                switch (argstrEvent.ToUpper())
                {
                    case "GETFOCUS":
                        if (strTypeOfInput != "TYPE")
                        {
                            strResult = "";
                            // call the inputpad routines
                            Control objControl = new Control();
                            objControl.Text = strCellValue;
                            objInputPads.SetInputPad(strPad);
                            objInputPads.FieldSelected = objControl;
                            objInputPads.CharPosition = 0;
                            objInputPads.ShowDialog();
                            strResult = objInputPads.Value;
                            // set the cell value
                            objGrid.Rows[intRow].Cells[intCol].Value = strResult;
                        }
                        break;

                }
            }

            return strResult;

        }



        /// <summary>
        /// Blue page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessEMS10001_Blue(string argstrEvent)
        {
            string strResult = "";

            return strResult;

        }




        /// <summary>
        /// Yellow page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessEMS10001_Yellow(string argstrEvent)
        {
            string strResult = "";

            return strResult;

        }


    }
}
