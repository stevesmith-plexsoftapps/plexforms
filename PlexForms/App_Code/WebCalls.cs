﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Web.Services;

namespace PlexForms
{
    class WebCalls
    {


        public WebCalls()
        {

        }


        public string UploadForm(string argstrCustomerName, string argstrFormName, 
                                string argstrForm, ref string argstrCustomerId)
        {

            string strResponse = "";

            PlexWS.PlexWebservice.WebServiceSoapClient objPlexWS = new PlexWS.PlexWebservice.WebServiceSoapClient();

            objPlexWS.Open();

            int intCustId = 0;
            strResponse = objPlexWS.UploadForm(argstrCustomerName, argstrFormName, argstrForm, ref intCustId);

            argstrCustomerId = intCustId.ToString();

            objPlexWS.Close();

            return strResponse;
        }


    }
}
