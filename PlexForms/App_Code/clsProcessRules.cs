﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public static class clsProcessRules
    {
        public static bool bolUseDLL = true;

        private static InputPads objInputPads = new InputPads();

        private static string[] astrActions = {
                                              "INPUTPAD",
                                              "GOTO",
                                              "EMAIL",
                                              "PRINT",
                                              "SET",
                                              "ASK"
                                              };



        public static string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";
            List<string> lstRules = new List<string>();

            clsCommonRtns.Display("Processing rules...");

            if (bolUseDLL)
            {
                // use the DLL instead of rules
                clsDLLRules objDLLRules = new clsDLLRules();

                strResult = objDLLRules.ProcessRules(argtabForms, argstrForm, argobjControl, argstrEvent);
                
            }
            else
            {
                // get the rules for the control passed
                lstRules = clsFormInputRtns.GetFieldRules(argstrForm, argobjControl.Name, argstrEvent);

                if (lstRules.Count > 0)
                {
                    // process the rules
                    for (int n = 0; n < lstRules.Count; n++)
                    {
                        strResult = RunRule(argobjControl, lstRules[n]);
                    }
                }
            }

            clsCommonRtns.Display("Ready");

            return strResult;

        }



        private static string RunDLLRules(string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";


            return strResult;

        }





        private static string RunRule(Control argobjControl, string argstrRule)
        {
            string strResult = "";
            string strLabel = "";
            string strRule = argstrRule.Trim();

            if (strRule.Length < 2)
            {
                return "";
            }

            // if the first chars are a // then it is a comment
            if (strRule.Substring(0, 2) == "//")
            {
                return "";
            }

            strRule = strRule.Replace(";","");

            // determine what the rule is
            // the rule format is label: action/field operator field/expression branch...

            // first see if there is a label...
            int intOffset = strRule.IndexOf(':');
            if (intOffset > 0)
            {
                // strip the label
                strLabel = strRule.Substring(0, intOffset);
                strRule = strRule.Substring(intOffset);

            }

            // split it up by spaces
            string[] astrElements = strRule.Split(' ');

            // see if the first element is an action
            string strAction = astrElements[0].ToUpper();
            switch (strAction)
            {
                case "INPUTPAD":
                    // get the parm
                    string strPad = astrElements[1].ToUpper();
                    // call the inputpad routines
                    objInputPads.SetInputPad(strPad);
                    objInputPads.FieldSelected = argobjControl;
                    objInputPads.CharPosition = 0;
                    objInputPads.ShowDialog();
                    break;

                default:
                    if (astrElements.Length > 1)
                    {
                        // may be a field
                        if (astrElements[1] == "=")
                        {
                            // set a value, format is field1 = field2
                            string strField = astrElements[0];
                            string strForm = "";
                            string strAttribute = "";
                            string strValue = astrElements[2];
                            // find the field in the form.  is it a form.field.attribute?
                            string[] astrQualifiers = strField.Split('.');
                            if (astrQualifiers.Length > 0)
                            {
                                if (astrQualifiers.Length > 1)
                                {
                                    // form.field.attribute
                                    strForm = astrQualifiers[0];
                                    strField = astrQualifiers[1];
                                    strAttribute = astrQualifiers[2];
                                }
                                else
                                {
                                    // field.attribute
                                    strForm = "";
                                    strField = "";
                                    strAttribute = "";
                                    if (astrQualifiers.Length > 0)
                                    {
                                        strField = astrQualifiers[0];
                                        if (astrQualifiers.Length > 1)
                                        {
                                            strAttribute = astrQualifiers[1];
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // field 
                            }
                            // now find the control
                            Control objControl1 = clsFormInputRtns.GetField(strForm, strField);
                        }
                    }
                    break;
            }
                   

            return strResult;
        }






        private static bool IsAction(string argstrAction)
        {
            bool bolIsAction = false;

            for (int n = 0; n < astrActions.Length; n++)
            {
                if (argstrAction.ToUpper() == astrActions[n])
                {
                    bolIsAction = true;
                    break;
                }
            }

            return bolIsAction;

        }


    }
}
