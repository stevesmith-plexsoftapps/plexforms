﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace PlexForms
{
    public class clsDatabaseRtns
    {

        static string strDBMSType = "SQLSERVER";
        static string strHost = "ssmithe64\\sqlexpress";
        static string strDatabase = "Forms";
        static string strUID = "dba";
        static string strPWD = "dba";
        static string strConnectionString = "Data Source=" + strHost + ";Initial Catalog=" + strDatabase + ";User Id=" + strUID + ";Password=" + strPWD + ";";
        static SqlConnection objCN = null;
        static SqlConnectionStringBuilder objConnBuilder = new SqlConnectionStringBuilder();



        public static bool OpenConnection(string argstrHost, string argstrDatabase, string argstrUID, string argstrPWD)
        {
            bool bolStatus = false;

            // create the connection
            strConnectionString = "Data Source=" + argstrHost + ";Initial Catalog=" + argstrDatabase + ";User Id=" + argstrUID + ";Password=" + argstrPWD + ";";

            bolStatus = OpenConnection(strConnectionString);

            return bolStatus;
        }


        public static bool OpenConnection(string argstrConnection)
        {
            bool bolStatus = false;

            strConnectionString = argstrConnection;

            try
            {
                objCN = new SqlConnection(strConnectionString);
                objCN.Open();
                if (objCN.State == System.Data.ConnectionState.Open)
                {
                    bolStatus = true;
                }
                else
                {
                    bolStatus = false;
                }
            }
            catch (SqlException sqlex)
            {
                // give error
                bolStatus = false;
            }

            return bolStatus;
        }



        public static List<string> ReadDatabase(string argstrFormGroupName)
        {
            List<string> lstRecords = new List<string>();
            string strSQL = "";
            SqlCommand objCmd = null;
            SqlDataReader objRS = null;

            if (objCN == null || objCN.State != System.Data.ConnectionState.Open)
            {
                // open the connection
                if (strConnectionString != "")
                {
                    if (!OpenConnection(strConnectionString))
                    {
                        // error!
                        return lstRecords;
                    }
                }
            }

            // read the database for the form

            // first read the form group
            strSQL = "Select * " +
                     "From Form_Groups ";
            //+
            //         "Where Group_Name='" + argstrFormGroupName + "' ";

            try 
            {
                objCmd = new SqlCommand(strSQL,objCN);

                objRS = objCmd.ExecuteReader();

                // read the groups
                while (objRS.Read())
                {
                    string strGroupName = objRS.GetString(0).ToString();
                    string strDescription = objRS.GetString(1).ToString();
                    int intGroupId = objRS.GetInt32(2);
                }
                
                objRS.Close();

                objCmd = null;

            }
            catch (SqlException sqlex)
            {

            }


            return lstRecords;
        }


    }
}
