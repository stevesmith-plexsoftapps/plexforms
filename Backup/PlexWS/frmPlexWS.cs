﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace PlexWS
{
    public partial class frmPlexWS : Form
    {

        public frmPlexWS()
        {
            InitializeComponent();

            this.Load += new EventHandler(frmPlexWS_Load);

        }

        void frmPlexWS_Load(object sender, EventArgs e)
        {

            txtStatus.Text = "Enter a customer name and select a form";
            txtStatus.Refresh();
            Application.DoEvents();
            
        }


        
        
        private void btnUpload_Click(object sender, EventArgs e)
        {
            int intCustId = 0;

            UploadWS();

        }



        private void UploadWS()
        {
            clsPlexWS objPlexWS = new clsPlexWS();
            int intCustId = 0;

            txtStatus.Text = "Calling web service...";
            txtStatus.Refresh();
            Application.DoEvents();

            string strResponse = objPlexWS.UploadForm(txtCustomerName.Text, txtFormName.Text, txtFormXML.Text);

            txtStatus.Text = "Returned from web service.  Status=" + strResponse;
            txtStatus.Refresh();
            Application.DoEvents();

            // get the id from the status
            int intIndex = strResponse.IndexOf("-");
            if (intIndex > 0)
            {
                string strTemp = strResponse.Substring(0, intIndex).Trim();
                intCustId = Convert.ToInt32(strTemp);
            }

            txtCustomerId.Text = intCustId.ToString();

        }




        private void btnBrowse_Click(object sender, EventArgs e)
        {

            string strFormName = clsCommonRtns.GetFile("frm", "", "0");

            if (strFormName != "")
            {
                txtFormName.Text = strFormName;
                txtFormXML.Text = "";
                // load the file
                txtStatus.Text = "Loading form...";
                txtStatus.Refresh();
                Application.DoEvents();

                List<string> lstRecords = clsCommonRtns.ReadFile(strFormName);
                for (int n = 1; n < lstRecords.Count; n++)
                {
                    txtFormXML.Text = txtFormXML.Text + lstRecords[n] + "\r\n";
                }


                txtStatus.Text = "Done Loading form...";
                txtStatus.Refresh();
                Application.DoEvents();
            }

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            clsPlexWS objPlexWS = new clsPlexWS();

            txtStatus.Text = "Calling web service...";
            txtStatus.Refresh();
            Application.DoEvents();

            string strResponse = objPlexWS.SendEmail("to", "from", "subject", "message");

            txtStatus.Text = "Returned from web service.  Status=" + strResponse;
            txtStatus.Refresh();
            Application.DoEvents();
        }




    }
}
