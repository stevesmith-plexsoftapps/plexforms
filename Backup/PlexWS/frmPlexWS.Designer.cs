﻿namespace PlexWS
{
    partial class frmPlexWS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageUploadCustomer = new System.Windows.Forms.TabPage();
            this.txtFormXML = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFormName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtCustomerId = new System.Windows.Forms.TextBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageSendEmail = new System.Windows.Forms.TabPage();
            this.btnSend = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPageUploadCustomer.SuspendLayout();
            this.tabPageSendEmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(9, 467);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Status:";
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(50, 465);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(597, 20);
            this.txtStatus.TabIndex = 9;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageUploadCustomer);
            this.tabControl1.Controls.Add(this.tabPageSendEmail);
            this.tabControl1.Location = new System.Drawing.Point(3, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(719, 441);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPageUploadCustomer
            // 
            this.tabPageUploadCustomer.Controls.Add(this.txtFormXML);
            this.tabPageUploadCustomer.Controls.Add(this.label3);
            this.tabPageUploadCustomer.Controls.Add(this.txtFormName);
            this.tabPageUploadCustomer.Controls.Add(this.label4);
            this.tabPageUploadCustomer.Controls.Add(this.btnBrowse);
            this.tabPageUploadCustomer.Controls.Add(this.txtCustomerId);
            this.tabPageUploadCustomer.Controls.Add(this.btnUpload);
            this.tabPageUploadCustomer.Controls.Add(this.txtCustomerName);
            this.tabPageUploadCustomer.Controls.Add(this.label1);
            this.tabPageUploadCustomer.Location = new System.Drawing.Point(4, 22);
            this.tabPageUploadCustomer.Name = "tabPageUploadCustomer";
            this.tabPageUploadCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUploadCustomer.Size = new System.Drawing.Size(711, 415);
            this.tabPageUploadCustomer.TabIndex = 0;
            this.tabPageUploadCustomer.Text = "Upload Customer";
            this.tabPageUploadCustomer.UseVisualStyleBackColor = true;
            // 
            // txtFormXML
            // 
            this.txtFormXML.Location = new System.Drawing.Point(21, 65);
            this.txtFormXML.Multiline = true;
            this.txtFormXML.Name = "txtFormXML";
            this.txtFormXML.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFormXML.Size = new System.Drawing.Size(619, 305);
            this.txtFormXML.TabIndex = 111;
            this.txtFormXML.WordWrap = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(444, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 110;
            this.label3.Text = "Customer Id:";
            // 
            // txtFormName
            // 
            this.txtFormName.BackColor = System.Drawing.Color.White;
            this.txtFormName.Location = new System.Drawing.Point(109, 39);
            this.txtFormName.Name = "txtFormName";
            this.txtFormName.Size = new System.Drawing.Size(432, 20);
            this.txtFormName.TabIndex = 105;
            this.txtFormName.TabStop = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(18, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 108;
            this.label4.Text = "Form File:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(547, 36);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 106;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.BackColor = System.Drawing.Color.White;
            this.txtCustomerId.Location = new System.Drawing.Point(510, 14);
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.ReadOnly = true;
            this.txtCustomerId.Size = new System.Drawing.Size(50, 20);
            this.txtCustomerId.TabIndex = 109;
            this.txtCustomerId.TabStop = false;
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(577, 376);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(75, 23);
            this.btnUpload.TabIndex = 107;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(109, 14);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(329, 20);
            this.txtCustomerName.TabIndex = 103;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 104;
            this.label1.Text = "Customer Name:";
            // 
            // tabPageSendEmail
            // 
            this.tabPageSendEmail.Controls.Add(this.btnSend);
            this.tabPageSendEmail.Location = new System.Drawing.Point(4, 22);
            this.tabPageSendEmail.Name = "tabPageSendEmail";
            this.tabPageSendEmail.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSendEmail.Size = new System.Drawing.Size(711, 415);
            this.tabPageSendEmail.TabIndex = 1;
            this.tabPageSendEmail.Text = "Send Email";
            this.tabPageSendEmail.UseVisualStyleBackColor = true;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(567, 358);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(117, 37);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // frmPlexWS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 498);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.label2);
            this.Name = "frmPlexWS";
            this.Text = "Test PlexWS";
            this.tabControl1.ResumeLayout(false);
            this.tabPageUploadCustomer.ResumeLayout(false);
            this.tabPageUploadCustomer.PerformLayout();
            this.tabPageSendEmail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageUploadCustomer;
        private System.Windows.Forms.TextBox txtFormXML;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFormName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtCustomerId;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageSendEmail;
        private System.Windows.Forms.Button btnSend;
    }
}

