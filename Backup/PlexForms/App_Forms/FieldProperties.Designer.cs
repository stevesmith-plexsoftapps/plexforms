﻿namespace PlexForms
{
    partial class FieldProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabFieldProperties = new System.Windows.Forms.TabControl();
            this.tabPageAttributes = new System.Windows.Forms.TabPage();
            this.grdAttributes = new System.Windows.Forms.DataGridView();
            this.Attribute = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributeValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageRules = new System.Windows.Forms.TabPage();
            this.txtRules = new System.Windows.Forms.RichTextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnEditRules = new System.Windows.Forms.Button();
            this.tabFieldProperties.SuspendLayout();
            this.tabPageAttributes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).BeginInit();
            this.tabPageRules.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabFieldProperties
            // 
            this.tabFieldProperties.Controls.Add(this.tabPageAttributes);
            this.tabFieldProperties.Controls.Add(this.tabPageRules);
            this.tabFieldProperties.Location = new System.Drawing.Point(12, 24);
            this.tabFieldProperties.Name = "tabFieldProperties";
            this.tabFieldProperties.SelectedIndex = 0;
            this.tabFieldProperties.Size = new System.Drawing.Size(606, 408);
            this.tabFieldProperties.TabIndex = 0;
            // 
            // tabPageAttributes
            // 
            this.tabPageAttributes.Controls.Add(this.grdAttributes);
            this.tabPageAttributes.Location = new System.Drawing.Point(4, 22);
            this.tabPageAttributes.Name = "tabPageAttributes";
            this.tabPageAttributes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAttributes.Size = new System.Drawing.Size(598, 382);
            this.tabPageAttributes.TabIndex = 0;
            this.tabPageAttributes.Text = "Attributes";
            this.tabPageAttributes.UseVisualStyleBackColor = true;
            // 
            // grdAttributes
            // 
            this.grdAttributes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAttributes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Attribute,
            this.AttributeValue});
            this.grdAttributes.Location = new System.Drawing.Point(10, 15);
            this.grdAttributes.MultiSelect = false;
            this.grdAttributes.Name = "grdAttributes";
            this.grdAttributes.Size = new System.Drawing.Size(569, 348);
            this.grdAttributes.TabIndex = 0;
            // 
            // Attribute
            // 
            this.Attribute.Frozen = true;
            this.Attribute.HeaderText = "Attribute";
            this.Attribute.Name = "Attribute";
            this.Attribute.Width = 200;
            // 
            // AttributeValue
            // 
            this.AttributeValue.Frozen = true;
            this.AttributeValue.HeaderText = "Value";
            this.AttributeValue.Name = "AttributeValue";
            this.AttributeValue.Width = 600;
            // 
            // tabPageRules
            // 
            this.tabPageRules.Controls.Add(this.btnEditRules);
            this.tabPageRules.Controls.Add(this.txtRules);
            this.tabPageRules.Location = new System.Drawing.Point(4, 22);
            this.tabPageRules.Name = "tabPageRules";
            this.tabPageRules.Size = new System.Drawing.Size(598, 382);
            this.tabPageRules.TabIndex = 2;
            this.tabPageRules.Text = "Rules";
            this.tabPageRules.UseVisualStyleBackColor = true;
            // 
            // txtRules
            // 
            this.txtRules.AcceptsTab = true;
            this.txtRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRules.Location = new System.Drawing.Point(3, 3);
            this.txtRules.Name = "txtRules";
            this.txtRules.Size = new System.Drawing.Size(592, 331);
            this.txtRules.TabIndex = 0;
            this.txtRules.Text = "";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(486, 438);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(61, 27);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(553, 438);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(61, 27);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnEditRules
            // 
            this.btnEditRules.Location = new System.Drawing.Point(521, 344);
            this.btnEditRules.Name = "btnEditRules";
            this.btnEditRules.Size = new System.Drawing.Size(61, 28);
            this.btnEditRules.TabIndex = 1;
            this.btnEditRules.Text = "Edit";
            this.btnEditRules.UseVisualStyleBackColor = true;
            this.btnEditRules.Click += new System.EventHandler(this.btnEditRules_Click);
            // 
            // FieldProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 477);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tabFieldProperties);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FieldProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Properties";
            this.tabFieldProperties.ResumeLayout(false);
            this.tabPageAttributes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).EndInit();
            this.tabPageRules.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabFieldProperties;
        private System.Windows.Forms.TabPage tabPageAttributes;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TabPage tabPageRules;
        private System.Windows.Forms.DataGridView grdAttributes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attribute;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeValue;
        private System.Windows.Forms.RichTextBox txtRules;
        private System.Windows.Forms.Button btnEditRules;
    }
}