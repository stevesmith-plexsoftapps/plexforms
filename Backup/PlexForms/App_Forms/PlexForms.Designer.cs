﻿namespace PlexForms
{
    partial class PlexForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlexForms));
            this.mnuPlexForms = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNewField = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNewRule = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFilePrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrintToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileEmailForm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileImport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileExport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuEditDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTools = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsSetTabstops = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuToolsEditForm = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsCloseEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsUploadForm = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsTouch = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsKeyboard = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindows = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindowsCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindowsTile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblCoordinates = new System.Windows.Forms.Label();
            this.picPoweredByPlexsoft = new System.Windows.Forms.PictureBox();
            this.lblClock = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.tabForms = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbrPlexForms = new System.Windows.Forms.ToolStrip();
            this.tbrPlexFormsOpen = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsSave = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsNew = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsAdd = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsClose = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsPrint = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsPrintPreview = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsEmail = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsProperties = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsSettings = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsHelp = new System.Windows.Forms.ToolStripButton();
            this.tbrPlexFormsExit = new System.Windows.Forms.ToolStripButton();
            this.cmnuRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmnuRightClickAddField = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldTextbox = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldCombobox = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldListbox = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldButton = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldCheckbox = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldOption = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickAddFieldFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickMoveField = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.cmnuRightClickCutField = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickCopyField = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickPasteField = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.cmnuRightClickDeleteField = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickChangeField = new System.Windows.Forms.ToolStripMenuItem();
            this.cmnuRightClickProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrClock = new System.Windows.Forms.Timer(this.components);
            this.pnlControls = new System.Windows.Forms.Panel();
            this.tabAttributes = new System.Windows.Forms.TabControl();
            this.tabPageAttributes = new System.Windows.Forms.TabPage();
            this.grdAttributes = new System.Windows.Forms.DataGridView();
            this.Attribute = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributeValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageRules = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.lblPalletSize = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.chkOneClick = new System.Windows.Forms.CheckBox();
            this.lblToolBox = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.objPrintPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.objPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.txtLogo = new System.Windows.Forms.TextBox();
            this.txtRules = new System.Windows.Forms.TextBox();
            this.mnuPlexForms.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPoweredByPlexsoft)).BeginInit();
            this.tabForms.SuspendLayout();
            this.tbrPlexForms.SuspendLayout();
            this.cmnuRightClick.SuspendLayout();
            this.pnlControls.SuspendLayout();
            this.tabAttributes.SuspendLayout();
            this.tabPageAttributes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).BeginInit();
            this.tabPageRules.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuPlexForms
            // 
            this.mnuPlexForms.BackColor = System.Drawing.SystemColors.Control;
            this.mnuPlexForms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuEdit,
            this.mnuTools,
            this.mnuWindows,
            this.mnuHelp});
            this.mnuPlexForms.Location = new System.Drawing.Point(0, 0);
            this.mnuPlexForms.Name = "mnuPlexForms";
            this.mnuPlexForms.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.mnuPlexForms.Size = new System.Drawing.Size(1148, 24);
            this.mnuPlexForms.TabIndex = 0;
            this.mnuPlexForms.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileNew,
            this.mnuFileOpen,
            this.mnuFileSave,
            this.mnuFileSaveAs,
            this.mnuFileClose,
            this.toolStripMenuItem1,
            this.mnuFilePrintPreview,
            this.mnuFilePrint,
            this.mnuFilePrintToFile,
            this.toolStripMenuItem2,
            this.mnuFileEmailForm,
            this.toolStripMenuItem10,
            this.mnuFileProperties,
            this.toolStripMenuItem3,
            this.mnuFileImport,
            this.mnuFileExport,
            this.toolStripMenuItem7,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "File";
            // 
            // mnuFileNew
            // 
            this.mnuFileNew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileNewForm,
            this.mnuFileNewField,
            this.mnuFileNewRule});
            this.mnuFileNew.Name = "mnuFileNew";
            this.mnuFileNew.Size = new System.Drawing.Size(143, 22);
            this.mnuFileNew.Text = "New";
            // 
            // mnuFileNewForm
            // 
            this.mnuFileNewForm.Name = "mnuFileNewForm";
            this.mnuFileNewForm.Size = new System.Drawing.Size(104, 22);
            this.mnuFileNewForm.Text = "Form";
            this.mnuFileNewForm.Click += new System.EventHandler(this.mnuFileNewForm_Click);
            // 
            // mnuFileNewField
            // 
            this.mnuFileNewField.Name = "mnuFileNewField";
            this.mnuFileNewField.Size = new System.Drawing.Size(104, 22);
            this.mnuFileNewField.Text = "Fields";
            this.mnuFileNewField.Click += new System.EventHandler(this.mnuFileNewField_Click);
            // 
            // mnuFileNewRule
            // 
            this.mnuFileNewRule.Name = "mnuFileNewRule";
            this.mnuFileNewRule.Size = new System.Drawing.Size(104, 22);
            this.mnuFileNewRule.Text = "Rule";
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Name = "mnuFileOpen";
            this.mnuFileOpen.Size = new System.Drawing.Size(143, 22);
            this.mnuFileOpen.Text = "Open";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuFileSave.Size = new System.Drawing.Size(143, 22);
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveAs
            // 
            this.mnuFileSaveAs.Name = "mnuFileSaveAs";
            this.mnuFileSaveAs.Size = new System.Drawing.Size(143, 22);
            this.mnuFileSaveAs.Text = "Save As";
            this.mnuFileSaveAs.Click += new System.EventHandler(this.mnuFileSaveAs_Click);
            // 
            // mnuFileClose
            // 
            this.mnuFileClose.Name = "mnuFileClose";
            this.mnuFileClose.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.mnuFileClose.Size = new System.Drawing.Size(143, 22);
            this.mnuFileClose.Text = "Close";
            this.mnuFileClose.Click += new System.EventHandler(this.mnuFileClose_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuFilePrintPreview
            // 
            this.mnuFilePrintPreview.Name = "mnuFilePrintPreview";
            this.mnuFilePrintPreview.Size = new System.Drawing.Size(143, 22);
            this.mnuFilePrintPreview.Text = "Print Preview";
            this.mnuFilePrintPreview.Click += new System.EventHandler(this.mnuFilePrintPreview_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.mnuFilePrint.Size = new System.Drawing.Size(143, 22);
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePrintToFile
            // 
            this.mnuFilePrintToFile.Name = "mnuFilePrintToFile";
            this.mnuFilePrintToFile.Size = new System.Drawing.Size(143, 22);
            this.mnuFilePrintToFile.Text = "Print to File";
            this.mnuFilePrintToFile.Click += new System.EventHandler(this.mnuFilePrintToFile_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuFileEmailForm
            // 
            this.mnuFileEmailForm.Name = "mnuFileEmailForm";
            this.mnuFileEmailForm.Size = new System.Drawing.Size(143, 22);
            this.mnuFileEmailForm.Text = "Email Form...";
            this.mnuFileEmailForm.Click += new System.EventHandler(this.mnuFileEmailForm_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuFileProperties
            // 
            this.mnuFileProperties.Name = "mnuFileProperties";
            this.mnuFileProperties.Size = new System.Drawing.Size(143, 22);
            this.mnuFileProperties.Text = "Properties";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuFileImport
            // 
            this.mnuFileImport.Name = "mnuFileImport";
            this.mnuFileImport.Size = new System.Drawing.Size(143, 22);
            this.mnuFileImport.Text = "Import";
            this.mnuFileImport.Click += new System.EventHandler(this.mnuFileImport_Click);
            // 
            // mnuFileExport
            // 
            this.mnuFileExport.Name = "mnuFileExport";
            this.mnuFileExport.Size = new System.Drawing.Size(143, 22);
            this.mnuFileExport.Text = "Export";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Size = new System.Drawing.Size(143, 22);
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // mnuEdit
            // 
            this.mnuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuEditCut,
            this.mnuEditCopy,
            this.mnuEditPaste,
            this.toolStripMenuItem4,
            this.mnuEditDelete});
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Size = new System.Drawing.Size(39, 20);
            this.mnuEdit.Text = "Edit";
            // 
            // mnuEditCut
            // 
            this.mnuEditCut.Name = "mnuEditCut";
            this.mnuEditCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.mnuEditCut.Size = new System.Drawing.Size(149, 22);
            this.mnuEditCut.Text = "Cut";
            this.mnuEditCut.Click += new System.EventHandler(this.mnuEditCut_Click);
            // 
            // mnuEditCopy
            // 
            this.mnuEditCopy.Name = "mnuEditCopy";
            this.mnuEditCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.mnuEditCopy.Size = new System.Drawing.Size(149, 22);
            this.mnuEditCopy.Text = "Copy";
            this.mnuEditCopy.Click += new System.EventHandler(this.mnuEditCopy_Click);
            // 
            // mnuEditPaste
            // 
            this.mnuEditPaste.Name = "mnuEditPaste";
            this.mnuEditPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.mnuEditPaste.Size = new System.Drawing.Size(149, 22);
            this.mnuEditPaste.Text = "Paste";
            this.mnuEditPaste.Click += new System.EventHandler(this.mnuEditPaste_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(146, 6);
            // 
            // mnuEditDelete
            // 
            this.mnuEditDelete.Name = "mnuEditDelete";
            this.mnuEditDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuEditDelete.Size = new System.Drawing.Size(149, 22);
            this.mnuEditDelete.Text = "Delete";
            // 
            // mnuTools
            // 
            this.mnuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuToolsOptions,
            this.mnuToolsSetTabstops,
            this.toolStripMenuItem8,
            this.mnuToolsEditForm,
            this.mnuToolsCloseEdit,
            this.mnuToolsUploadForm,
            this.mnuToolsTouch,
            this.mnuToolsKeyboard});
            this.mnuTools.Name = "mnuTools";
            this.mnuTools.Size = new System.Drawing.Size(48, 20);
            this.mnuTools.Text = "Tools";
            // 
            // mnuToolsOptions
            // 
            this.mnuToolsOptions.Name = "mnuToolsOptions";
            this.mnuToolsOptions.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsOptions.Text = "&Options";
            this.mnuToolsOptions.Click += new System.EventHandler(this.mnuToolsOptions_Click);
            // 
            // mnuToolsSetTabstops
            // 
            this.mnuToolsSetTabstops.Name = "mnuToolsSetTabstops";
            this.mnuToolsSetTabstops.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsSetTabstops.Text = "Set &Tabstops";
            this.mnuToolsSetTabstops.Click += new System.EventHandler(this.mnuToolsSetTabstops_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(140, 6);
            // 
            // mnuToolsEditForm
            // 
            this.mnuToolsEditForm.Name = "mnuToolsEditForm";
            this.mnuToolsEditForm.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsEditForm.Text = "&Edit Form";
            this.mnuToolsEditForm.Click += new System.EventHandler(this.mnuToolsEditForm_Click);
            // 
            // mnuToolsCloseEdit
            // 
            this.mnuToolsCloseEdit.Name = "mnuToolsCloseEdit";
            this.mnuToolsCloseEdit.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsCloseEdit.Tag = "EDIT";
            this.mnuToolsCloseEdit.Text = "&Close Edit";
            this.mnuToolsCloseEdit.Click += new System.EventHandler(this.mnuToolsCloseEdit_Click);
            // 
            // mnuToolsUploadForm
            // 
            this.mnuToolsUploadForm.Name = "mnuToolsUploadForm";
            this.mnuToolsUploadForm.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsUploadForm.Text = "&Upload Form";
            this.mnuToolsUploadForm.Click += new System.EventHandler(this.mnuToolsUploadForm_Click);
            // 
            // mnuToolsTouch
            // 
            this.mnuToolsTouch.CheckOnClick = true;
            this.mnuToolsTouch.Name = "mnuToolsTouch";
            this.mnuToolsTouch.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsTouch.Text = "Touch";
            this.mnuToolsTouch.Click += new System.EventHandler(this.mnuToolsTouch_Click);
            // 
            // mnuToolsKeyboard
            // 
            this.mnuToolsKeyboard.CheckOnClick = true;
            this.mnuToolsKeyboard.Name = "mnuToolsKeyboard";
            this.mnuToolsKeyboard.Size = new System.Drawing.Size(143, 22);
            this.mnuToolsKeyboard.Text = "Keyboard";
            this.mnuToolsKeyboard.Click += new System.EventHandler(this.mnuToolsKeyboard_Click);
            // 
            // mnuWindows
            // 
            this.mnuWindows.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuWindowsCascade,
            this.mnuWindowsTile});
            this.mnuWindows.Name = "mnuWindows";
            this.mnuWindows.Size = new System.Drawing.Size(68, 20);
            this.mnuWindows.Text = "Windows";
            // 
            // mnuWindowsCascade
            // 
            this.mnuWindowsCascade.Name = "mnuWindowsCascade";
            this.mnuWindowsCascade.Size = new System.Drawing.Size(118, 22);
            this.mnuWindowsCascade.Text = "Cascade";
            // 
            // mnuWindowsTile
            // 
            this.mnuWindowsTile.Name = "mnuWindowsTile";
            this.mnuWindowsTile.Size = new System.Drawing.Size(118, 22);
            this.mnuWindowsTile.Text = "Tile";
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(44, 20);
            this.mnuHelp.Text = "Help";
            // 
            // mnuHelpAbout
            // 
            this.mnuHelpAbout.Name = "mnuHelpAbout";
            this.mnuHelpAbout.Size = new System.Drawing.Size(107, 22);
            this.mnuHelpAbout.Text = "About";
            this.mnuHelpAbout.Click += new System.EventHandler(this.mnuHelpAbout_Click);
            // 
            // pnlStatus
            // 
            this.pnlStatus.BackColor = System.Drawing.SystemColors.Control;
            this.pnlStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlStatus.Controls.Add(this.lblCoordinates);
            this.pnlStatus.Controls.Add(this.picPoweredByPlexsoft);
            this.pnlStatus.Controls.Add(this.lblClock);
            this.pnlStatus.Controls.Add(this.lblStatus);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatus.Location = new System.Drawing.Point(0, 478);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(1148, 33);
            this.pnlStatus.TabIndex = 2;
            this.pnlStatus.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStatus_Paint);
            // 
            // lblCoordinates
            // 
            this.lblCoordinates.BackColor = System.Drawing.SystemColors.Control;
            this.lblCoordinates.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCoordinates.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoordinates.Location = new System.Drawing.Point(435, 1);
            this.lblCoordinates.Name = "lblCoordinates";
            this.lblCoordinates.Size = new System.Drawing.Size(153, 29);
            this.lblCoordinates.TabIndex = 3;
            this.lblCoordinates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picPoweredByPlexsoft
            // 
            this.picPoweredByPlexsoft.BackgroundImage = global::PlexForms.Properties.Resources.poweredbyplexsoft1;
            this.picPoweredByPlexsoft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picPoweredByPlexsoft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picPoweredByPlexsoft.Location = new System.Drawing.Point(894, 1);
            this.picPoweredByPlexsoft.Name = "picPoweredByPlexsoft";
            this.picPoweredByPlexsoft.Size = new System.Drawing.Size(140, 29);
            this.picPoweredByPlexsoft.TabIndex = 2;
            this.picPoweredByPlexsoft.TabStop = false;
            this.picPoweredByPlexsoft.Visible = false;
            // 
            // lblClock
            // 
            this.lblClock.BackColor = System.Drawing.SystemColors.Control;
            this.lblClock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblClock.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClock.Location = new System.Drawing.Point(591, 0);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(244, 29);
            this.lblClock.TabIndex = 1;
            this.lblClock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.SystemColors.Control;
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStatus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblStatus.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(3, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(383, 29);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Ready";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabForms
            // 
            this.tabForms.Controls.Add(this.tabPage1);
            this.tabForms.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabForms.Location = new System.Drawing.Point(313, 324);
            this.tabForms.Name = "tabForms";
            this.tabForms.SelectedIndex = 0;
            this.tabForms.Size = new System.Drawing.Size(460, 176);
            this.tabForms.TabIndex = 3;
            this.tabForms.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(452, 140);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "(None)";
            // 
            // tbrPlexForms
            // 
            this.tbrPlexForms.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tbrPlexForms.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tbrPlexForms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbrPlexFormsOpen,
            this.tbrPlexFormsSave,
            this.tbrPlexFormsNew,
            this.tbrPlexFormsAdd,
            this.tbrPlexFormsClose,
            this.tbrPlexFormsPrint,
            this.tbrPlexFormsPrintPreview,
            this.tbrPlexFormsEmail,
            this.tbrPlexFormsProperties,
            this.tbrPlexFormsSettings,
            this.tbrPlexFormsHelp,
            this.tbrPlexFormsExit});
            this.tbrPlexForms.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tbrPlexForms.Location = new System.Drawing.Point(0, 24);
            this.tbrPlexForms.Name = "tbrPlexForms";
            this.tbrPlexForms.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tbrPlexForms.Size = new System.Drawing.Size(1148, 38);
            this.tbrPlexForms.TabIndex = 4;
            // 
            // tbrPlexFormsOpen
            // 
            this.tbrPlexFormsOpen.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsOpen.Image")));
            this.tbrPlexFormsOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsOpen.Name = "tbrPlexFormsOpen";
            this.tbrPlexFormsOpen.Size = new System.Drawing.Size(40, 35);
            this.tbrPlexFormsOpen.Text = "Open";
            this.tbrPlexFormsOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsOpen.Click += new System.EventHandler(this.tbrPlexFormsOpen_Click);
            // 
            // tbrPlexFormsSave
            // 
            this.tbrPlexFormsSave.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsSave.Image")));
            this.tbrPlexFormsSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsSave.Name = "tbrPlexFormsSave";
            this.tbrPlexFormsSave.Size = new System.Drawing.Size(35, 35);
            this.tbrPlexFormsSave.Text = "Save";
            this.tbrPlexFormsSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsSave.Click += new System.EventHandler(this.tbrPlexFormsSave_Click);
            // 
            // tbrPlexFormsNew
            // 
            this.tbrPlexFormsNew.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsNew.Image")));
            this.tbrPlexFormsNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsNew.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.tbrPlexFormsNew.Name = "tbrPlexFormsNew";
            this.tbrPlexFormsNew.Size = new System.Drawing.Size(35, 35);
            this.tbrPlexFormsNew.Tag = "EDIT";
            this.tbrPlexFormsNew.Text = "New";
            this.tbrPlexFormsNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsNew.Click += new System.EventHandler(this.tbrPlexFormsNew_Click);
            // 
            // tbrPlexFormsAdd
            // 
            this.tbrPlexFormsAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsAdd.Image")));
            this.tbrPlexFormsAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsAdd.Name = "tbrPlexFormsAdd";
            this.tbrPlexFormsAdd.Size = new System.Drawing.Size(33, 35);
            this.tbrPlexFormsAdd.Tag = "EDIT";
            this.tbrPlexFormsAdd.Text = "Add";
            this.tbrPlexFormsAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsAdd.Click += new System.EventHandler(this.tbrPlexFormsAdd_Click);
            // 
            // tbrPlexFormsClose
            // 
            this.tbrPlexFormsClose.Image = global::PlexForms.Properties.Resources.CLOSED1;
            this.tbrPlexFormsClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsClose.Name = "tbrPlexFormsClose";
            this.tbrPlexFormsClose.Size = new System.Drawing.Size(40, 35);
            this.tbrPlexFormsClose.Text = "Close";
            this.tbrPlexFormsClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsClose.Click += new System.EventHandler(this.tbrPlexFormsClose_Click);
            // 
            // tbrPlexFormsPrint
            // 
            this.tbrPlexFormsPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsPrint.Image")));
            this.tbrPlexFormsPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsPrint.Name = "tbrPlexFormsPrint";
            this.tbrPlexFormsPrint.Size = new System.Drawing.Size(36, 35);
            this.tbrPlexFormsPrint.Text = "Print";
            this.tbrPlexFormsPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsPrint.Click += new System.EventHandler(this.tbrPlexFormsPrint_Click);
            // 
            // tbrPlexFormsPrintPreview
            // 
            this.tbrPlexFormsPrintPreview.Image = global::PlexForms.Properties.Resources.PREVIEW;
            this.tbrPlexFormsPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsPrintPreview.Name = "tbrPlexFormsPrintPreview";
            this.tbrPlexFormsPrintPreview.Size = new System.Drawing.Size(52, 35);
            this.tbrPlexFormsPrintPreview.Text = "Preview";
            this.tbrPlexFormsPrintPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsPrintPreview.ToolTipText = "Print Preview";
            this.tbrPlexFormsPrintPreview.Click += new System.EventHandler(this.tbrPlexFormsPrintPreview_Click);
            // 
            // tbrPlexFormsEmail
            // 
            this.tbrPlexFormsEmail.Image = global::PlexForms.Properties.Resources.email;
            this.tbrPlexFormsEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsEmail.Name = "tbrPlexFormsEmail";
            this.tbrPlexFormsEmail.Size = new System.Drawing.Size(40, 35);
            this.tbrPlexFormsEmail.Text = "Email";
            this.tbrPlexFormsEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsEmail.Click += new System.EventHandler(this.tbrPlexFormsEmail_Click);
            // 
            // tbrPlexFormsProperties
            // 
            this.tbrPlexFormsProperties.Image = global::PlexForms.Properties.Resources.EDI;
            this.tbrPlexFormsProperties.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsProperties.Name = "tbrPlexFormsProperties";
            this.tbrPlexFormsProperties.Size = new System.Drawing.Size(64, 35);
            this.tbrPlexFormsProperties.Text = "Properties";
            this.tbrPlexFormsProperties.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsProperties.ToolTipText = "Properties";
            this.tbrPlexFormsProperties.Click += new System.EventHandler(this.tbrPlexFormsProperties_Click);
            // 
            // tbrPlexFormsSettings
            // 
            this.tbrPlexFormsSettings.Image = global::PlexForms.Properties.Resources.Settings;
            this.tbrPlexFormsSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsSettings.Name = "tbrPlexFormsSettings";
            this.tbrPlexFormsSettings.Size = new System.Drawing.Size(53, 35);
            this.tbrPlexFormsSettings.Text = "Settings";
            this.tbrPlexFormsSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbrPlexFormsHelp
            // 
            this.tbrPlexFormsHelp.Image = ((System.Drawing.Image)(resources.GetObject("tbrPlexFormsHelp.Image")));
            this.tbrPlexFormsHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsHelp.Name = "tbrPlexFormsHelp";
            this.tbrPlexFormsHelp.Size = new System.Drawing.Size(36, 35);
            this.tbrPlexFormsHelp.Text = "Help";
            this.tbrPlexFormsHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbrPlexFormsExit
            // 
            this.tbrPlexFormsExit.Image = global::PlexForms.Properties.Resources.exit2;
            this.tbrPlexFormsExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbrPlexFormsExit.Name = "tbrPlexFormsExit";
            this.tbrPlexFormsExit.Size = new System.Drawing.Size(29, 35);
            this.tbrPlexFormsExit.Text = "Exit";
            this.tbrPlexFormsExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbrPlexFormsExit.ToolTipText = "Exit PlexForms";
            this.tbrPlexFormsExit.Click += new System.EventHandler(this.tbrPlexFormsExit_Click);
            // 
            // cmnuRightClick
            // 
            this.cmnuRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmnuRightClickAddField,
            this.cmnuRightClickMoveField,
            this.toolStripMenuItem6,
            this.cmnuRightClickCutField,
            this.cmnuRightClickCopyField,
            this.cmnuRightClickPasteField,
            this.toolStripMenuItem5,
            this.cmnuRightClickDeleteField,
            this.cmnuRightClickChangeField,
            this.cmnuRightClickProperties});
            this.cmnuRightClick.Name = "cmnuRightClick";
            this.cmnuRightClick.Size = new System.Drawing.Size(128, 192);
            this.cmnuRightClick.Opening += new System.ComponentModel.CancelEventHandler(this.cmnuRightClick_Opening);
            // 
            // cmnuRightClickAddField
            // 
            this.cmnuRightClickAddField.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmnuRightClickAddFieldTextbox,
            this.cmnuRightClickAddFieldCombobox,
            this.cmnuRightClickAddFieldListbox,
            this.cmnuRightClickAddFieldButton,
            this.cmnuRightClickAddFieldLabel,
            this.cmnuRightClickAddFieldCheckbox,
            this.cmnuRightClickAddFieldOption,
            this.cmnuRightClickAddFieldGrid,
            this.cmnuRightClickAddFieldFrame});
            this.cmnuRightClickAddField.Name = "cmnuRightClickAddField";
            this.cmnuRightClickAddField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickAddField.Text = "Add Field";
            // 
            // cmnuRightClickAddFieldTextbox
            // 
            this.cmnuRightClickAddFieldTextbox.Name = "cmnuRightClickAddFieldTextbox";
            this.cmnuRightClickAddFieldTextbox.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldTextbox.Text = "Textbox";
            // 
            // cmnuRightClickAddFieldCombobox
            // 
            this.cmnuRightClickAddFieldCombobox.Name = "cmnuRightClickAddFieldCombobox";
            this.cmnuRightClickAddFieldCombobox.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldCombobox.Text = "Combobox";
            // 
            // cmnuRightClickAddFieldListbox
            // 
            this.cmnuRightClickAddFieldListbox.Name = "cmnuRightClickAddFieldListbox";
            this.cmnuRightClickAddFieldListbox.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldListbox.Text = "Listbox";
            // 
            // cmnuRightClickAddFieldButton
            // 
            this.cmnuRightClickAddFieldButton.Name = "cmnuRightClickAddFieldButton";
            this.cmnuRightClickAddFieldButton.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldButton.Text = "Button";
            // 
            // cmnuRightClickAddFieldLabel
            // 
            this.cmnuRightClickAddFieldLabel.Name = "cmnuRightClickAddFieldLabel";
            this.cmnuRightClickAddFieldLabel.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldLabel.Text = "Label";
            // 
            // cmnuRightClickAddFieldCheckbox
            // 
            this.cmnuRightClickAddFieldCheckbox.Name = "cmnuRightClickAddFieldCheckbox";
            this.cmnuRightClickAddFieldCheckbox.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldCheckbox.Text = "Checkbox";
            // 
            // cmnuRightClickAddFieldOption
            // 
            this.cmnuRightClickAddFieldOption.Name = "cmnuRightClickAddFieldOption";
            this.cmnuRightClickAddFieldOption.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldOption.Text = "Option";
            // 
            // cmnuRightClickAddFieldGrid
            // 
            this.cmnuRightClickAddFieldGrid.Name = "cmnuRightClickAddFieldGrid";
            this.cmnuRightClickAddFieldGrid.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldGrid.Text = "Grid";
            // 
            // cmnuRightClickAddFieldFrame
            // 
            this.cmnuRightClickAddFieldFrame.Name = "cmnuRightClickAddFieldFrame";
            this.cmnuRightClickAddFieldFrame.Size = new System.Drawing.Size(133, 22);
            this.cmnuRightClickAddFieldFrame.Text = "Frame";
            // 
            // cmnuRightClickMoveField
            // 
            this.cmnuRightClickMoveField.Name = "cmnuRightClickMoveField";
            this.cmnuRightClickMoveField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickMoveField.Text = "Move...";
            this.cmnuRightClickMoveField.Click += new System.EventHandler(this.cmnuRightClickMoveField_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(124, 6);
            // 
            // cmnuRightClickCutField
            // 
            this.cmnuRightClickCutField.Name = "cmnuRightClickCutField";
            this.cmnuRightClickCutField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickCutField.Text = "Cut";
            this.cmnuRightClickCutField.Click += new System.EventHandler(this.cmnuRightClickCutField_Click);
            // 
            // cmnuRightClickCopyField
            // 
            this.cmnuRightClickCopyField.Name = "cmnuRightClickCopyField";
            this.cmnuRightClickCopyField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickCopyField.Text = "Copy";
            this.cmnuRightClickCopyField.Click += new System.EventHandler(this.cmnuRightClickCopyField_Click);
            // 
            // cmnuRightClickPasteField
            // 
            this.cmnuRightClickPasteField.Enabled = false;
            this.cmnuRightClickPasteField.Name = "cmnuRightClickPasteField";
            this.cmnuRightClickPasteField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickPasteField.Text = "Paste";
            this.cmnuRightClickPasteField.Click += new System.EventHandler(this.cmnuRightClickPasteField_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(124, 6);
            // 
            // cmnuRightClickDeleteField
            // 
            this.cmnuRightClickDeleteField.Name = "cmnuRightClickDeleteField";
            this.cmnuRightClickDeleteField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickDeleteField.Text = "Delete";
            this.cmnuRightClickDeleteField.Click += new System.EventHandler(this.cmnuRightClickDeleteField_Click);
            // 
            // cmnuRightClickChangeField
            // 
            this.cmnuRightClickChangeField.Name = "cmnuRightClickChangeField";
            this.cmnuRightClickChangeField.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickChangeField.Text = "Change...";
            // 
            // cmnuRightClickProperties
            // 
            this.cmnuRightClickProperties.Name = "cmnuRightClickProperties";
            this.cmnuRightClickProperties.Size = new System.Drawing.Size(127, 22);
            this.cmnuRightClickProperties.Text = "Properties";
            this.cmnuRightClickProperties.Click += new System.EventHandler(this.cmnuRightClickProperties_Click);
            // 
            // tmrClock
            // 
            this.tmrClock.Enabled = true;
            this.tmrClock.Interval = 1000;
            this.tmrClock.Tick += new System.EventHandler(this.tmrClock_Tick);
            // 
            // pnlControls
            // 
            this.pnlControls.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlControls.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlControls.Controls.Add(this.tabAttributes);
            this.pnlControls.Controls.Add(this.button9);
            this.pnlControls.Controls.Add(this.button10);
            this.pnlControls.Controls.Add(this.lblPalletSize);
            this.pnlControls.Controls.Add(this.button8);
            this.pnlControls.Controls.Add(this.button7);
            this.pnlControls.Controls.Add(this.button6);
            this.pnlControls.Controls.Add(this.chkOneClick);
            this.pnlControls.Controls.Add(this.lblToolBox);
            this.pnlControls.Controls.Add(this.button5);
            this.pnlControls.Controls.Add(this.button4);
            this.pnlControls.Controls.Add(this.button3);
            this.pnlControls.Controls.Add(this.button2);
            this.pnlControls.Controls.Add(this.button1);
            this.pnlControls.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlControls.Location = new System.Drawing.Point(856, 62);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(292, 416);
            this.pnlControls.TabIndex = 5;
            // 
            // tabAttributes
            // 
            this.tabAttributes.Controls.Add(this.tabPageAttributes);
            this.tabAttributes.Controls.Add(this.tabPageRules);
            this.tabAttributes.Location = new System.Drawing.Point(8, 206);
            this.tabAttributes.Name = "tabAttributes";
            this.tabAttributes.SelectedIndex = 0;
            this.tabAttributes.Size = new System.Drawing.Size(276, 349);
            this.tabAttributes.TabIndex = 15;
            // 
            // tabPageAttributes
            // 
            this.tabPageAttributes.Controls.Add(this.grdAttributes);
            this.tabPageAttributes.Location = new System.Drawing.Point(4, 23);
            this.tabPageAttributes.Name = "tabPageAttributes";
            this.tabPageAttributes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAttributes.Size = new System.Drawing.Size(268, 322);
            this.tabPageAttributes.TabIndex = 0;
            this.tabPageAttributes.Text = "Properties";
            this.tabPageAttributes.UseVisualStyleBackColor = true;
            // 
            // grdAttributes
            // 
            this.grdAttributes.AllowUserToDeleteRows = false;
            this.grdAttributes.AllowUserToOrderColumns = true;
            this.grdAttributes.AllowUserToResizeColumns = false;
            this.grdAttributes.AllowUserToResizeRows = false;
            this.grdAttributes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAttributes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Attribute,
            this.AttributeValue});
            this.grdAttributes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdAttributes.Location = new System.Drawing.Point(3, 5);
            this.grdAttributes.MultiSelect = false;
            this.grdAttributes.Name = "grdAttributes";
            this.grdAttributes.RowHeadersVisible = false;
            this.grdAttributes.Size = new System.Drawing.Size(262, 306);
            this.grdAttributes.TabIndex = 12;
            // 
            // Attribute
            // 
            this.Attribute.Frozen = true;
            this.Attribute.HeaderText = "Attribute";
            this.Attribute.Name = "Attribute";
            this.Attribute.Width = 50;
            // 
            // AttributeValue
            // 
            this.AttributeValue.Frozen = true;
            this.AttributeValue.HeaderText = "Value";
            this.AttributeValue.Name = "AttributeValue";
            this.AttributeValue.Width = 600;
            // 
            // tabPageRules
            // 
            this.tabPageRules.Controls.Add(this.txtRules);
            this.tabPageRules.Location = new System.Drawing.Point(4, 23);
            this.tabPageRules.Name = "tabPageRules";
            this.tabPageRules.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRules.Size = new System.Drawing.Size(268, 322);
            this.tabPageRules.TabIndex = 1;
            this.tabPageRules.Text = "Rules";
            this.tabPageRules.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(149, 139);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(135, 23);
            this.button9.TabIndex = 14;
            this.button9.Tag = "";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(8, 139);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(135, 23);
            this.button10.TabIndex = 13;
            this.button10.Tag = "";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            // 
            // lblPalletSize
            // 
            this.lblPalletSize.AutoSize = true;
            this.lblPalletSize.Location = new System.Drawing.Point(4, 177);
            this.lblPalletSize.Name = "lblPalletSize";
            this.lblPalletSize.Size = new System.Drawing.Size(41, 14);
            this.lblPalletSize.TabIndex = 12;
            this.lblPalletSize.Text = "label1";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(149, 116);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(135, 23);
            this.button8.TabIndex = 11;
            this.button8.Tag = "GRID";
            this.button8.Text = "Grid";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(8, 116);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(135, 23);
            this.button7.TabIndex = 8;
            this.button7.Tag = "OPTION";
            this.button7.Text = "Option";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(149, 93);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(135, 23);
            this.button6.TabIndex = 7;
            this.button6.Tag = "CHECKBOX";
            this.button6.Text = "Checkbox";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // chkOneClick
            // 
            this.chkOneClick.AutoSize = true;
            this.chkOneClick.Location = new System.Drawing.Point(6, 28);
            this.chkOneClick.Name = "chkOneClick";
            this.chkOneClick.Size = new System.Drawing.Size(80, 18);
            this.chkOneClick.TabIndex = 6;
            this.chkOneClick.Text = "One Click";
            this.chkOneClick.UseVisualStyleBackColor = true;
            // 
            // lblToolBox
            // 
            this.lblToolBox.BackColor = System.Drawing.Color.MidnightBlue;
            this.lblToolBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToolBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblToolBox.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToolBox.ForeColor = System.Drawing.Color.LemonChiffon;
            this.lblToolBox.Location = new System.Drawing.Point(0, 0);
            this.lblToolBox.Name = "lblToolBox";
            this.lblToolBox.Size = new System.Drawing.Size(288, 25);
            this.lblToolBox.TabIndex = 5;
            this.lblToolBox.Text = "ToolBox";
            this.lblToolBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(149, 69);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(135, 23);
            this.button5.TabIndex = 4;
            this.button5.Tag = "LISTBOX";
            this.button5.Text = "Listbox";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(8, 93);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(135, 23);
            this.button4.TabIndex = 3;
            this.button4.Tag = "COMBOBOX";
            this.button4.Text = "Combobox";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 69);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 23);
            this.button3.TabIndex = 2;
            this.button3.Tag = "LABEL";
            this.button3.Text = "Label";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(149, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 23);
            this.button2.TabIndex = 1;
            this.button2.Tag = "TEXTBOX";
            this.button2.Text = "Textbox";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 23);
            this.button1.TabIndex = 0;
            this.button1.Tag = "BUTTON";
            this.button1.Text = "Button";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // objPrintPreviewDialog
            // 
            this.objPrintPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.objPrintPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.objPrintPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.objPrintPreviewDialog.Enabled = true;
            this.objPrintPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("objPrintPreviewDialog.Icon")));
            this.objPrintPreviewDialog.Name = "objPrintPreviewDialog";
            this.objPrintPreviewDialog.Visible = false;
            // 
            // picLogo
            // 
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picLogo.Location = new System.Drawing.Point(238, 172);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(300, 300);
            this.picLogo.TabIndex = 6;
            this.picLogo.TabStop = false;
            this.picLogo.Visible = false;
            // 
            // txtLogo
            // 
            this.txtLogo.BackColor = System.Drawing.Color.White;
            this.txtLogo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLogo.Font = new System.Drawing.Font("Tahoma", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogo.ForeColor = System.Drawing.Color.MediumBlue;
            this.txtLogo.HideSelection = false;
            this.txtLogo.Location = new System.Drawing.Point(21, 65);
            this.txtLogo.Multiline = true;
            this.txtLogo.Name = "txtLogo";
            this.txtLogo.ReadOnly = true;
            this.txtLogo.Size = new System.Drawing.Size(297, 169);
            this.txtLogo.TabIndex = 7;
            this.txtLogo.TabStop = false;
            this.txtLogo.Text = "Text";
            this.txtLogo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtRules
            // 
            this.txtRules.Location = new System.Drawing.Point(8, 7);
            this.txtRules.Multiline = true;
            this.txtRules.Name = "txtRules";
            this.txtRules.Size = new System.Drawing.Size(245, 131);
            this.txtRules.TabIndex = 13;
            // 
            // PlexForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1148, 511);
            this.Controls.Add(this.pnlControls);
            this.Controls.Add(this.txtLogo);
            this.Controls.Add(this.tbrPlexForms);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.mnuPlexForms);
            this.Controls.Add(this.tabForms);
            this.Font = new System.Drawing.Font("Lucida Fax", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.mnuPlexForms;
            this.Name = "PlexForms";
            this.Text = "Plexsoft PlexForms Utility";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PlexForms_Load);
            this.mnuPlexForms.ResumeLayout(false);
            this.mnuPlexForms.PerformLayout();
            this.pnlStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPoweredByPlexsoft)).EndInit();
            this.tabForms.ResumeLayout(false);
            this.tbrPlexForms.ResumeLayout(false);
            this.tbrPlexForms.PerformLayout();
            this.cmnuRightClick.ResumeLayout(false);
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.tabAttributes.ResumeLayout(false);
            this.tabPageAttributes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttributes)).EndInit();
            this.tabPageRules.ResumeLayout(false);
            this.tabPageRules.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuPlexForms;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNew;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNewForm;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNewField;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNewRule;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuFileClose;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrintPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuTools;
        private System.Windows.Forms.ToolStripMenuItem mnuWindows;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrint;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mnuFileProperties;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mnuEditCut;
        private System.Windows.Forms.ToolStripMenuItem mnuEditCopy;
        private System.Windows.Forms.ToolStripMenuItem mnuEditPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsOptions;
        private System.Windows.Forms.ToolStripMenuItem mnuWindowsCascade;
        private System.Windows.Forms.ToolStripMenuItem mnuWindowsTile;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.TabControl tabForms;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStrip tbrPlexForms;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsOpen;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsSave;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsPrint;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsHelp;
        private System.Windows.Forms.ContextMenuStrip cmnuRightClick;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddField;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldTextbox;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldCombobox;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldListbox;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldButton;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldLabel;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldCheckbox;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldOption;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldGrid;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickAddFieldFrame;
        private System.Windows.Forms.ToolStripMenuItem mnuFileImport;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExport;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickMoveField;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickCutField;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickCopyField;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickPasteField;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickDeleteField;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickChangeField;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsNew;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsAdd;
        private System.Windows.Forms.ToolStripMenuItem cmnuRightClickProperties;
        private System.Windows.Forms.Timer tmrClock;
        private System.Windows.Forms.PictureBox picPoweredByPlexsoft;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSave;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSaveAs;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsClose;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsExit;
        private System.Windows.Forms.Label lblToolBox;
        private System.Windows.Forms.CheckBox chkOneClick;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsSetTabstops;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsEditForm;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsSettings;
        private System.Windows.Forms.Label lblPalletSize;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsProperties;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsPrintPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsCloseEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuEditDelete;
        private System.Windows.Forms.PrintPreviewDialog objPrintPreviewDialog;
        private System.Drawing.Printing.PrintDocument objPrintDocument;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrintToFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileEmailForm;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.Label lblCoordinates;
        private System.Windows.Forms.TabControl tabAttributes;
        private System.Windows.Forms.TabPage tabPageAttributes;
        private System.Windows.Forms.DataGridView grdAttributes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attribute;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeValue;
        private System.Windows.Forms.TabPage tabPageRules;
        private System.Windows.Forms.ToolStripButton tbrPlexFormsEmail;
        private System.Windows.Forms.TextBox txtLogo;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsUploadForm;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsTouch;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsKeyboard;
        private System.Windows.Forms.TextBox txtRules;
    }
}

