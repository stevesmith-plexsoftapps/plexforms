﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class EmailForm : Form
    {
        private bool bolCancel = false;

        private TabControl objTabForms = null;
        public string strFrom = "";
        public string strTo = "";
        public string strSubject = "";
        public string strMessage = "";

        public EmailForm()
        {
            InitializeComponent();

            txtFrom.Text = "";
        }


        public EmailForm(TabControl argobjTabForms, string argstrFrom)
        {
            InitializeComponent();
            int intTop = lblFormNames.Top + lblFormNames.Height + 5;
            int intMaxWidth = 0;

            bolCancel = true;

            // fill in the fields
            objTabForms = argobjTabForms;
            txtFrom.Text = argstrFrom;

            // make a checkbox, form name and to textbox for each of the forms on the pallet
            for (int n = 0; n < objTabForms.TabPages.Count; n++)
            {
                // get the names for each form
                string strFormName = objTabForms.TabPages[n].Text;
                CheckBox chkFormName = new CheckBox();
                chkFormName.Name = "chkFormName_" + (n + 1).ToString();
                chkFormName.Text = strFormName;
                chkFormName.Tag = strFormName;
                chkFormName.Parent = this;
                chkFormName.Left = lblFormNames.Left;
                chkFormName.Width = lblEmailTo.Left - lblFormNames.Left - 10;
                chkFormName.Top = intTop + (n * (chkFormName.Height + 2));
                chkFormName.Visible = true;

                // now put a To textbox
                TextBox txtTo = new TextBox();
                txtTo.Name = "txtSendTo_" + (n + 1).ToString();
                txtTo.Left = lblEmailTo.Left;
                txtTo.Width = 300;
                txtTo.Top = chkFormName.Top;
                txtTo.Parent = this;
                txtTo.Visible = true;

                // get the email to send to.  each form should have an email address field on it
                string strEmailTo = clsFormInputRtns.GetFieldValue(strFormName, "EMAIL_ADDRESS");
                txtTo.Text = strEmailTo;
            }

        }




        private void btnSend_Click(object sender, EventArgs e)
        {

            lblStatus.Text = "Transmitting forms...";
            lblStatus.Refresh();
            Application.DoEvents();

            bolCancel = false;

            // send emails specified
            for (int n = 0; n < this.Controls.Count; n++)
            {

                if (bolCancel)
                {
                    break;
                }

                // for each chkForm... control, grab the corresponding send to
                Control objControl = this.Controls["chkFormName_" + (n + 1).ToString()];
                if (objControl != null)
                {
                    string strFormName = objControl.Tag.ToString();
                    CheckBox objCheckBox = (CheckBox)objControl;
                    if (objCheckBox.Checked)
                    {
                        if (objTabForms.TabPages[strFormName].Controls[0] != null)
                        {
                            PictureBox objSelectedPallet = (PictureBox)objTabForms.TabPages[strFormName].Controls[0];
                            if (objSelectedPallet != null)
                            {
                                TextBox objSendTo = (TextBox)this.Controls["txtSendTo_" + (n + 1).ToString()];
                                strTo = objSendTo.Text;
                                if (strTo.Length > 0)
                                {
                                    strSubject = "Form " + strFormName;
                                    strMessage = "Here is the form " + strFormName;
                                    lblStatus.Text = "Sending " + strFormName + "...";
                                    lblStatus.Refresh();
                                    Application.DoEvents();
                                    string strResponse = clsPlexPrint.EmailForm(objSelectedPallet, strTo, strSubject, strMessage);
                                    lblStatus.Text = strResponse;
                                    lblStatus.Refresh();
                                    Application.DoEvents();
                                }
                                else
                                {
                                    MessageBox.Show("You have selected the form " + strFormName + " but have not specified who to email it to.");
                                }
                            }
                        }
                    }
                }

            }

            lblStatus.Text = "Finished!";
            lblStatus.Refresh();
            Application.DoEvents();

            bolCancel = true;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            if (bolCancel)
            {
                Close();
            }
            else
            {
                bolCancel = true;
            }

        }





    }
}
