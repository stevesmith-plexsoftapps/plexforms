﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class PlexForms : Form
    {
        private Properties.Settings objSettings = new Properties.Settings();
        private System.Drawing.Color clrTextBoxColor = clsFormInputRtns.clrTextBoxColor;
        private System.Drawing.Color clrHightlightColor = clsFormInputRtns.clrHightlightColor;
        private System.Drawing.Color clrBackGroundColor = clsFormInputRtns.clrBackGroundColor;
        private System.Drawing.Color clrButtonColor = clsFormInputRtns.clrButtonColor;

        private bool bolStandardMode = false;
        private bool bolRedisplayForm = false;

        private double dblProportion = clsFormInputRtns.dblProportion;
        private int intPicFormHeight = clsFormInputRtns.intPicFormHeight;
        private int intPicFormWidth = clsFormInputRtns.intPicFormHeight;
        private int intMaxWidth = clsFormInputRtns.intMaxWidth;

        private string strToolboxControlSelected = "";
        private Control objSelectedToolboxControl = null;
        private Control objCopyControl = null;

        private bool bolControlMouseDown = false;
        private bool bolPictureMouseDown = false;
        private bool bolControlMouseClick = false;

        private int intPicClickX = 0;
        private int intPicClickY = 0;
        private int intPicClickWidth = 0;
        private int intPicClickHeight = 0;

        private bool bolSetControlAtClick = false;
        private bool bolMoveControl = false;
        private PictureBox objSelectedPallet = null;
        private Control objSelectedControl = null;
        private TabPage objSelectedTabPage = null;
        private Color objSelectedColor = clsFormInputRtns.objSelectedColor;
        private Color objNonselectedColor = clsFormInputRtns.objNonselectedColor;

        public PlexForms()
        {
            InitializeComponent();
        }


        /// <summary>
        /// PlexForms_Load - process the load of the PlexForms app
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlexForms_Load(object sender, EventArgs e)
        {
            InitializeApp();
        }


        private void InitializeApp()
        {

            // load the options
            LoadConfigSettings();

            // close the tabforms...
            CloseForm();

            // set the toolbar and menu items for initial load
            SetMenusAndToolbars("INITIAL");

            // set the event handlers
            this.Resize += new EventHandler(PlexForms_Resize);

            // position the base controls
            ResizeControls();

            pnlControls.Visible = false;

            // create the event routines for the context menus
            for (int n=0;n<cmnuRightClick.Items.Count;n++)
            {
                if (cmnuRightClick.Items[n].GetType().Name == "ToolStripMenuItem")
                {
                    ToolStripMenuItem objItem = (ToolStripMenuItem) cmnuRightClick.Items[n];
                    // set their click event to the same one, but not if it has dropdowns
                    if (objItem.DropDownItems.Count == 0)
                    {
                        objItem.Click += new EventHandler(RightClickMenuItems_Click);
                    }
                    else
                    {
                        for (int m = 0; m < objItem.DropDownItems.Count; m++)
                        {
                            if (objItem.DropDownItems[m].GetType().Name == "ToolStripMenuItem")
                            {
                                ToolStripMenuItem objDropdownItem = (ToolStripMenuItem)objItem.DropDownItems[m];
                                objDropdownItem.Click += new EventHandler(RightClickMenuItems_Click);

                                // one more time...
                                foreach (ToolStripMenuItem objDropdownItemDropdownItem in objDropdownItem.DropDownItems)
                                {
                                    // set the event
                                    objDropdownItemDropdownItem.Click += new EventHandler(RightClickMenuItems_Click);
                                }
                            }
                        }
                    }
                }

            }


            // create the click event for all of the toolbox controls
            for (int n = 0; n < pnlControls.Controls.Count; n++)
            {
                if (pnlControls.Controls[n].GetType().Name.ToUpper() == "BUTTON")
                {
                    pnlControls.Controls[n].Click += new EventHandler(pnlControls_Control_Click);
                }
            }

            // set the attribute grid events
            grdAttributes.Click += new EventHandler(grdAttributes_Click);
            grdAttributes.CellEndEdit += new DataGridViewCellEventHandler(grdAttributes_CellEndEdit);
            // freeze the first column
            grdAttributes.Columns[0].Frozen = true;
            grdAttributes.Columns[0].ReadOnly = true;
            grdAttributes.Columns[0].Width = 80;

            grdAttributes.Rows.Clear();
            grdAttributes.Rows.Add(30);

            // grab the keys pressed for the form
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(PlexForms_KeyDown);

            // set the click event for the tab
            tabForms.Click += new EventHandler(tabForms_Click);
            tabForms.DoubleClick += new EventHandler(tabForms_DoubleClick);

            clsCommonRtns.strCurrentPath = Application.ExecutablePath;
            // remove the exe name
            int intOffset = clsCommonRtns.strCurrentPath.LastIndexOf("\\");
            if (intOffset >= 0)
            {
                clsCommonRtns.strCurrentPath = clsCommonRtns.strCurrentPath.Substring(0, intOffset);
            }

            clsCommonRtns.strLastFormPath = clsCommonRtns.strCurrentPath + "\\formfiles";
            clsCommonRtns.strLastImagePath = clsCommonRtns.strLastFormPath + "\\images";

            clsCommonRtns.objStatus = lblStatus;

        }


        void LoadConfigSettings()
        {

            // load the config file
            clsCommonRtns.ReadConfigFile();

            // set the form souce (D or F)
            clsFormInputRtns.strFormSource = "F";

            // set the input type
            clsFormInputRtns.strTypeOfInput = clsCommonRtns.GetSetting("TypeOfInput").ToUpper();

            bolStandardMode = false;
            // set edit mode
            if (clsCommonRtns.GetSetting("InitialMode").ToUpper() == "EDIT")
            {
                clsFormInputRtns.bolEditMode = true;
            }
            else
            {
                clsFormInputRtns.bolEditMode = false;
                // set the initial mode
                if (clsCommonRtns.GetSetting("InitialMode").ToUpper() == "STANDARD")
                {
                    bolStandardMode = true;
                }
            }


            // set the designor mode
            if (clsCommonRtns.GetSetting("EnableDesignor").ToUpper() == "YES")
            {
                clsFormInputRtns.bolEnableDesigner = true;
            }
            else
            {
                clsFormInputRtns.bolEnableDesigner = false;
            }

            // set the properties
            clsCommonRtns.strFormsPath = clsCommonRtns.GetSetting("FormPath");

            // set the company name
            txtLogo.Text = clsCommonRtns.GetSetting("CompanyName");

        }


        
        void grdAttributes_Click(object sender, EventArgs e)
        {

            int intRow = grdAttributes.CurrentCell.RowIndex;

            if (grdAttributes.CurrentCell.ColumnIndex == 0)
            {
                // the attribute column, put focus on the value column
                grdAttributes.CurrentCell = grdAttributes.Rows[intRow].Cells[1];
            }

        }




        void grdAttributes_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (objSelectedControl == null)
            {
                return;
            }

            DataGridView objGrid = new DataGridView();
            string strType = objSelectedControl.GetType().Name.ToUpper();
            string strAttribute = "";
            string strValue = "";

            // set the value changed on the selected control
            int intRow = grdAttributes.CurrentCell.RowIndex;
            object objAttribute = grdAttributes.Rows[intRow].Cells["Attribute"].Value;
            if (objAttribute == null)
            {
                return;
            }

            strAttribute = objAttribute.ToString().ToUpper();

            object objValue = grdAttributes.Rows[intRow].Cells["AttributeValue"].Value;
            if (objValue == null)
            {
                strValue = "";
            }
            else
            {
                strValue = objValue.ToString();
            }

            switch (strAttribute)
            {
                case "NAME":
                    objSelectedControl.Name = strValue;
                    break;

                case "TEXT":
                    objSelectedControl.Text = strValue;
                    break;

                case "TOP":
                    objSelectedControl.Top = Convert.ToInt32(strValue);
                    break;

                case "LEFT":
                    objSelectedControl.Left = Convert.ToInt32(strValue); ;
                    break;

                case "WIDTH":
                    objSelectedControl.Width = Convert.ToInt32(strValue); ;
                    break;

                case "HEIGHT":
                    objSelectedControl.Height = Convert.ToInt32(strValue); ;
                    break;

                case "TABINDEX":
                    objSelectedControl.TabIndex = Convert.ToInt32(strValue); ;
                    break;

                case "COL_NAME":
                    if (strType == "DATAGRIDVIEW")
                    {
                        objGrid = (DataGridView)objSelectedControl;
                        objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Name = strValue;
                    }
                    break;

                case "COL_WIDTH":
                    if (strType == "DATAGRIDVIEW")
                    {
                        objGrid = (DataGridView)objSelectedControl;
                        objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Width = Convert.ToInt32(strValue);
                    }
                    break;

                case "COL_HEADERTEXT":
                    if (strType == "DATAGRIDVIEW")
                    {
                        objGrid = (DataGridView)objSelectedControl;
                        objGrid.Columns[objGrid.CurrentCell.ColumnIndex].HeaderText = strValue;
                    }
                    break;

                case "COL_HEADERHEIGHT":
                    if (strType == "DATAGRIDVIEW")
                    {
                        objGrid = (DataGridView)objSelectedControl;
                        objGrid.ColumnHeadersHeight = Convert.ToInt32(strValue);
                    }
                    break;

                case "COL_VALUE":
                    if (strType == "DATAGRIDVIEW")
                    {
                        objGrid = (DataGridView)objSelectedControl;
                        objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value = strValue;
                    }
                    break;

                case "LIST":
                    string strList = (strValue.Replace("[", "")).Replace("]", "");
                    string[] astrListItems = strList.Split('~');
                    if (strType == "COMBOBOX")
                    {
                        ComboBox objListControl = (ComboBox)objSelectedControl;
                        // load the values
                        for (int n = 0; n < astrListItems.Length; n++)
                        {
                            if (astrListItems[n].Trim() != "")
                            {
                                string[] astrCodeValuePair = astrListItems[n].Split(';');
                                if (astrCodeValuePair.Length > 0)
                                {
                                    objListControl.Items.Add(astrCodeValuePair[1]);
                                }
                            }
                        }
                    }
                    else
                    {
                        ListBox objListControl = (ListBox)objSelectedControl;
                        // load the values
                        for (int n = 0; n < astrListItems.Length; n++)
                        {
                            if (astrListItems[n].Trim() != "")
                            {
                                string[] astrCodeValuePair = astrListItems[n].Split(';');
                                if (astrCodeValuePair.Length > 0)
                                {
                                    objListControl.Items.Add(astrCodeValuePair[1]);
                                }
                            }
                        }
                    }
                    break;

            }

        }



        /// <summary>
        /// pnlControls_Control_Click - routine to process the click of one of the controls
        ///             on the pnlControls toolbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void pnlControls_Control_Click(object sender, EventArgs e)
        {
            Control objControl = (Control)sender;

            // see what control it is
            strToolboxControlSelected = objControl.Tag.ToString().ToUpper();
            bolSetControlAtClick = true;
            objSelectedToolboxControl = objControl;

        }




        void tabForms_DoubleClick(object sender, EventArgs e)
        {
            if (clsFormInputRtns.bolEditMode)
            {
                GetFormInfo("Change");
            }

        }



        void tabForms_Click(object sender, EventArgs e)
        {
            // set the selected picture to the tab selected picture
            int intTabSelected = tabForms.SelectedIndex;
            objSelectedTabPage = tabForms.TabPages[intTabSelected];
            if (tabForms.TabPages[intTabSelected].Controls.Count > 0)
            {
                objSelectedPallet = (PictureBox)tabForms.TabPages[intTabSelected].Controls[0];
            }

            // see if right clicked...

        }



        void PlexForms_KeyDown(object sender, KeyEventArgs e)
        {

            // see what key was pressed
            switch (e.KeyCode)
            {
                case Keys.PageDown:
                    MovePalletDown();
                    e.Handled = true;
                    break;

                case Keys.PageUp:
                    MovePalletUp();
                    e.Handled = true;
                    break;

                case Keys.Up:
                    if (clsFormInputRtns.bolEditMode)
                    {
                        if (objSelectedControl != null)
                        {
                            if (e.Alt)
                            {
                                // make it smaller
                                objSelectedControl.Height = objSelectedControl.Height - 1;
                            }
                            else
                            {
                                // move it up
                                objSelectedControl.Top = objSelectedControl.Top - 1;
                            }
                            e.Handled = true;
                        }
                    }
                    break;


                case Keys.Down:
                    if (clsFormInputRtns.bolEditMode)
                    {
                        if (objSelectedControl != null)
                        {
                            if (e.Alt)
                            {
                                // make it taller
                                objSelectedControl.Height = objSelectedControl.Height + 1;
                            }
                            else
                            {
                                // if a control has focus, then move it down
                                objSelectedControl.Top = objSelectedControl.Top + 1;
                            }
                            e.Handled = true;
                        }
                    }
                    break;

                case Keys.Left:
                    if (clsFormInputRtns.bolEditMode)
                    {
                        if (objSelectedControl != null)
                        {
                            if (e.Alt)
                            {
                                // make it wider
                                objSelectedControl.Width = objSelectedControl.Width - 1;
                            }
                            else
                            {
                                // move it left
                                objSelectedControl.Left = objSelectedControl.Left - 1;
                            }
                            e.Handled = true;
                        }
                    }
                    break;

                case Keys.Right:
                    if (clsFormInputRtns.bolEditMode)
                    {
                        if (objSelectedControl != null)
                        {
                            if (e.Alt)
                            {
                                // make it narrower
                                objSelectedControl.Width = objSelectedControl.Width + 1;
                            }
                            else
                            {
                                // move it right
                                objSelectedControl.Left = objSelectedControl.Left + 1;
                            }
                            e.Handled = true;
                        }
                    }
                    break;

            }

        }


        private void MovePalletDown()
        {

            // move the total page up (to scroll down)
            objSelectedPallet.Top = objSelectedPallet.Top - 20;

        }


        private void MovePalletUp()
        {

            // move the total page down (to scroll up)
            objSelectedPallet.Top = objSelectedPallet.Top + 20;
            if (objSelectedPallet.Top > 0)
            {
                objSelectedPallet.Top = 0;
            }

        }


        private void MovePalletLeft()
        {

        }


        private void MovePalletRight()
        {

        }

        void RightClickMenuItems_Click(object sender, EventArgs e)
        {

            ToolStripItem objItem = (ToolStripItem)sender;

            string strItemName = objItem.Name.ToUpper().Replace("CMNURIGHTCLICKADDFIELD", "");
            //MessageBox.Show("Item clicked=" + strItemName);

            Control objControl = CreateControl(strItemName);

            // insert the control at the point clicked
            if (objControl != null)
            {
                InsertControl(objControl);

            }

        }


        void InsertControl(Control argobjControl)
        {
            Control objControl = argobjControl;

            objControl.Left = intPicClickX;
            objControl.Top = intPicClickY;
            //objControl.Width = objSelectedToolboxControl.Width;
            //objControl.Height = objSelectedToolboxControl.Height;
            objControl.Name = "Field_1" + objSelectedPallet.Controls.Count + 1;
            objControl.BackColor = objSelectedColor;
            objControl.Parent = objSelectedPallet;
            objSelectedPallet.Controls.Add(objControl);
            objControl.Visible = true;
            objControl.Click += new EventHandler(objControl_Click);
            objControl.MouseDown += new MouseEventHandler(objControl_MouseDown);
            objControl.MouseMove += new MouseEventHandler(objControl_MouseMove);
            objControl.MouseUp += new MouseEventHandler(objControl_MouseUp);
            objControl.KeyDown += new KeyEventHandler(PlexForms_KeyDown);
            objControl.Enter += new EventHandler(objControl_Enter);
            objControl.Leave += new EventHandler(objControl_Leave);
            objControl.DoubleClick += new EventHandler(objControl_DoubleClick);
            SetFocus(objControl);

        }


        void objControl_Click(object sender, EventArgs e)
        {
            Control objControl = (Control)sender;

            bolControlMouseClick = true;

            SetFocus(objControl);

        }




        void objControl_Leave(object sender, EventArgs e)
        {
            Control objControl = (Control)sender;

            if (clsFormInputRtns.bolEditMode)
            {
                // save the rules
                clsFormInputRtns.SetFieldRules(objSelectedTabPage.Text, objSelectedControl.Name, "", txtRules.Text);
            }
            else
            {
                //// process the lostfocus rules
                clsProcessRules.ProcessRules(tabForms, objSelectedTabPage.Name, objControl, "LOSTFOCUS");
            }

        }



        void objControl_Enter(object sender, EventArgs e)
        {
            Control objControl = (Control)sender;

            if (!bolControlMouseClick)
            {
                SetFocus(objControl);
            }

            bolControlMouseClick = false;

        }

        void objControl_DoubleClick(object sender, EventArgs e)
        {
            // open properties form
            ControlProperties(objSelectedControl);
        }



        void objControl_MouseUp(object sender, MouseEventArgs e)
        {

            if (clsFormInputRtns.bolEditMode)
            {
                Control objControl = (Control)sender;

                if (objControl.GetType().Name.ToUpper() == "CHECKBOX")
                {
                    return;
                }

                // drop the control at this point
                if (bolControlMouseDown)
                {
                    objControl.Left = intPicClickX;
                    objControl.Top = intPicClickY;

                }
            }

            bolControlMouseDown = false;

        }


        void objControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (clsFormInputRtns.bolEditMode)
            {
                // move the control
                if (bolControlMouseDown)
                {
                    intPicClickX = e.X;
                    intPicClickY = e.Y;
                    clsCommonRtns.Display("Moving to " + intPicClickX + ":" + intPicClickY);
                }
            }

        }




        void objControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (clsFormInputRtns.bolEditMode)
            {
                Control objControl = (Control)sender;

                // if a right click then show the field properties
                if (e.Button == MouseButtons.Right)
                {
                    // show the menu
                    // adjust the x and y to take into account the controls location
                    int X = tabForms.Left + objSelectedPallet.Left + objControl.Left + e.X;
                    int Y = tabForms.Top + objSelectedPallet.Top + objControl.Top + e.Y;
                    cmnuRightClick.Show(this, X, Y);  // (this, new Point(X, Y));
                    // disable the add
                    cmnuRightClickAddField.Enabled = false;
                }
            }

        }



        void ControlProperties(Control argobjControl)
        {
            if (clsFormInputRtns.bolEditMode)
            {
                FieldProperties objFieldProperties = new FieldProperties();

                objFieldProperties.SelectedControl = argobjControl;
                objFieldProperties.SelectedForm = objSelectedTabPage;

                objFieldProperties.ShowDialog(this);
            }

            // reshow the attributes
            ShowAttributes(argobjControl);

        }





        private Control CreateControl(string argstrControl)
        {
            Control objControl = null;

            switch (argstrControl.ToUpper())
            {
                case "TEXTBOX":
                    objControl = new Panel();
                    // set the height and width
                    objControl.Height = 24;
                    objControl.Width = 100;

                    //objControl = new TextBox();
                    //TextBox objTextBox = (TextBox)objControl;
                    //objTextBox.Multiline = true;
                    //objTextBox.ReadOnly = true;
                    break;

                case "COMBOBOX":
                    objControl = new ComboBox();
                    break;

                case "BUTTON":
                    objControl = new Button();
                    break;

                case "PANEL":
                    objControl = new Panel();
                    break;

                case "GRID":
                    objControl = new DataGridView();
                    break;

                case "LABEL":
                    objControl = new Label();
                    break;

                case "CHECKBOX":
                    objControl = new CheckBox();
                    break;

                case "OPTION":
                    objControl = new RadioButton();
                    break;

                default:
                    break;

            }

            if (objControl != null)
            {
                objControl.MouseDown += new MouseEventHandler(objControl_MouseDown);
                objControl.MouseMove += new MouseEventHandler(objControl_MouseMove);
                objControl.MouseUp += new MouseEventHandler(objControl_MouseUp);
                objControl.KeyDown += new KeyEventHandler(PlexForms_KeyDown);
                objControl.DoubleClick += new EventHandler(objControl_DoubleClick);
            }

            return objControl;

        }



        void PlexForms_Resize(object sender, EventArgs e)
        {

            ResizeControls();

        }



        /// <summary>
        /// mnuFileExit - close the app
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            clsFormInputRtns.bolEditMode = false;

            // close the app
            Close();

        }



        /// <summary>
        /// mnuAbout - about dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuHelpAbout_Click(object sender, EventArgs e)
        {

            About frmAbout = new About();

            frmAbout.Show(this);

        }



        /// <summary>
        /// ResizeControls - resize and place the controls on the form
        /// </summary>
        private void ResizeControls()
        {

            // resize the controls panel
            pnlControls.Top = mnuPlexForms.Height + 2;

            // resize the attributes grid
            grdAttributes.Height = pnlControls.Height - grdAttributes.Top - 100;
            grdAttributes.Left = lblToolBox.Left;
            grdAttributes.Width = lblToolBox.Width;

            // place and resize the status panel
            pnlStatus.Top = this.ClientRectangle.Height - pnlStatus.Height - 10;
            pnlStatus.Left = 0;
            pnlStatus.Width = this.ClientRectangle.Width - (2 * pnlStatus.Left);

            // place and resize the tabforms
            tabForms.Top = mnuPlexForms.Height + tbrPlexForms.Height + 2;
            tabForms.Left = 0;  // tbrPlexForms.Width + 3;
            tabForms.Width = this.ClientRectangle.Width - tabForms.Left;
            if (clsFormInputRtns.bolEditMode)
            {
                tabForms.Width = tabForms.Width - pnlControls.Width;
            }
            tabForms.Height = this.ClientRectangle.Height - tbrPlexForms.Height - tabForms.Top - pnlStatus.Height;

            // resize the tabpages
            for (int n = 0; n < tabForms.TabPages.Count; n++)
            {
                tabForms.TabPages[n].Width = tabForms.Width - (2 * tabForms.TabPages[n].Left);
                tabForms.TabPages[n].Height = tabForms.Height - tabForms.TabPages[n].Top - 20;
            }

            // resize and place the status bar components
            lblStatus.Left = 0;
            picPoweredByPlexsoft.Left = pnlStatus.Width - picPoweredByPlexsoft.Width - 1;
            lblClock.Left = pnlStatus.Width - lblClock.Width - picPoweredByPlexsoft.Width - 2;
            lblCoordinates.Left = pnlStatus.Width - lblClock.Width - lblCoordinates.Width - picPoweredByPlexsoft.Width - 2;
            lblStatus.Width = pnlStatus.Width - lblStatus.Left - lblClock.Width - lblCoordinates.Width - picPoweredByPlexsoft.Width - 5;

            // put the label in the center
            txtLogo.Width = (this.Width / 2);
            txtLogo.Height = (this.Height / 2);
            txtLogo.Left = (this.Width - txtLogo.Width) / 2;
            txtLogo.Top = (this.Height - txtLogo.Height) / 2;

            // resize the control panel
            tabAttributes.Height = pnlControls.Height - tabAttributes.Top - 30;
            tabAttributes.Width = pnlControls.Width - (2 * tabAttributes.Left);
            grdAttributes.Width = tabPageAttributes.Width - (2 * grdAttributes.Left);
            grdAttributes.Height = tabPageAttributes.Height - grdAttributes.Top - 10;
            txtRules.Left = grdAttributes.Left;
            txtRules.Width = grdAttributes.Width;
            txtRules.Height = grdAttributes.Height;

            // if no forms are displayed..
            if (!tabForms.Visible)
            {
                // load the logo picture
                if (clsCommonRtns.GetSetting("LogoPicture") != "")
                {
                    // load the image
                    string strPicLogo = clsCommonRtns.GetSetting("LogoPicture");
                    if (strPicLogo.IndexOf('\\') < 0)
                    {
                        // no path
                        strPicLogo = clsCommonRtns.strCurrentPath + "\\" + strPicLogo;
                    }

                    if (System.IO.File.Exists(strPicLogo))
                    {
                        picLogo.Width = 400;
                        picLogo.Height = 400;
                        picLogo.SizeMode = PictureBoxSizeMode.StretchImage;
                        picLogo.ImageLocation = strPicLogo;
                        picLogo.Refresh();
                        picLogo.Left = (this.Width - picLogo.Width) / 2;
                        picLogo.Top = (this.Height - picLogo.Height) / 2;
                        picLogo.Visible = true;
                        txtLogo.Visible = false;
                    }
                    else
                    {
                        picLogo.Visible = false;
                        txtLogo.Visible = true;
                    }
                }
                else
                {
                    picLogo.Visible = false;
                    txtLogo.Visible = true;
                }
            }

        }



        #region [ Toolbar Routines ]








        /// <summary>
        /// tbrPlexFormsOpen_Click - open an existing form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbrPlexFormsOpen_Click(object sender, EventArgs e)
        {
            // open an existing form
            OpenExistingForm("");

        }



        #endregion 


        #region [ form routines ]



        /// <summary>
        /// GetFormInfo - get the form info
        /// </summary>
        private void GetFormInfo(string argstrTypeOfForm)
        {
            string strTypeOfForm = argstrTypeOfForm.ToUpper();
            int intNumberForms = tabForms.TabCount;
            PictureBox objPicture = new PictureBox();
            string strImageFileName = "";
            string strFormGroup = "";
            string strFormName = "";
            int intTabIndex = 0;

            if (tabForms.Tag == null)
            {
                tabForms.Tag = "";
            }

            // get the form information
            NewForm objNewForm = new NewForm();

            objNewForm.FormGroup = tabForms.Tag.ToString();
            objNewForm.FormImageName = "";

            switch (strTypeOfForm)
            {
                case "NEW":
                    strImageFileName = "";
                    strFormName = "";
                    break;

                case "CHANGE":
                    strFormName = tabForms.SelectedTab.Text;
                    objPicture = (PictureBox)tabForms.SelectedTab.Controls[0];
                    strImageFileName = objPicture.ImageLocation;
                    break;

                case "ADD":
                    strImageFileName = "";
                    strFormName = "";
                    intTabIndex = tabForms.TabCount;
                    break;

            }

            objNewForm.FormName = strFormName;
            objNewForm.FormImageName = strImageFileName;

            objNewForm.ShowDialog(this);

            // first get the file name for the image
            strImageFileName = objNewForm.FormImageName;
            strFormGroup = objNewForm.FormGroup;
            strFormName = objNewForm.FormName;

            if (strImageFileName.Length < 1)
            {
                // none specified
                return;
            }

            txtLogo.Visible = false;
            pnlControls.Visible = true;
            tabForms.Visible = true;

            switch (strTypeOfForm)
            {
                case "NEW":
                    // init the tab
                    tabForms.TabPages.Clear();
                    tabForms.Tag = strFormGroup;
                    break;

                case "CHANGE":
                    // change the values
                    tabForms.SelectedTab.Text = strFormName;
                    tabForms.Tag = strFormGroup;
                    objPicture = (PictureBox)tabForms.SelectedTab.Controls[0];
                    if (strImageFileName != objPicture.ImageLocation)
                    {
                        objPicture.ImageLocation = strImageFileName;
                        objPicture.Load(strImageFileName);
                    }
                    break;

                case "ADD":
                    break;

            }

            tabForms.Tag = strFormGroup;

            if (strTypeOfForm == "CHANGE")
            {
                return;
            }

            intTabIndex = tabForms.TabCount;

            // set the image
            objPicture.ImageLocation = strImageFileName;

            // see if the tab is empty 
            TabPage objNewPage = new TabPage();
            if (tabForms.TabPages.Count == 1)
            {
                // is it empty?
                if (tabForms.TabPages[0].Controls.Count == 0)
                {
                    // use this tab
                    objNewPage = tabForms.TabPages[0];
                    objNewPage.Text = strFormName;
                }
                else
                {
                    // add a tab
                    tabForms.TabPages.Add(strFormName);
                    objNewPage = tabForms.TabPages[tabForms.TabPages.Count - 1];
                }
            }
            else
            {
                // add a tab
                tabForms.TabPages.Add(strFormName);
                objNewPage = tabForms.TabPages[tabForms.TabPages.Count - 1];
            }

            // add the picture box to the new tab
            objPicture.Parent = objNewPage;

            // resize it
            objPicture.Left = 0;
            objPicture.Top = 10;

            // determine the relative size from the current pallet...
            //objPictureSelected.Height = Convert.ToInt32(objPictureSelected.Height * 1.10);
            //objPictureSelected.Width = Convert.ToInt32(objPictureSelected.Width * 1.10);
            // if the tabpages width is greater than the max width, reset
            int intFactorWidth = objNewPage.Width;
            if (intFactorWidth > intMaxWidth)
            {
                intFactorWidth = intMaxWidth;
            }
            double dblFactor = Convert.ToDouble(intFactorWidth) / Convert.ToDouble(intPicFormWidth);
            objPicture.Height = Convert.ToInt32(intPicFormHeight * dblFactor);
            objPicture.Width = Convert.ToInt32(intPicFormWidth * dblFactor);
            objPicture.Load(strImageFileName);
            objPicture.SizeMode = PictureBoxSizeMode.StretchImage;

            objPicture.Refresh();

            objSelectedPallet = objPicture;

            objPicture.Click += new EventHandler(objPicture_Click);
            objPicture.MouseDown += new MouseEventHandler(objPicture_MouseDown);
            objPicture.MouseMove += new MouseEventHandler(objPicture_MouseMove);
            objPicture.MouseUp += new MouseEventHandler(objPicture_MouseUp);
            objPicture.Visible = true;

            clsCommonRtns.Display("");

            objNewPage.AutoScroll = false;
            objNewForm.Select();

            tabForms.SelectTab(intTabIndex);

            objSelectedTabPage = objNewPage;

            return;

        }





        void objPicture_MouseMove(object sender, MouseEventArgs e)
        {
            //// if the mouse is down for this time then move the picture up and dow
            //if (bolPictureMouseDown)
            //{
            //    // move the picture up and down
            //    if (e.Y > intPicClickY)
            //    {
            //        // moving the mouse up...
            //        MovePalletDown();
            //    }
            //    else
            //    {
            //        // moving the mouse down
            //        MovePalletUp();
            //    }

            //}

            // show where the mouse is...
            intPicClickX = e.X;
            intPicClickY = e.Y;

            lblCoordinates.Text = "X:" + intPicClickX + ", Y:" + intPicClickY;
   
        }




        void objPicture_MouseDown(object sender, MouseEventArgs e)
        {

            // set the selected pallet
            objSelectedPallet = (PictureBox)sender;

            intPicClickX = e.X;
            intPicClickY = e.Y;

            int X = tabForms.Left + objSelectedPallet.Left + e.X;
            int Y = tabForms.Top + objSelectedPallet.Top + e.Y;

            // if a right click then show the menu
            if (e.Button == MouseButtons.Right)
            {
                cmnuRightClick.Show(this, X, Y);
                // disable the add
                cmnuRightClickAddField.Enabled = true;
            }
            else
            {
                // if a move operation is in play...
                if (bolMoveControl)
                {
                    if (objSelectedControl != null)
                    {
                        // position the middle of the control here...
                        objSelectedControl.Left = intPicClickX;  // -(objSelectedControl.Width / 2);
                        objSelectedControl.Top = intPicClickY; // -(objSelectedControl.Height / 2);
                    }
                    bolMoveControl = false;
                }
                else
                {
                    // push the down flag
                    bolPictureMouseDown = true;
                }
            }


        }



        void objPicture_MouseUp(object sender, MouseEventArgs e)
        {
            // push the up flag
            bolPictureMouseDown = false;
        }



        /// <summary>
        /// OpenExistingForm - open an existing form
        /// </summary>
        private void OpenExistingForm(string argstrFormGroupName)
        {
            string strFormGroupName = argstrFormGroupName;
            int intRC = 0;

            if (clsCommonRtns.strLastFormPath.Length > 0)
            {
                clsCommonRtns.strLastPath = clsCommonRtns.strLastFormPath;
            }

            if (bolRedisplayForm)
            {
                intRC = clsFormInputRtns.LoadFromFile("", tabForms);

            }
            else
            {
                if (strFormGroupName == "")
                {
                    // first get the file name for the image
                    //strFormFileName = clsCommonRtns.GetFile("frm", "","O");
                    tmrClock.Enabled = false;
                    OpenForms objOpenForms = new OpenForms();
                    objOpenForms.ShowDialog(this);
                    strFormGroupName = objOpenForms.strFormSelected;
                    objOpenForms.Close();
                    tmrClock.Enabled = true;
                }

                clsCommonRtns.strLastFormPath = clsCommonRtns.strLastPath;

                if (strFormGroupName.Length < 1)
                {
                    // none specified
                    return;
                }

                // see if it is a file or a database group name
                switch (clsFormInputRtns.strFormSource.ToUpper())
                {
                    case "F":
                        // open from file
                        intRC = clsFormInputRtns.LoadFromFile(strFormGroupName, tabForms);
                        break;

                    case "D":
                        // open from database
                        intRC = clsFormInputRtns.LoadFromDatabase(strFormGroupName, tabForms);
                        break;

                    default:
                        intRC = -1;
                        break;

                }

                // did it work...
                if (intRC < 0)
                {
                    return;
                }
            }

            LoadForm();

            clsFormInputRtns.bolFormLoaded = true;

            // run the load event for the form
            clsProcessRules.ProcessRules(tabForms, tabForms.Tag.ToString(), null, "LOAD");

            bolRedisplayForm = false;

            picLogo.Visible = false;

        }




        private void LoadForm()
        {
            PictureBox objPallet = null;

            txtLogo.Visible = false;
            picLogo.Visible = false;
            pnlControls.Visible = clsFormInputRtns.bolEditMode;
            tabForms.Visible = true;

            // if initial load then run, otherwise bypass
            // set the event for the tabforms
            tabForms.MouseDown += new MouseEventHandler(tabForms_MouseDown);

            // now associate all the controls on each pallet to the event handler
            foreach (TabPage objPage in tabForms.TabPages)
            {
                objPallet = (PictureBox)objPage.Controls[0];

                objPallet.Click += new EventHandler(objPicture_Click);
                objPallet.MouseDown += new MouseEventHandler(objPicture_MouseDown);
                objPallet.MouseMove += new MouseEventHandler(objPicture_MouseMove);
                objPallet.Visible = true;
                objSelectedPallet = objPallet;

                // now for each control on the pallet
                foreach (Control objControl in objPallet.Controls)
                {
                    objControl.Click += new EventHandler(objControl_Click);
                    objControl.MouseDown += new MouseEventHandler(objControl_MouseDown);
                    objControl.MouseMove += new MouseEventHandler(objControl_MouseMove);
                    objControl.MouseUp += new MouseEventHandler(objControl_MouseUp);
                    objControl.KeyDown += new KeyEventHandler(PlexForms_KeyDown);
                    objControl.Enter += new EventHandler(objControl_Enter);
                    objControl.Leave += new EventHandler(objControl_Leave);
                    objControl.DoubleClick += new EventHandler(objControl_DoubleClick);

                }
            }

            // now set the current form
            objSelectedTabPage = tabForms.TabPages[0];
            objSelectedPallet = (PictureBox) objSelectedTabPage.Controls[0];

            // enable the menu and toolbars for the appropriate mode
            if (clsFormInputRtns.bolEditMode)
            {
                SetMenusAndToolbars("EDIT");
            }
            else
            {
                if (bolStandardMode)
                {
                    SetMenusAndToolbars("STANDARD");
                }
                else
                {
                    SetMenusAndToolbars("RUN");
                }
            }

            if (tabForms.TabPages.Count > 0)
            {
                // display the pallet size in the toolbar
                objPallet = (PictureBox)tabForms.TabPages[0].Controls[0];
                lblPalletSize.Text = "Pallet:" + objPallet.Width.ToString() + "W:" + objPallet.Height + "H";
            }

        }




        void ProcessControlFocus(Control argobjControl)
        {

            if (!clsFormInputRtns.bolEditMode)
            {
                // turn off the timer
                tmrClock.Enabled = false;

                // process getfocus rules. send the whole tabForms, the current tab and the current control
                clsProcessRules.ProcessRules(tabForms, objSelectedTabPage.Name, argobjControl, "GETFOCUS");

                tmrClock.Enabled = true;
            }

        }





        void tabForms_MouseDown(object sender, MouseEventArgs e)
        {

            objSelectedControl = tabForms;

            // if a right click then show the field properties
            if (e.Button == MouseButtons.Right)
            {
                // show the menu
                // adjust the x and y to take into account the controls location
                int X = e.X;
                int Y = tabForms.Top + e.Y;
                cmnuRightClick.Show(this, X, Y);  // (this, new Point(X, Y));
                // disable the add
                cmnuRightClickAddField.Enabled = false;
            }

        }



        #endregion 


        #region [ Menu Routines]

        private void mnuFileNewField_Click(object sender, EventArgs e)
        {
            // set the flag that will put a textbox where they click next
            bolSetControlAtClick = true;

        }

        #endregion 



        /// <summary>
        /// tbrPlexFormsSave_Click - save the current form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbrPlexFormsSave_Click(object sender, EventArgs e)
        {

            // save the form 
            clsFormInputRtns.SaveToXML(tabForms, "");

            if (clsFormInputRtns.bolEditMode)
            {
                LoadForm();
                ResizeControls();
            }

        }


        /// <summary>
        /// tbrOpenExistingForm_Click - open an existing form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbrOpenExistingForm_Click(object sender, EventArgs e)
        {

            // open an existing form
            OpenExistingForm("");


        }





        private void tbrPlexFormsNew_Click(object sender, EventArgs e)
        {

            // create a new form with a blank pallet
            GetFormInfo("New");

        }



        private void tbrPlexFormsAdd_Click(object sender, EventArgs e)
        {

            GetFormInfo("Add");

        }



        private void mnuFileClose_Click(object sender, EventArgs e)
        {

            if (clsFormInputRtns.bolEditMode)
            {
                mnuToolsCloseEdit_Click(sender, e);
            }
            else
            {
                CloseForm();
            }

        }


        private void CloseForm()
        {

            // close the open form
            // check if anything has been done...  code for later...

            // init the tab
            tabForms.TabPages.Clear();

            tabForms.Visible = false;

            pnlControls.Visible = false;

            txtLogo.Visible = true;

            clsFormInputRtns.bolEditMode = false;

            clsFormInputRtns.bolFormLoaded = false;

            clsFormInputRtns.strFormGroupName = "";

        }




        private void cmnuRightClick_Opening(object sender, CancelEventArgs e)
        {

        }

        private void cmnuRightClickMoveField_Click(object sender, EventArgs e)
        {

            bolMoveControl = true;

        }


        void objPicture_Click(object sender, EventArgs e)
        {

            SetFocus(null);

            // if the add control at click is on...
            if (bolSetControlAtClick)
            {
                // put the last selected add where the mouse is..  if none then put a textbox
                if (strToolboxControlSelected != "") 
                {
                    // place the control
                    Control objControl = CreateControl(strToolboxControlSelected);

                    // insert the control at the point clicked
                    if (objControl != null)
                    {
                        InsertControl(objControl);
                    }

                }

                if (!chkOneClick.Checked)
                {
                    strToolboxControlSelected = "";
                    bolSetControlAtClick = false;
                }
            }

            objSelectedControl = null;

        }



        private void cmnuRightClickProperties_Click(object sender, EventArgs e)
        {

            ControlProperties(objSelectedControl);
        }




        private void tmrClock_Tick(object sender, EventArgs e)
        {

            // update the lblclock
            lblClock.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

        }


        private void mnuFileImport_Click(object sender, EventArgs e)
        {

            // first get the file name for the form to convert
            string strFormFileName = clsCommonRtns.GetFile("", "","O");

            if (strFormFileName.Length < 1)
            {
                // none specified
                return;
            }

            // import from previous forms
            strFormFileName = clsFormInputRtns.ConvertDelimitedToXML(strFormFileName);

            // now open up the form
            OpenExistingForm(strFormFileName);

        }




        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {

            // first get the file name for the new form
            string strFormFileName = clsCommonRtns.GetFile("frm", "","S");

            if (strFormFileName.Length < 1)
            {
                // none specified
                return;
            }

            // save the form 
            clsFormInputRtns.SaveToXML(tabForms, strFormFileName);

        }



        private void mnuFileSave_Click(object sender, EventArgs e)
        {

            tbrPlexFormsSave_Click(sender, e);

        }


        
        private void tbrPlexFormsClose_Click(object sender, EventArgs e)
        {

            mnuFileClose_Click(sender, e);

        }



        /// <summary>
        /// tbrPlexFormsExit_Click - exit the app
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbrPlexFormsExit_Click(object sender, EventArgs e)
        {

            mnuFileExit_Click(sender, e);

        }


        /// <summary>
        /// cmnuRightClickDeleteField_Click - delete the current control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmnuRightClickDeleteField_Click(object sender, EventArgs e)
        {

            if (objSelectedControl != null)
            {
                if (objSelectedControl == tabForms)
                {
                    tabForms.TabPages.Remove(tabForms.SelectedTab);

                }
                else
                {
                    objSelectedPallet.Controls.Remove(objSelectedControl);
                }
            }

        }




        private void cmnuRightClickCopyField_Click(object sender, EventArgs e)
        {

            CopyField();

        }


        private void CopyField()
        {
            // set the copy control object
            objCopyControl = objSelectedControl;
            cmnuRightClickPasteField.Enabled = true;

        }




        private void cmnuRightClickPasteField_Click(object sender, EventArgs e)
        {

            PasteField();

        }


        private void PasteField()
        {

            if (objCopyControl != null)
            {
                // put it at the position 
                Control objNewControl = CloneControl(objCopyControl);

                SetFocus(null);

                InsertControl(objNewControl);

//                objNewControl.Left = intPicClickX;  // -(objNewControl.Width / 2);
//                objNewControl.Top = intPicClickY - (objNewControl.Height / 2);
////                cmnuRightClickPasteField.Enabled = false;
//                // reset the selected control

//                objSelectedPallet.Controls.Add(objNewControl);
////                objCopyControl = null;
//                // switch focus...
//                SetFocus(objNewControl);

            }

        }


        private Control CloneControl(Control argobjControl)
        {
            Control objNewControl = new Control();
            string strControlType = argobjControl.GetType().Name.ToUpper();

            objNewControl = CreateControl(strControlType);

            if (objNewControl != null)
            {
                // now add to the saved control
                objNewControl.Name = argobjControl.Name + "_" + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                objNewControl.Text = argobjControl.Text;
                objNewControl.Height = argobjControl.Height;
                objNewControl.Width = argobjControl.Width;
                objNewControl.Top = argobjControl.Top;
                objNewControl.Left = argobjControl.Left;
                if (strControlType == "TEXTBOX")
                {
                    objNewControl.BackColor = objNonselectedColor;
                }
                else
                {
                    objNewControl.BackColor = Color.White;
                }
                objNewControl.Font = argobjControl.Font;
                objNewControl.Visible = true;

            }

            return objNewControl;
        }




        private void SetFocus(Control objControl)
        {

            // defect 4 - reset all controls before setting focus backcolor
            // reset the color on the current tabpage
            foreach (Control objCtl in objSelectedPallet.Controls)
            {
                if (objCtl.GetType().Name.ToUpper() == "BUTTON")
                {
                    objCtl.BackColor = clrButtonColor;
                }
                else
                {
                    objCtl.BackColor = objNonselectedColor;
                }
                objSelectedControl = null;
            }

            // make sure all other events are fired...
            if (objControl != null)
            {
                objSelectedControl = objControl;
                if (clsFormInputRtns.bolEditMode)
                {
                    // turn off a checkbox
                    if (objSelectedControl.GetType().Name.ToUpper() == "CHECKBOX")
                    {
                        CheckBox objCheckBox = (CheckBox)objSelectedControl;
                        objCheckBox.Checked = false;
                    }
                }

                objSelectedControl.BackColor = objSelectedColor;

                // show the properties
                ShowAttributes(objSelectedControl);

                // process any focus logic
                ProcessControlFocus(objControl);

            }
            else
            {
                ShowAttributes(null);
            }

        }



        private void ShowAttributes(Control objControl)
        {
            int intRow = 0;

            if (clsFormInputRtns.bolEditMode)
            {
                string strList = ""; 

                // put in the rules

                // put in the attributes
                grdAttributes.Rows.Clear();
                grdAttributes.Rows.Add(15);
                grdAttributes.Columns["AttributeValue"].Width = 500;

                if (objControl != null)
                {
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Form";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objSelectedTabPage.Name;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Name";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Name;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Text";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Text;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Type";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.GetType().Name;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Top";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Top;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Left";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Left;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Width";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Width;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Height";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.Height;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Tabindex";
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objControl.TabIndex;

                    // if it is a grid, show the active cell info and the column header
                    string strType = objControl.GetType().Name.ToUpper();

                    switch (strType)
                    {
                        case "DATAGRIDVIEW":
                            DataGridView objGrid = (DataGridView)objControl;

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Location";
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = objGrid.CurrentCell.RowIndex.ToString() + ":" +
                                        objGrid.CurrentCell.ColumnIndex.ToString();

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Col_Name";
                            string strColumnName = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Name;
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnName;

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Col_Width";
                            string strColumnWidth = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Width.ToString();
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnWidth;

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Col_HeaderText";
                            string strColumnHeaderText = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].HeaderText;
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnHeaderText;

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Col_HeaderHeight";
                            string strColumnHeaderHeight = objGrid.ColumnHeadersHeight.ToString();
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnHeaderHeight;

                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Col_Value";
                            if (objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value != null)
                            {
                                string strColumnValue = objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value.ToString();
                                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnValue;
                            }

                            break;

                        case "COMBOBOX":
                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "List";
                            strList = "";
                            ComboBox objComboBox = (ComboBox)objControl;
                            for (int n = 0;n < objComboBox.Items.Count; n++)
                            {
                                strList = strList + "0;" + objComboBox.Items[n].ToString();
                            }
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strList;
                            break;

                        case "LISTBOX":
                            intRow++;
                            grdAttributes.Rows[intRow].Cells["Attribute"].Value = "List";
                            strList = "";
                            ListBox objListBox = (ListBox)objControl;
                            for (int n = 0; n < objListBox.Items.Count; n++)
                            {
                                strList = strList + "0;" + objListBox.Items[n].ToString();
                            }
                            grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strList;
                            break;

                    }

                }


                // get any rules for this control
                List<string> lstRules = new List<string>();
                if (objSelectedControl != null)
                {
                    lstRules = clsFormInputRtns.GetFieldRules(objSelectedTabPage.Text, objSelectedControl.Name, "");
                }

                // if any rules are in there for this control, then load them
                txtRules.Text = "";
                for (int m = 0; m < lstRules.Count; m++)
                {
                    txtRules.Text = txtRules.Text + lstRules[m].ToString() + "\r\n";
                }

                // set to the attributes tab
                tabAttributes.SelectTab(0);


            }

        }




        private void mnuToolsOptions_Click(object sender, EventArgs e)
        {

            Configuration objConfiguration = new Configuration();

            objConfiguration.ShowDialog();

            // reset the options
            LoadConfigSettings();

            // refresh the controls
            ResizeControls();

        }







        private void SetMenusAndToolbars(string argstrState)
        {
            string strState = "";

            if (argstrState == "")
            {
                if (clsFormInputRtns.bolEditMode)
                {
                    strState = "EDIT";
                }
                else
                {
                    strState = "RUN";
                }
            }
            else
            {
                strState = argstrState.ToUpper();
            }

            // check to see if the enable designor flag is no
            if (clsFormInputRtns.bolEnableDesigner)
            {
                // turn off the edit capability
                strState = "STANDARD";
            }

            // turn off the toolbar...
            for (int n=0;n<tbrPlexForms.Items.Count;n++)
            {
                tbrPlexForms.Items[n].Visible = false;
            }

            // turn on the menus
            ToggleMenu(true, null);

            // turn on the toolbar buttons
            ToggleToolbar(true, null);

            // turn off the edit buttons
            ToggleMenu(false, "EDIT");
            ToggleToolbar(false, "EDIT");

            switch (argstrState.ToUpper())
            {
                case "INITIAL":
                    break;

                case "EDIT":
                    // turn on the edit buttons
                    ToggleMenu(true, "EDIT");
                    ToggleToolbar(true, "EDIT");
                    break;

                case "RUN":
                    break;

                case "STANDARD":
                    break;

            }

        }



        private void ToggleMenu(bool argbolOnOff, string argstrTag)
        {

            try
            {
                for (int n = 0; n < mnuPlexForms.Items.Count; n++)
                {
                    // if a tag is specified, check to see if it is to be set
                    if (argstrTag != null)
                    {
                        if (mnuPlexForms.Items[n].Tag != null)
                        {
                            if (mnuPlexForms.Items[n].Tag.ToString().ToUpper().Equals(argstrTag.ToUpper()))
                            {
                                mnuPlexForms.Items[n].Enabled = argbolOnOff;
                            }
                        }
                    }
                    else
                    {
                        mnuPlexForms.Items[n].Enabled = argbolOnOff;
                    }

                    if (mnuPlexForms.Items[n].GetType().Name.ToUpper() == "TOOLSTRIPMENUITEM")
                    {
                        ToolStripMenuItem objMenuItem = (ToolStripMenuItem)mnuPlexForms.Items[n];
                        // for each of their items, disable them
                        for (int m = 0; m < objMenuItem.DropDownItems.Count; m++)
                        {
                            if (argstrTag != null)
                            {
                                if (objMenuItem.DropDownItems[m].Tag != null)
                                {
                                    if (objMenuItem.DropDownItems[m].Tag.ToString().ToUpper().Equals(argstrTag.ToUpper()))
                                    {
                                        objMenuItem.DropDownItems[m].Enabled = argbolOnOff;
                                    }
                                }
                            }
                            else
                            {
                                objMenuItem.DropDownItems[m].Enabled = argbolOnOff;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // an error
                MessageBox.Show("Error in ToggleMenu, err=" + ex.Message);
            }


        }



        private void ToggleToolbar(bool argbolOnOff, string argstrTag)
        {

            try
            {
                for (int n = 0; n < tbrPlexForms.Items.Count;n++)
                {
                    if (argstrTag != null)
                    {
                        if (tbrPlexForms.Items[n].Tag != null)
                        {
                            if (tbrPlexForms.Items[n].Tag.ToString().ToUpper().Equals(argstrTag.ToUpper()))
                            {
                                //tbrPlexForms.Items[n].Enabled = argbolOnOff;
                                tbrPlexForms.Items[n].Visible = argbolOnOff;
                            }
                        }
                    }
                    else
                    {
                        //tbrPlexForms.Items[n].Enabled = argbolOnOff;
                        tbrPlexForms.Items[n].Visible = argbolOnOff;
                    }
                }
            }
            catch (Exception ex)
            {
                // an error
                MessageBox.Show("Error in ToggleToolbar, err=" + ex.Message);
            }


        }


        private void mnuFileOpen_Click(object sender, EventArgs e)
        {

            // open an existing form
            OpenExistingForm("");

        }




        private void mnuFileNewForm_Click(object sender, EventArgs e)
        {

            // create a new form
            GetFormInfo("New");

            // put in edit mode
            mnuToolsEditForm_Click(sender, e);

        }

        private void tbrScrollDown_Click(object sender, EventArgs e)
        {

            MovePalletDown();
        }




        private void tbrScrollUp_Click(object sender, EventArgs e)
        {

            MovePalletUp();

        }



        private void mnuToolsEditForm_Click(object sender, EventArgs e)
        {

            if (tabForms.TabPages.Count > 0)
            {
                clsFormInputRtns.bolEditMode = true;
                bolRedisplayForm = true;

                OpenExistingForm("");

                ResizeControls();
            }

        }


        private void mnuToolsCloseEdit_Click(object sender, EventArgs e)
        {

            // close the edit mode
            if (tabForms.TabPages.Count > 0)
            {
                clsFormInputRtns.bolEditMode = false;
                bolRedisplayForm = true;

                OpenExistingForm("");

                ResizeControls();

                ResizeControls();
            }

        }


        private void pnlStatus_Paint(object sender, PaintEventArgs e)
        {

        }



        private void tbrPlexFormsProperties_Click(object sender, EventArgs e)
        {

            if (clsFormInputRtns.bolEditMode)
            {
                GetFormInfo("Change");
            }

        }

        private void mnuToolsSetTabstops_Click(object sender, EventArgs e)
        {

            // bring up the form to set the tabstops for the controls

        }

        private void mnuEditCut_Click(object sender, EventArgs e)
        {

        }

        private void cmnuRightClickCutField_Click(object sender, EventArgs e)
        {

        }

        private void mnuEditCopy_Click(object sender, EventArgs e)
        {

            CopyField();

        }

        private void mnuEditPaste_Click(object sender, EventArgs e)
        {

            PasteField();

        }


        
        
        private void tbrPlexFormsPrint_Click(object sender, EventArgs e)
        {

            mnuFilePrint_Click(sender, e);

        }



        private void tbrPlexFormsPrintPreview_Click(object sender, EventArgs e)
        {

            mnuFilePrintPreview_Click(sender, e);

        }

        
        
        private void mnuFilePrint_Click(object sender, EventArgs e)
        {

            if (objSelectedPallet != null)
            {
                clsPlexPrint.PrintForm(objSelectedPallet, "");
            }

        }



        private void mnuFilePrintPreview_Click(object sender, EventArgs e)
        {

            if (objSelectedPallet != null)
            {
                clsPlexPrint.PrintForm(objSelectedPallet, "PDF995");
            }

        }




        private void mnuFilePrintToFile_Click(object sender, EventArgs e)
        {
            string strFileName = clsCommonRtns.strCurrentPath + "\\" + objSelectedTabPage.Text + ".bmp";

            if (clsPlexPrint.PrintFormToFile(objSelectedPallet, strFileName))
            {
                MessageBox.Show("File " + strFileName + " saved");
            }
            else
            {
                MessageBox.Show("File " + strFileName + " not saved");
            }

        }




        private void mnuFileEmailForm_Click(object sender, EventArgs e)
        {

            EmailForm();

        }

        private void EmailForm()
        {
            // see if a form group is loaded
            if (!clsFormInputRtns.bolFormLoaded)
            {
                return;
            }

            // call the processrules to see if it has an email rule
            string strResult = ""; // clsProcessRules.ProcessRules(tabForms, "CARPETBUSINESS", null, "EMAIL");
            string strFrom = "steve.smith@plexsoft.com";

            // if the values are set (to~subject~message)
            if (strResult.Length > 0)
            {
                clsCommonRtns.Display("Emailing order form...");
                Application.DoEvents();
                //Application.UseWaitCursor = true;

                //split the values
                strResult = strResult + "~~~";
                string[] astrParms = strResult.Split('~');
                string strTo = astrParms[0];
                string strSubject = astrParms[1];
                string strMessage = astrParms[2];
                if (strTo.Length < 1)
                {
                    strTo = "steve.smith@plexsoft.com";
                }
                if (strSubject.Length < 1)
                {
                    strSubject = "Order form";
                }
                if (strMessage.Length < 1)
                {
                    strMessage = "Order form";
                }


                EmailForm frmEmailForm = new EmailForm(tabForms, strFrom);
                frmEmailForm.ShowDialog();

                strFrom = frmEmailForm.strFrom;
                strTo = frmEmailForm.strTo;
                strSubject = frmEmailForm.strSubject;
                strMessage = frmEmailForm.strMessage;

                frmEmailForm.Dispose();

                clsPlexPrint.EmailForm(objSelectedPallet, strTo, strSubject, strMessage);

                clsCommonRtns.Display("");
                Application.UseWaitCursor = false;
                Application.DoEvents();
            }
            else
            {
                // get the information 
                EmailForm frmEmailForm = new EmailForm(tabForms, strFrom);
                frmEmailForm.ShowDialog();
                //clsPlexPrint.EmailForm(objSelectedPallet);
            }

        }




        private void tbrPlexFormsEmail_Click(object sender, EventArgs e)
        {
            // email the current form
            mnuFileEmailForm_Click(sender, e);

        }

        private void mnuToolsUploadForm_Click(object sender, EventArgs e)
        {

            // upload the form
            WebCalls objWebCalls = new WebCalls();

            string strCustId = "";

            string strResponse = objWebCalls.UploadForm("PlexForms", "TestForm", "<field>Test</field>", ref strCustId);

            MessageBox.Show("Return from upload=" + strResponse + ", custid=" + strCustId);


        }




        private void mnuToolsTouch_Click(object sender, EventArgs e)
        {

            if (mnuToolsTouch.Checked)
            {
                clsFormInputRtns.strInputStyle = "TOUCH";
            }
            else
            {
                clsFormInputRtns.strInputStyle = "KEYBOARD";
            }
               
        }




        private void mnuToolsKeyboard_Click(object sender, EventArgs e)
        {

            if (mnuToolsKeyboard.Checked)
            {
                clsFormInputRtns.strInputStyle = "KEYBOARD";
            }
            else
            {
                clsFormInputRtns.strInputStyle = "TOUCH";
            }

        }



    }
}
