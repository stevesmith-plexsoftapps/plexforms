﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class NewForm : Form
    {

        private string strFormGroup = "";
        private string strFormName = "";
        private string strFormImageName = "";

        public string FormGroup
        {
            get { return strFormGroup; }
            set { strFormGroup = value;
                  txtFormGroup.Text = strFormGroup;
                }
        }


        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value;
                  txtFormName.Text = strFormName;
                }
        }


        public string FormImageName
        {
            get { return strFormImageName; }
            set { strFormImageName = value;
                  txtFormImage.Text = strFormImageName;
                }
        }



        public NewForm()
        {
            InitializeComponent();

            strFormGroup = "";
            strFormName = "";
            strFormImageName = "";

        }



        private void btnDone_Click(object sender, EventArgs e)
        {
            
            // set the form name
            strFormGroup = txtFormGroup.Text;
            strFormName = txtFormName.Text;
            strFormImageName = txtFormImage.Text;

            this.Hide();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            // cancel, set no values
            this.Hide();

        }

        private void btnFindFile_Click(object sender, EventArgs e)
        {
            string strSaveLastPath = clsCommonRtns.strLastPath;

            if (clsCommonRtns.strLastImagePath.Length > 0)
            {
                clsCommonRtns.strLastPath = clsCommonRtns.strLastImagePath;
            }

            // first get the file name for the image
            string strImageFileName = clsCommonRtns.GetFile(".*","Picture Files|*.jpg;*.bmp;*.gif","O");

            if (strImageFileName.Length > 0)
            {
                // set the image name
                txtFormImage.Text = strImageFileName;
            }

            clsCommonRtns.strLastImagePath = clsCommonRtns.strLastPath;

            clsCommonRtns.strLastPath = strSaveLastPath;


        }
    }
}
