﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public partial class FieldProperties : Form
    {

        private Control _selectedControl = null;
        public Control SelectedControl
        {
            get { return _selectedControl; }
            set { _selectedControl = value; }
        }

        private Control _selectedForm = null;
        public Control SelectedForm
        {
            get { return _selectedForm; }
            set { _selectedForm = value; }
        }

        private List<clsFormInputRtns.udtRules> _selectedControlRules = new List<clsFormInputRtns.udtRules>();
        public List<clsFormInputRtns.udtRules> SelectedControlRules
        {
            get { return _selectedControlRules; }
            set { _selectedControlRules = value; }
        }



        public FieldProperties()
        {
            InitializeComponent();

            Initialize();

        }


        private void Initialize()
        {

            this.Resize += new EventHandler(FieldProperties_Resize);
            this.Load += new EventHandler(FieldProperties_Load);
       
        }




        /// <summary>
        /// FieldProperties_Load  - 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FieldProperties_Load(object sender, EventArgs e)
        {
            int intRow = 0;

            tabFieldProperties.SelectedIndex = 0;

            // load the fields with the values from the selected control
            if (SelectedControl != null)
            {
                // put in the attributes
                grdAttributes.Rows.Add(30);

                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Form";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedForm.Name;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Name";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Name;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Text";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Text;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Type";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.GetType().Name;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Top";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Top;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Left";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Left;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Width";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Width;

                intRow++;
                grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Height";
                grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = SelectedControl.Height;

                // if it is a grid, show the active cell info and the column header
                string strType = SelectedControl.GetType().Name.ToUpper();
                if (strType == "DATAGRIDVIEW")
                {
                    DataGridView objGrid = (DataGridView)SelectedControl;
                    
                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Column_Name";
                    string strColumnName = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Name;
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnName;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Column_Width";
                    string strColumnWidth = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Width.ToString();
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnWidth;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Column_HeaderText";
                    string strColumnHeaderText = objGrid.Columns[objGrid.CurrentCell.ColumnIndex].HeaderText;
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnHeaderText;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Column_HeaderHeight";
                    string strColumnHeaderHeight = objGrid.ColumnHeadersHeight.ToString();
                    grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnHeaderHeight;

                    intRow++;
                    grdAttributes.Rows[intRow].Cells["Attribute"].Value = "Column_Value";
                    if (objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value != null)
                    {
                        string strColumnValue = objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value.ToString();
                        grdAttributes.Rows[intRow].Cells["AttributeValue"].Value = strColumnValue;
                    }

                }

                // get any rules for this control
                List<string> lstRules = clsFormInputRtns.GetFieldRules(SelectedForm.Text, SelectedControl.Name, "");

                // if any rules are in there for this control, then load them
                txtRules.Text = "";
                for (int m = 0; m < lstRules.Count; m++)
                {
                    txtRules.Text = txtRules.Text + lstRules[m].ToString() + "\r\n";
                }

            }

        }





        void FieldProperties_Resize(object sender, EventArgs e)
        {

            // resize the controls
            tabFieldProperties.Width = this.Width - (tabFieldProperties.Left * 2);
            tabFieldProperties.Height = btnCancel.Top - tabFieldProperties.Top;

            // resize the tab pages
            for (int n = 0; n < tabFieldProperties.TabPages.Count; n++)
            {
                tabFieldProperties.TabPages[n].Width = tabFieldProperties.Width - (tabFieldProperties.TabPages[n].Left * 2);
                tabFieldProperties.TabPages[n].Height = tabFieldProperties.Height - tabFieldProperties.TabPages[n].Top - 30;
            }

            // resize the controls in the tab pages
            txtRules.Width = tabFieldProperties.TabPages["Rules"].Width - (txtRules.Left * 2);
            txtRules.Height = tabFieldProperties.TabPages["Rules"].Height - txtRules.Top - 30;

        }




        private void btnOK_Click(object sender, EventArgs e)
        {
            string strType = SelectedControl.GetType().Name.ToUpper();
            DataGridView objGrid = null;

            // save the values back to the control
            for (int n = 0; n < grdAttributes.Rows.Count; n++)
            {
                if (grdAttributes.Rows[n].Cells["Attribute"].Value != null)
                {
                    string strAttribute = "";
                    string strValue = "";
                    if (grdAttributes.Rows[n].Cells["Attribute"].Value != null)
                    {
                        strAttribute = grdAttributes.Rows[n].Cells["Attribute"].Value.ToString().ToUpper();
                    }
                    if (grdAttributes.Rows[n].Cells["AttributeValue"].Value != null)
                    {
                        strValue = grdAttributes.Rows[n].Cells["AttributeValue"].Value.ToString();
                    }

                    switch (strAttribute)
                    {
                        case "FORM":
                            SelectedForm.Name = strValue;
                            break;

                        case "NAME":
                            SelectedControl.Name = strValue;
                            break;

                        case "TEXT":
                            SelectedControl.Text = strValue;
                            break;

                        case "TOP":
                            SelectedControl.Top = Convert.ToInt32(strValue);
                            break;

                        case "LEFT":
                            SelectedControl.Left = Convert.ToInt32(strValue); ;
                            break;

                        case "WIDTH":
                            SelectedControl.Width = Convert.ToInt32(strValue); ;
                            break;

                        case "HEIGHT":
                            SelectedControl.Height = Convert.ToInt32(strValue); ;
                            break;

                        case "COLUMN_NAME":
                            if (strType == "DATAGRIDVIEW")
                            {
                                objGrid = (DataGridView)SelectedControl;
                                objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Name = strValue;
                            }
                            break;

                        case "COLUMN_WIDTH":
                            if (strType == "DATAGRIDVIEW")
                            {
                                objGrid = (DataGridView)SelectedControl;
                                objGrid.Columns[objGrid.CurrentCell.ColumnIndex].Width = Convert.ToInt32(strValue);
                            }
                            break;

                        case "COLUMN_HEADERTEXT":
                            if (strType == "DATAGRIDVIEW")
                            {
                                objGrid = (DataGridView)SelectedControl;
                                objGrid.Columns[objGrid.CurrentCell.ColumnIndex].HeaderText = strValue;
                            }
                            break;

                        case "COLUMN_HEADERHEIGHT":
                            if (strType == "DATAGRIDVIEW")
                            {
                                objGrid = (DataGridView)SelectedControl;
                                objGrid.ColumnHeadersHeight = Convert.ToInt32(strValue);
                            }
                            break;

                        case "COLUMN_VALUE":
                            if (strType == "DATAGRIDVIEW")
                            {
                                objGrid = (DataGridView)SelectedControl;
                                objGrid.Rows[objGrid.CurrentCell.RowIndex].Cells[objGrid.CurrentCell.ColumnIndex].Value = strValue;
                            }
                            break;

                    }
                }
            }

            // now are there any rules?
            if (txtRules.Text.Trim() != "")
            {
                clsFormInputRtns.SetFieldRules(SelectedForm.Text, SelectedControl.Name, "", txtRules.Text);
            }

            Close();

        }





        private void btnCancel_Click(object sender, EventArgs e)
        {

            Close();

        }

        private void btnEditRules_Click(object sender, EventArgs e)
        {

            EditRules frmEditRules = new EditRules();
            frmEditRules.ShowDialog(this);

        }

    }
}
