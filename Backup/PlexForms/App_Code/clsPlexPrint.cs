﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace PlexForms
{
    public class clsPlexPrint
    {
        public static PrintDialog objPrintwDialog = new PrintDialog();
        private static Bitmap objBitmap = null;
        private static Graphics objGraphic = null;
        private static Size objPortraitSize = new Size(800, 1050);
        private static string strDefaultPrinter = "HP Photosmart C4700 series";


        public static bool PrintForm(PictureBox argobjPallet, string argstrPrinterName)
        {
            PrintDocument objPrintDocument = new PrintDocument();
            objBitmap = clsFormInputRtns.CreateBitmapFromPallet(argobjPallet);
            string strPrinterName = argstrPrinterName;

            if (strPrinterName == "")
            {
                strPrinterName = strDefaultPrinter;
            }

            objPrintDocument.PrintPage += new PrintPageEventHandler(objPrintDocument_PrintPage);

            if (strPrinterName != "")
            {
                // print to the printer
                objPrintDocument.PrinterSettings.PrinterName = strPrinterName;
                objPrintDocument.Print();
            }
            else
            {

                // preview the print - to be added
            }

            return true;
        }



        public static bool PrintFormToFile(PictureBox argobjPallet, string argstrFileName)
        {
            objBitmap = clsFormInputRtns.CreateBitmapFromPallet(argobjPallet);

            // resize the bitmap

            // convert the bitmap to a jpg
            SaveToJpeg(argstrFileName, objBitmap, 100);

            return true;
        }


        public static string EmailForm(PictureBox argobjPallet)
        {
            return EmailForm(argobjPallet,"steve.smith@fuelquest.com","Carpet Order", "Here is your order for carpet");
        }



        public static string EmailForm(PictureBox argobjPallet, string argstrTo, string argstrSubject, string argstrMessage)
        {
            objBitmap = clsFormInputRtns.CreateBitmapFromPallet(argobjPallet);
            objGraphic = Graphics.FromImage(objBitmap);
            string strFileName = "";
            TabPage objTabPage = (TabPage)argobjPallet.Parent;

            strFileName = clsCommonRtns.strCurrentPath + "\\" + objTabPage.Text + ".bmp";

            // convert the bitmap to a jpg
            SaveToJpeg(strFileName, objBitmap, 100);

            // now email the attachment
            string strResponse = clsPlexMail.SendMailWithAttachment(argstrTo, argstrSubject, argstrMessage, strFileName);

            return strResponse;
        }



        private static void objPrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {

            if (objBitmap != null)
            {
                e.Graphics.DrawImage(ResizeImage((Image)objBitmap,objPortraitSize), 0, 0);
            }

        }

        private static Image ResizeImage(Image argobjImage, Size argobjSize)
        {
            Image objNewImage = null;
            int sourceWidth = argobjImage.Width;
            int sourceHeight = argobjImage.Height;
            Size objSize = new Size();

            if (argobjSize != null)
            {
                objSize = argobjSize;
            }
            else
            {
                objSize = objPortraitSize;
            }

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)objSize.Width / (float)sourceWidth);
            nPercentH = ((float)objSize.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(argobjImage, 0, 0, destWidth, destHeight);
            g.Dispose();

            objNewImage = (Image)b;

            return objNewImage;

        }


        private static void SaveToJpeg(string argstrPath, Bitmap argobjImage, long arglngQuality)
        {
           
            try
            {
                //// Encoder parameter for image quality
                //EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, arglngQuality);

                //// Jpeg image codec
                //ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

                //if (jpegCodec == null)
                //    return;

                //EncoderParameters encoderParams = new EncoderParameters(1);
                //encoderParams.Param[0] = qualityParam;

                //argobjImage.Save(argstrPath, jpegCodec, encoderParams);

                argobjImage.Save(argstrPath);
            }
            catch (Exception ex)
            {
                clsCommonRtns.Log(ex.Message);
            }

        }



        private static ImageCodecInfo GetEncoderInfo(string strMimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo codec = null;

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType == strMimeType)
                {
                    codec = codecs[i];
                    break;
                }
            }

            return codec;
        }





    }
}
