﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//*******************************************************************************************************************
//  This DLL is to process the lakewood forms rules
//*******************************************************************************************************************

namespace PlexForms
{
    class clsDLLRules
    {


        public string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";

            // process for the specified form
            switch (argstrForm.ToUpper())
            {
                case "EMS10001":
                case "EMS10001_WHITE":
                case "EMS10001_BLUE":
                case "EMS10001_YELLOW":
                    // lakewood form
                    clsEMS10001 objEMS10001Rules = new clsEMS10001();
                    strResult = objEMS10001Rules.ProcessRules(argtabForms,argstrForm,argobjControl,argstrEvent);
                    break;

                case "STLUKES":
                case "ADV_DIR_QUES":
                case "CONSENT":
                    // st lukes form
                    clsStLukes objSTLUKESRules = new clsStLukes();
                    strResult = objSTLUKESRules.ProcessRules(argtabForms, argstrForm, argobjControl, argstrEvent);
                    break;

                case "CARPETBUSINESS":
                case "CUSTOMER INFORMATION":
                case "SALESORDER":
                case "INVOICE":
                case "PURCHASEORDER":
                    // sales order form
                    clsSalesOrder objSalesOrderRules = new clsSalesOrder();
                    strResult = objSalesOrderRules.ProcessRules(argtabForms, argstrForm, argobjControl, argstrEvent);
                    break;

                case "YMCA":
                case "VISITOR":
                case "ENROLLMENT":
                    // sales order form
                    clsYMCA objYMCA = new clsYMCA();
                    strResult = objYMCA.ProcessRules(argtabForms, argstrForm, argobjControl, argstrEvent);
                    break;

            }

            return strResult;

        }

    }
}
