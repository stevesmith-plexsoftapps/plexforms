﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace PlexForms
{
    public static class clsFormInputRtns
    {
        public static bool bolFormLoaded = false;

        public static string strDBMSHost = "ssmithe64\\sqlexpress";
        public static string strDBMSUID = "dba";
        public static string strDBMSPWD = "dba";
        public static string strDBMSDatabase = "dba";
        public static string strConnection = "";
        public static SqlConnection objConnection = new SqlConnection();

        public static string strFormPath = clsCommonRtns.strFormsPath;
        public static string strFormSource = "F";           // D=database, F=file 
        public static string strTypeOfInput = "TOUCH";      // Touch, Type or Mixed

        public static string strFormGroupName = "";

        public static TabControl tabForms = new TabControl();

        public static List<string> lstRecords = new List<string>();

        public static bool bolEditMode = false;
        public static bool bolEnableDesigner = false;

        // input style can be TOUCH, PEN and KEYBOARD
        public static string strInputStyle = "TOUCH";

        public struct udtRules
        {
            public string strFormName;
            public string strFieldName;
            public string strEvent;
            public List<string> lstRules;
        }
        public static udtRules udtTempRule = new udtRules();

        public static List<udtRules> lstRules = new List<udtRules>();

        public static List<string> lstHeaderProperties = new List<string>();
        public static System.Drawing.Color clrTextBoxColor = System.Drawing.Color.BlanchedAlmond; // .LightBlue;
        public static System.Drawing.Color clrHightlightColor = System.Drawing.Color.Yellow;
        public static System.Drawing.Color clrBackGroundColor = System.Drawing.Color.White;
        public static System.Drawing.Color clrButtonColor = System.Drawing.Color.LightGray;   //.FromName("BlanchedAlmond"); // .BlanchedAlmond;
        
        public static Color objSelectedColor = Color.Yellow;
        public static Color objNonselectedColor = clrTextBoxColor;
        public static string strLoadedFileName = "";
        public static double dblProportion = (8.5 / 11);
        public static int intPicFormHeight = 987;   //Convert.ToInt32(76 * 11);
        public static int intPicFormWidth = 767;   //Convert.ToInt32(76 * 8.5);
        public static int intMaxWidth = 1200;

        public static int intImageWidth = 0;
        public static int intImageHeight = 0;
        public static double dblImageProportion = 0.00;

        #region [Delimited to XML mapping]
        public static string[][] astrDelimitedToXML = {
                        new string[] {"Page_Name","Name"},
                        new string[] {"Image_File","Image"},
                        new string[] {"Form_Id","Id"},
                        new string[] {"Field_Name","Name"},
                        new string[] {"Field_Type","Type"},
                        new string[] {"Data_Type","DataType"},
                        new string[] {"Field_Top","Top"},
                        new string[] {"Field_Left","Left"},
                        new string[] {"Field_Height","Height"},
                        new string[] {"Field_Width","Width"},
                        new string[] {"Field_No","TabIndex"},
                        new string[] {"Font","Font"},
                        new string[] {"Font_Size","FontSize"},
                        new string[] {"Font_Style","FontStyle"}
                      };

        #endregion 



        /// <summary>
        /// LoadFromDatabase - load the passed tab control with the form definitions in the database
        /// </summary>
        /// <param name="argstrFormGroupName"></param>
        /// <param name="argtabForms"></param>
        /// <returns></returns>
        public static int LoadFromDatabase(string argstrFormGroupName, TabControl argtabForms)
        {
            int intRC = 0;

            if (argstrFormGroupName != "")
            {
                // set the current id
                strFormGroupName = argstrFormGroupName;

                // an id was specified

                // open the file
                lstRecords = clsDatabaseRtns.ReadDatabase(strFormGroupName);

                // did we get some?
                if (lstRecords.Count == 0)
                {
                    // nope...
                    return -1;
                }

                // now load it 
                intRC = LoadFromXML(lstRecords, argtabForms);

            }
            else
            {
                // check to see if records have been entered before...
                if (lstRecords.Count == 0)
                {
                    // nope...
                    return -1;
                }
            }

            return intRC;
        }



        /// <summary>
        /// LoadFromFile - load the passed tab control with the form definitions in the XML file 
        /// </summary>
        /// <param name="argstrFormGroupName"></param>
        /// <param name="argtabForms"></param>
        /// <returns></returns>
        public static int LoadFromFile(string argstrFormGroupName, TabControl argtabForms)
        {
            int intRC = 0;

            if (argstrFormGroupName != "")
            {
                // set the current file
                strFormGroupName = argstrFormGroupName;
            }

            int intOffset = strFormGroupName.LastIndexOf('\\');
            if (intOffset > 0)
            {
                strFormPath = strFormGroupName.Substring(0, intOffset);
            }

            if (argstrFormGroupName != "")
            {
                // a file was specified

                // open the file
                lstRecords = clsCommonRtns.ReadFile(strFormGroupName);

                // did we get some?
                if (lstRecords.Count == 0)
                {
                    // nope...
                    return -1;
                }
            }

            if (lstRecords.Count > 0)
            {
                // set the loaded form name
                strLoadedFileName = argstrFormGroupName;

                // now load it 
                intRC = LoadFromXML(lstRecords, argtabForms);

            }
            else
            {
                // nope...
                return -1;
            }

            return intRC;
        }



        /// <summary>
        /// LoadFromXML - load the passed tab control with the form definitions in the XML list
        /// </summary>
        /// <param name="argstrFormGroupName"></param>
        /// <param name="argtabForms"></param>
        /// <returns></returns>
        public static int LoadFromXML(List<string> arglstRecords, TabControl argtabForms)
        {
            int intRC = 0;
            List<string> lstEntities = new List<string>();
            string strComplexType = "";
            string strTag = "";
            string strValue = "";
            PictureBox objCurrentPallet = null;
            Control objCurrentControl = null;
            TabPage objCurrentPage = null;
            string strControlType = "";
            Control objControl = new Control();
            double dblFactor = 0;
            int intColumnRow = -1;
            int intColumnCol = -1;
            string strFormTag = "";
            string strFieldTag = "";
            string strFontTag = "";
            string strFontFamily = "";
            string strFontType = "";
            float fltFontSize = 0.00F;
            string strChecked = "";
            string strRuleEvent = "";
            int intOffset = 0;
            string strList = "";

            // convert the xml to name=value pairs
            lstEntities = ConvertXMLToNameValuePairs(lstRecords);

            // init the rules list
            lstRules.Clear();

            // ok, now load the tab control with the form definition

            tabForms = argtabForms;

            // clear the form
            argtabForms.TabPages.Clear();

            // now read the records and create it...
            foreach (string strEntity in lstEntities)
            {
                strFontFamily = "Microsoft Sans Serif";
                fltFontSize = 12.0F;

                strTag = "";
                // now fill the form tab
                int intIndex = strEntity.IndexOf("=");
                if (intIndex >= 0)
                {
                    // there is a name/value pair
                    strTag = strEntity.Substring(0, intIndex);
                    strValue = strEntity.Substring(intIndex + 1);
                }
                else
                {
                    // this is a complex type
                    strComplexType = strEntity;
                    strValue = "";
                    // process the complextype
                    switch (strComplexType.ToUpper())
                    {
                        case "FORM":
                            // create a new tab
                            TabPage objPage = new TabPage("New Form");
                            argtabForms.TabPages.Add(objPage);
                            objCurrentPage = objPage;
                            PictureBox objPallet = new PictureBox();
                            objPage.Controls.Add(objPallet);
                            // add the picture box to the new tab
                            objPallet.Parent = objPage;

                            // resize it
                            objPallet.Left = 0;
                            objPallet.Top = 10;

                            // reset the form tag
                            strFormTag = "";

                            // determine the relative size from the current pallet...
                            //objPictureSelected.Height = Convert.ToInt32(objPictureSelected.Height * 1.10);
                            //objPictureSelected.Width = Convert.ToInt32(objPictureSelected.Width * 1.10);
                            // if the tabpages width is greater than the max width, reset
                            int intFactorWidth = objPage.Width;
                            if (intFactorWidth > intMaxWidth)
                            {
                                intFactorWidth = intMaxWidth;
                            }
                            // force it to the maxwidth
                            intFactorWidth = intMaxWidth;
                            dblFactor = Convert.ToDouble(intFactorWidth) / Convert.ToDouble(intPicFormWidth);
                            objPallet.Height = Convert.ToInt32(intPicFormHeight * dblFactor);
                            objPallet.Width = Convert.ToInt32(intPicFormWidth * dblFactor);
                            objCurrentPallet = objPallet;
                            objPage.AutoScroll = true;
                            objPage.Select();
                            break;

                        case "FIELD":
                            // save the form tag to the tab control
                            objCurrentPage.Tag = strFormTag;
                            strChecked = "";
                            objControl = new Control();
                            break;

                        case "/FIELD":
                            strTag = "CREATEFIELD";
                            break;

                        case "FONT":
                            break;

                        case "PARMS":
                            strComplexType = "PARMS";
                            break;

                        case "RULES":
                            strComplexType = "RULES";
                            break;

                        case "/RULES":
                            // fill in the rules
                            udtRules udtRule = new udtRules();
                            udtRule.strFormName = udtTempRule.strFormName;
                            udtRule.strFieldName = udtTempRule.strFieldName;
                            udtRule.lstRules = udtTempRule.lstRules;
                            lstRules.Add(udtRule);
                            break;

                        case "RULE":
                            strComplexType = "RULE";
                            break;

                        case "PARM":
                        case "COLUMN":
                            strComplexType = "PARM";
                            intColumnRow = -1;
                            intColumnCol = -1;
                            break;

                        case "/FONT":
                        case "/PARM":
                        case "/RULE":
                            strComplexType = "FIELD";
                            break;

                    }
                }

                if (strTag.Length > 0)
                {
                    // based on the complex type, process the tag
                    switch (strComplexType.ToUpper())
                    {
                        case "FORMS":
                            switch (strTag.ToUpper())
                            {
                                case "GROUP":
                                    argtabForms.Tag = strValue;
                                    break;

                            }
                            break;

                        case "FORM":
                            // add to the tag
                            strFormTag = strFormTag + strTag + "=" + strValue + "~";
                            switch (strTag.ToUpper())
                            {
                                case "IMAGE":
                                    // check the value (filename)
                                    // get the actual image
                                    if (strValue.Substring(0, 1) == ".")
                                    {
                                        // add the path 
                                        strValue = strFormPath + strValue.Substring(1);
                                    }
                                    else
                                    {
                                        intOffset = strValue.LastIndexOf('\\');
                                        if (intOffset > 0)
                                        {
                                            strValue = strValue.Substring(intOffset + 1);
                                            strValue = strFormPath + "\\images\\" + strValue;
                                        }
                                    }

                                    objCurrentPallet.ImageLocation = strValue;
                                    objCurrentPallet.Load(strValue);
                                    objCurrentPallet.SizeMode = PictureBoxSizeMode.StretchImage;
                                    objCurrentPallet.Refresh();
                                    // display the image size 
                                    int intPWidth = objCurrentPallet.Width;
                                    int intPHeight = objCurrentPallet.Height;
                                    break;

                                case "IMAGEWIDTH":
                                    intImageWidth = Convert.ToInt32(strValue);
                                    break;

                                case "IMAGEHEIGHT":
                                    intImageHeight = Convert.ToInt32(strValue);
                                    break;

                                case "NAME":
                                    objCurrentPage.Name = strValue;
                                    objCurrentPage.Text = strValue;
                                    break;

                            }
                            break;

                        case "FIELD":
                            udtTempRule.strFormName = "";
                            udtTempRule.strFieldName = "";
                            udtTempRule.lstRules = new List<string>();
                            double dblValue = 0.00;
                            if (intImageHeight == objCurrentPallet.Height && intImageWidth == objCurrentPallet.Width)
                            {
                                dblFactor = 1.00;
                            }
                            switch (strTag.ToUpper())
                            {
                                case "TYPE":
                                    strControlType = strValue;
                                    break;

                                case "NAME":
                                    objControl.Name = strValue;
                                    break;

                                case "TOP":
                                    dblValue = Convert.ToDouble(strValue);
                                    objControl.Top = Convert.ToInt32(dblValue * dblFactor);
                                    break;

                                case "LEFT":
                                    dblValue = Convert.ToDouble(strValue);
                                    objControl.Left = Convert.ToInt32(dblValue * dblFactor);
                                    break;

                                case "WIDTH":
                                    dblValue = Convert.ToDouble(strValue);
                                    objControl.Width = Convert.ToInt32(dblValue * dblFactor);
                                    break;

                                case "HEIGHT":
                                    dblValue = Convert.ToDouble(strValue);
                                    objControl.Height = Convert.ToInt32(dblValue * dblFactor);
                                    break;

                                case "TABINDEX":
                                    objControl.TabIndex = Convert.ToInt32(strValue);
                                    break;

                                case "VALUE":
                                    objControl.Text = objControl.Text + strValue;
                                    break;

                                case "LIST":
                                    strList = strValue;
                                    break;

                                case "CHECKED":
                                    // this tag is good for checkboxes and radio buttons
                                    switch (strControlType)
                                    {
                                        case "CHECKBOX":
                                        case "RADIOBUTTON":
                                            strChecked = strValue;
                                            break;

                                    }
                                    break;

                                case "FONTNAME":
                                    strFontFamily = strValue;
                                    break;

                                case "FONTSIZE":
                                    fltFontSize = (float)Convert.ToDecimal(strValue);
                                    break;

                                default:
                                    // all other things...
                                    strFieldTag = strFieldTag + strTag + "=" + strValue + "~";
                                    break;

                            }
                            break;

                        case "/FIELD":
                            // set the type
                            Control objNewControl = new Control();
                            switch (strControlType)
                            {
                                case "TEXTBOX":
                                    if (bolEditMode)
                                    {
                                        objNewControl = new Panel();
                                    }
                                    else
                                    {
                                        objNewControl = new TextBox();
                                        TextBox objTextBox = (TextBox)objNewControl;
                                        objTextBox.Multiline = true;
                                        
                                    }
                                    break;

                                case "PANEL":
                                    objNewControl = new Panel();
                                    break;

                                case "BUTTON":
                                    objNewControl = new Button();
                                    break;

                                case "LABEL":
                                    objNewControl = new Label();
                                    break;

                                case "DATAGRIDVIEW":
                                    objNewControl = new DataGridView();
                                    DataGridView objGrid = (DataGridView)objNewControl;
                                    objGrid.MultiSelect = false;
                                    break;

                                case "COMBOBOX":
                                    objNewControl = new ComboBox();
                                    break;

                                case "LISTBOX":
                                    objNewControl = new ListBox();
                                    break;

                                case "RADIOBUTTON":
                                    objNewControl = new RadioButton();
                                    RadioButton objRadioButton = (RadioButton)objNewControl;
                                    objRadioButton.Checked = (strChecked == "TRUE") ? true : false;
                                    break;

                                case "CHECKBOX":
                                    objNewControl = new CheckBox();
                                    CheckBox objCheckBox = (CheckBox)objNewControl;
                                    objCheckBox.Checked = (strChecked == "TRUE") ? true : false;
                                    break;

                                default:
                                    objNewControl = null;
                                    break;
                            }

                            if (objNewControl != null)
                            {
                                // now add to the saved control
                                objNewControl.Name = objControl.Name;
                                string strText = objControl.Text;
                                objNewControl.Text = strText;
                                objNewControl.Tag = strFieldTag;
                                objNewControl.TabIndex = objControl.TabIndex;
                                objNewControl.Height = objControl.Height;
                                objNewControl.Width = objControl.Width;
                                objNewControl.Top = objControl.Top;
                                objNewControl.Left = objControl.Left;
                                if (bolEditMode)
                                {
                                    objNewControl.BackColor = objNonselectedColor;
                                    switch (strControlType)
                                    {
                                        case "BUTTON":
                                            objNewControl.BackColor = clrButtonColor;
                                            break;
                                    }
                                }
                                switch (strControlType)
                                {
                                    case "LISTBOX":
                                        if (strList != "")
                                        {
                                            // set the items
                                            ListBox objListBox = (ListBox)objNewControl;
                                            string[] astrListItems = strList.Split('~');
                                            for (int n = 0; n < astrListItems.Length; n++)
                                            {
                                                string[] astrCodeValuePair = astrListItems[n].Split(';');
                                                if (astrCodeValuePair.Length > 0)
                                                {
                                                    objListBox.Items.Add(astrCodeValuePair[1]);
                                                }
                                            }
                                        }
                                        break;

                                    case "COMBOBOX":
                                        if (strList != "")
                                        {
                                            // set the items
                                            ComboBox objComboBox = (ComboBox)objNewControl;
                                            strList = (strList.Replace("[", "")).Replace("]", "");
                                            string[] astrListItems = strList.Split('~');
                                            for (int n = 0; n < astrListItems.Length; n++)
                                            {
                                                if (astrListItems[n].Trim() != "")
                                                {
                                                    string[] astrCodeValuePair = astrListItems[n].Split(';');
                                                    if (astrCodeValuePair.Length > 0)
                                                    {
                                                        objComboBox.Items.Add(astrCodeValuePair[1]);
                                                    }
                                                }
                                            }
                                        }
                                        break;

                                    default:
                                        objNewControl.BackColor = clrBackGroundColor;
                                        break;
                                   
                                }
                                objNewControl.Font = new Font(strFontFamily, fltFontSize);
                                // add to the pallet
                                objCurrentPallet.Controls.Add(objNewControl);
                                objCurrentControl = objNewControl;
                                objNewControl.Visible = true;

                                // now check to see if it is a combo box.  
                                // look for a file with the code,description values
                                // file name is the field name with the extension .cbo


                            }
                            strFieldTag = "";
                            break;

                        case "FONT":
                            break;


                        case "/FONT":
                            break;

                        case "RULES":
                            // process the type
                            switch (strTag)
                            {
                                case "RULEEVENT":
                                    strRuleEvent = strValue;
                                    break;

                                case "RULE":
                                    // fill in the rules
                                    udtTempRule.strFormName = objCurrentPage.Name;
                                    udtTempRule.strFieldName = objCurrentControl.Name;
                                    udtTempRule.strEvent = strRuleEvent;
                                    udtTempRule.lstRules.Add(strValue);
                                    break;
                            }
                            break;

                        case "PARM":
                            // the parms apply to the previous control
                            if (objCurrentControl != null)
                            {
                                //strControlType = objCurrentControl.GetType().Name.ToUpper();
                                switch (strControlType)
                                {
                                    case "TEXTBOX":
                                        break;

                                    case "LABEL":
                                        break;

                                    case "DATAGRIDVIEW":
                                        DataGridView objGrid = (DataGridView) objCurrentControl;
                                        // get the parm and the value
                                        switch (strTag.ToUpper())
                                        {
                                            case "COLS":
                                                int intCols = Convert.ToInt32(strValue);
                                                objGrid.Rows.Clear();
                                                objGrid.Columns.Clear();
                                                for (int n = 0; n < intCols; n++)
                                                {
                                                    objGrid.Columns.Add("Col" + n, "Col" + n);
                                                }
                                                break;

                                            case "ROWS":
                                                int intRows = Convert.ToInt32(strValue);
                                                objGrid.Rows.Add(intRows);
                                                break;

                                            case "FREEZECOL":
                                                intColumnCol = Convert.ToInt32(strValue);
                                                objGrid.Columns[intColumnCol].Frozen = true;
                                                break;

                                            case "COL":
                                                intColumnCol = Convert.ToInt32(strValue);
                                                if (intColumnCol < objGrid.ColumnCount)
                                                {
                                                    objGrid.Columns[intColumnCol].SortMode = DataGridViewColumnSortMode.NotSortable;
                                                }
                                                break;

                                            case "ROW":
                                                intColumnRow = Convert.ToInt32(strValue);
                                                break;

                                            case "NAME":
                                                if (intColumnCol < objGrid.ColumnCount)
                                                {
                                                    objGrid.Columns[intColumnCol].Name = strValue;
                                                }
                                                break;

                                            case "HEADER":
                                                if (intColumnCol < objGrid.ColumnCount)
                                                {
                                                    objGrid.Columns[intColumnCol].HeaderText = strValue;
                                                }
                                                break;

                                            case "HEADERHEIGHT":
                                                objGrid.ColumnHeadersHeight = Convert.ToInt32(strValue);
                                                objGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                                                break;

                                            case "WIDTH":
                                                if (intColumnCol < objGrid.ColumnCount)
                                                {
                                                    objGrid.Columns[intColumnCol].Width = Convert.ToInt32(strValue);
                                                }
                                                break;

                                            case "HEIGHT":
                                                if (intColumnRow < objGrid.RowCount)
                                                {
                                                    objGrid.Rows[intColumnRow].Height = Convert.ToInt32(strValue);
                                                }
                                                break;

                                            case "TEXT":
                                                if (intColumnRow < objGrid.RowCount && intColumnCol < objGrid.ColumnCount)
                                                {

                                                } objGrid.Rows[intColumnRow].Cells[intColumnCol].Value = strValue;
                                                break;

                                        }
                                        break;

                                    case "COMBOBOX":
                                        break;

                                    case "LISTBOX":
                                        break;

                                    case "RADIOBUTTON":
                                        break;

                                    case "CHECKBOX":
                                        break;

                                    default:
                                        break;
                                }
                            }
                            break;


                    }

                }
            }

            // set the loaded flag
            bolFormLoaded = true;

            return intRC;
        }





        /// <summary>
        /// ConvertXMLToNameValuePairs - convert the xml records to name=value pair records
        /// </summary>
        /// <param name="arglstRecords"></param>
        /// <returns></returns>
        private static List<string> ConvertXMLToNameValuePairs(List<string> arglstRecords)
        {
            List<string> lstXMLRecords = new List<string>();
            string strEntity = "";
            string strTag = "";
            string strValue = "";
            int intStart = 0;
            int intEnd = 0;
            int intLength = 0;

            // walk the list
            foreach (string strRecord in arglstRecords)
            {
                // check for the header...
                if (strRecord.ToUpper().IndexOf("<?XML") >= 0)
                {
                    // skip
                }
                else
                {
                    // check for the start of the tag
                    intStart = strRecord.IndexOf('<');
                    // if not a <, get the next record
                    if (intStart < 0)
                    {
                        continue;
                    }
                    while(intStart < strRecord.Length)
                    {
                        if (intStart >= 0)
                        {
                            intEnd = strRecord.IndexOf('>', intStart);
                            if (intEnd > intStart)
                            {
                                intLength = intEnd - intStart - 1;
                                // get the tag
                                strTag = strRecord.Substring(intStart + 1, intLength).ToUpper();
                                strEntity = strTag;
                                intStart = intEnd;
                                // get the value
                                intEnd = strRecord.IndexOf('<', intStart);
                                if (intEnd > intStart)
                                {
                                    intLength = intEnd - intStart - 1;
                                    strValue = strRecord.Substring(intStart + 1, intLength).ToUpper();
                                    strEntity = strEntity + "=" + strValue;
                                }
                                lstXMLRecords.Add(strEntity);
                                strTag = "";
                                strValue = "";
                                if (intEnd > 0)
                                {
                                    intStart = intEnd;
                                    // now find the end of the record
                                    intEnd = strRecord.IndexOf('>', intStart);
                                    if (intEnd > intStart)
                                    {
                                        intStart = intEnd;
                                    }
                                }
                            }
                            else
                            {
                                intStart++;
                            }
                        }
                    }
                }
            }

            return lstXMLRecords;

        }



        /// <summary>
        /// ConvertDelimitedToNameValuePairs - convert the xml records to name=value pair records
        /// </summary>
        /// <returns></returns>
        public static string ConvertDelimitedToXML(string argstrFormGroupName)
        {
            List<string> lstXMLRecords = new List<string>();
            int intRC = 0;
            string strEntity = "";
            string strTag = "";
            string strValue = "";
            int intStart = 0;
            int intEnd = 0;
            int intLength = 0;
            string[] strHeaderNames = {};
            int intNumHeaderNames = 0;
            string[] strFieldNames = {};
            int intNumFieldNames = 0;
            int intIndent = 0;
            string strSpaces = "                                                                                  ";
            string strIndent = "";
            bool bolParm = false;

            List<string> lstRecords = clsCommonRtns.ReadFile(argstrFormGroupName);

            // walk the list
            foreach (string strRecord in lstRecords)
            {
                if (strRecord.Trim().Length == 0)
                {
                    // done...
                    break;
                }

                // split the record
                string[] strFields = strRecord.Split('|');

                // see if parms are in process, and if they are done...
                if (bolParm)
                {
                    if (strFields[0].ToUpper() != "P")
                    {
                        // output the tag
                        intIndent -= 4;
                        strIndent = strSpaces.Substring(0,intIndent);
                        lstXMLRecords.Add(strIndent + "</Parms>");
                        bolParm = false;
                    }
                }

                switch (strFields[0].ToUpper())
                {
                    case "$$":
                        // fill in the forms fields
                        lstXMLRecords.Add("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                        lstXMLRecords.Add("<Forms>");
                        intIndent = 4;
                        strIndent = strSpaces.Substring(0, intIndent);
                        lstXMLRecords.Add(strIndent + "<Group>" + strFields[1].ToUpper() + "</Group>");
                        break;

                    case "$D":
                        break;

                    case "HL":
                        // if the headernames is filled in, then output an ending form header
                        if (strHeaderNames.Length > 0)
                        {
                            intIndent -= 4;
                            strIndent = strSpaces.Substring(0, intIndent);
                            lstXMLRecords.Add(strIndent + "</Form>");
                        }
                        // create the header fields
                        strHeaderNames = strFields;
                        intNumHeaderNames = 0;
                        // set the field number
                        for (int n = 0; n < strHeaderNames.Length; n++)
                        {
                            if (strHeaderNames[n] == "")
                            {
                                break;
                            }
                            intNumHeaderNames++;
                        }
                        lstXMLRecords.Add(strIndent + "<Form>");
                        intIndent += 4;
                        strIndent = strSpaces.Substring(0, intIndent);
                        break;

                    case "H":
                        // fill in the header values
                        for (int n = 3; n < intNumHeaderNames; n++)
                        {
                            string strHeaderName = GetMappedField(strHeaderNames[n]);
                            lstXMLRecords.Add(strIndent + "<" + strHeaderName + ">" + strFields[n] + "</" + strHeaderName + ">");
                        }
                        break;

                    case "FL":
                        strFieldNames = strFields;
                        intNumFieldNames = 0;
                        // set the field number
                        for (int n = 0; n < strFieldNames.Length; n++)
                        {
                            if (strFieldNames[n] == "")
                            {
                                break;
                            }
                            intNumFieldNames++;
                        }
                        break;

                    case "F":
                        // fill in the field values
                        lstXMLRecords.Add(strIndent + "<Field>");
                        bool bolFont = false;
                        intIndent += 4;
                        strIndent = strSpaces.Substring(0, intIndent);
                        for (int n = 3; n < intNumFieldNames; n++)
                        {
                            string strFieldName = GetMappedField(strFieldNames[n]);
                            if (strFieldName.ToUpper() == "FONT")
                            {
                                lstXMLRecords.Add(strIndent + "<Font>");
                                strFieldName = "FontName";
                                intIndent += 4;
                                strIndent = strSpaces.Substring(0, intIndent);
                                bolFont = true;
                            }
                            if (bolFont)
                            {
                                if (strFieldName.Substring(0, 4).ToUpper() != "FONT")
                                {
                                    bolFont = false;
                                    intIndent -= 4;
                                    strIndent = strSpaces.Substring(0, intIndent);
                                    lstXMLRecords.Add(strIndent + "</Font>");
                                }
                            }
                            lstXMLRecords.Add(strIndent + "<" + strFieldName + ">" + strFields[n] + "</" + strFieldName + ">");
                        }
                        intIndent -= 4;
                        strIndent = strSpaces.Substring(0, intIndent);
                        lstXMLRecords.Add(strIndent + "</Field>");
                        break;

                    case "P":
                        // fill in the field parm values
                        // parms are: Parm|row~column~value
                        string strParm = strFields[4];
                        string[] strParts = strFields[5].Split('~');
                        string strParmRow = strParts[0];
                        string strParmCol = strParts[1];
                        string strParmValue = strParts[2];
                        // output the correct parm
                        if (!bolParm)
                        {
                            // have not already started parm output
                            bolParm = true;
                            // output the tag
                            lstXMLRecords.Add(strIndent + "<Parms>");
                            intIndent += 4;
                            strIndent = strSpaces.Substring(0, intIndent);
                        }
                        lstXMLRecords.Add(strIndent + "<Parm>");

                        switch (strParm.ToUpper())
                        {
                            case "COLS":
                                lstXMLRecords.Add(strIndent + "    <" + strParm + ">" + strParmValue + "</" + strParm + ">");
                                break;

                            case "ROWS":
                                lstXMLRecords.Add(strIndent + "    <" + strParm + ">" + strParmValue + "</" + strParm + ">");
                                break;

                            case "FREEZECOL":
                                lstXMLRecords.Add(strIndent + "    <" + strParm + ">" + strParmValue + "</" + strParm + ">");
                                break;

                            default:
                                // this is a column specific parm
                                strParm = strParm.Substring(7);
                                lstXMLRecords.Add(strIndent + "    <Column>");
                                lstXMLRecords.Add(strIndent + "        <Row>" + strParmRow + "</Row>");
                                lstXMLRecords.Add(strIndent + "        <Col>" + strParmCol + "</Col>");
                                lstXMLRecords.Add(strIndent + "        <" + strParm + ">" + strParmValue + "</" + strParm + ">");
                                lstXMLRecords.Add(strIndent + "    </Column>");
                                break;

                        }
                        lstXMLRecords.Add(strIndent + "</Parm>");
                        break;
                    
                    default:
                        break;

                }
            }

            // last form
            intIndent -= 4;
            strIndent = strSpaces.Substring(0, intIndent);
            lstXMLRecords.Add(strIndent + "</Form>");

            intIndent -= 4;
            strIndent = strSpaces.Substring(0, intIndent);
            lstXMLRecords.Add(strIndent + "</Forms>");

            // now output the xml
            string strXMLFileName = argstrFormGroupName + ".xml";
            clsCommonRtns.WriteFile(strXMLFileName, lstXMLRecords);

            return strXMLFileName;

        }




        public static int SaveToXML(TabControl argtabForms, string argstrNewFileName)
        {
            int intNumberRowsWritten = 0;
            List<string> lstRecords = new List<string>();

            // format
            //  XML header
            //  Form Group
            //      Form
            //          Field
            //              attribute
            //      Form...
            //  XML footer

            // xml header
            lstRecords.Add("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            lstRecords.Add("<Forms>");

            // set the file name if not passed
            if (argstrNewFileName.Length == 0)
            {
            }

            string strFormGroup = argtabForms.Tag.ToString();
            lstRecords.Add("    <Group>" + strFormGroup + "</Group>");

            // walk the tab control and write out the xml
            foreach (TabPage objTabPage in argtabForms.TabPages)
            {
                // each tabpage has a pallet (picturebox) that has controls on it (fields)

                // get the pallet - it is control 0
                PictureBox objPallet = (PictureBox)objTabPage.Controls[0];

                // get the image
                string strImageFileName = objPallet.ImageLocation;

                // add the 
                lstRecords.Add("    <Form>");
                lstRecords.Add("        <Name>" + objTabPage.Text + "</Name>");
                lstRecords.Add("        <Image>" + strImageFileName + "</Image>");
                lstRecords.Add("        <ImageHeight>" + objPallet.Height + "</ImageHeight>");
                lstRecords.Add("        <ImageWidth>" + objPallet.Width + "</ImageWidth>");
                lstRecords.Add("        <Tag>" + objTabPage.Tag + "</Tag>");

                //// the tag has the rest of the properties
                //if (objTabPage.Tag != null)
                //{
                //    string strFormTag = objTabPage.Tag.ToString();
                //    // it is delimited by tilde
                //    string[] astrFormProperties = strFormTag.Split('~');
                //    for (int n = 0; n < astrFormProperties.Length; n++)
                //    {
                //        string[] astrNameValue = astrFormProperties[n].Split('=');
                //        if (astrNameValue.Length > 1)
                //        {
                //            lstRecords.Add("        <" + astrNameValue[0] + ">" + astrNameValue[1] + "</" + astrNameValue[0] + ">");
                //        }
                //    }
                //}

                lstRecords.Add("        <Fields>");

                foreach (Control objControl in objPallet.Controls)
                {
                    lstRecords.Add("            <Field>");
                    lstRecords.Add("                <Name>" + objControl.Name + "</Name>");
                    string strType = objControl.GetType().Name;
                    if (strType.ToUpper() == "PANEL")
                    {
                        strType = "TextBox";
                    }
                    lstRecords.Add("                <Type>" + strType + "</Type>");
                    lstRecords.Add("                <Top>" + objControl.Top + "</Top>");
                    lstRecords.Add("                <Left>" + objControl.Left + "</Left>");
                    lstRecords.Add("                <Width>" + objControl.Width + "</Width>");
                    lstRecords.Add("                <Height>" + objControl.Height + "</Height>");
                    lstRecords.Add("                <Tag>" + objControl.Tag + "</Tag>");
                    lstRecords.Add("                <TabIndex>" + objControl.TabIndex + "</TabIndex>");
                    string strText = objControl.Text;
                    // if a combobox or a listbox, add the items to the value
                    string strList = "";
                    switch (strType.ToUpper())
                    {
                        case "COMBOBOX":
                            ComboBox objComboBox = (ComboBox)objControl;
                            for (int n = 0; n < objComboBox.Items.Count; n++)
                            {
                                strList = strList + "0;" + objComboBox.Items[n].ToString() + "~";
                            }
                            if (strList != "")
                            {
                                lstRecords.Add("                <List>" + strList + "</List>");
                                //strText = strText + "[" + strList + "]";
                            }
                            break;

                        case "LISTBOX":
                            ComboBox objListBox = (ComboBox)objControl;
                            for (int n = 0; n < objListBox.Items.Count; n++)
                            {
                                strList = strList + "0;" + objListBox.Items[n].ToString() + "~";
                            }
                            if (strList != "")
                            {
                                lstRecords.Add("                <List>" + strList + "</List>");
                                //strText = strText + "[" + strList + "]";
                            }
                            break;

                        case "CHECKBOX":
                        case "RADIOBUTTON":
                            CheckBox objCheckBox = (CheckBox)objControl;
                            lstRecords.Add("                <Checked>" + objCheckBox.Checked + "</Checked>");
                            break;
                    }
                    lstRecords.Add("                <Value>" + strText + "</Value>");
                    // the checked tag is for checkboxes and radio buttons
                    lstRecords.Add("                <Font>");
                    lstRecords.Add("                    <FontName>" + objControl.Font.Name + "</FontName>");
                    lstRecords.Add("                    <FontSize>" + objControl.Font.Size + "</FontSize>");
                    lstRecords.Add("                    <FontBold>" + objControl.Font.Bold + "</FontBold>");
                    lstRecords.Add("                    <FontItalic>" + objControl.Font.Italic + "</FontItalic>");
                    lstRecords.Add("                </Font>");

                    //// now output the rest of the non-standard properties
                    //string strFieldTag = objControl.Tag.ToString();
                    //// it is delimited by tilde
                    //string[] astrFieldProperties = strFieldTag.Split('~');
                    //for (int n = 0; n < astrFieldProperties.Length; n++)
                    //{
                    //    string[] astrNameValue = astrFieldProperties[n].Split('=');
                    //    if (astrNameValue.Length > 1)
                    //    {
                    //        lstRecords.Add("                <" + astrNameValue[0] + ">" + astrNameValue[1] + "</" + astrNameValue[0] + ">");
                    //    }
                    //}

                    lstRecords.Add("            </Field>");
                    // if a grid, output the rows and columns
                    switch (strType.ToUpper())
                    {
                        case "DATAGRIDVIEW":
                            DataGridView objGrid = (DataGridView)objControl;
                            lstRecords.Add("            <Parms>");
                            lstRecords.Add("                <Parm>");
                            lstRecords.Add("                    <Cols>" + objGrid.ColumnCount + "</Cols>");
                            lstRecords.Add("                    <Rows>" + objGrid.RowCount + "</Rows>");
                            lstRecords.Add("                    <HeaderHeight>" + objGrid.ColumnHeadersHeight + "</HeaderHeight>");
                            lstRecords.Add("                </Parm>");
                            for (int intCol = 0; intCol < objGrid.ColumnCount; intCol++)
                            {
                                lstRecords.Add("                <Parm>");
                                lstRecords.Add("                    <Col>" + intCol + "</Col>");
                                lstRecords.Add("                    <Name>" + objGrid.Columns[intCol].Name + "</Name>");
                                lstRecords.Add("                    <Width>" + objGrid.Columns[intCol].Width + "</Width>");
                                lstRecords.Add("                    <Header>" + objGrid.Columns[intCol].HeaderText + "</Header>");
                                if (objGrid.Columns[intCol].Frozen)
                                {
                                    lstRecords.Add("                    <FreezeCol>" + intCol + "</FreezeCol>");
                                }
                                lstRecords.Add("                </Parm>");

                                for (int intRow = 0; intRow < objGrid.RowCount; intRow++)
                                {
                                    if (intCol == 0)
                                    {
                                        // set the row height
                                        lstRecords.Add("                <Parm>");
                                        lstRecords.Add("                    <Col>" + intCol + "</Col>");
                                        lstRecords.Add("                    <Row>" + intRow + "</Row>");
                                        lstRecords.Add("                    <Height>" + objGrid.Rows[intRow].Height + "</Height>");
                                        lstRecords.Add("                <Parm>");
                                    }

                                    if (objGrid.Rows[intRow].Cells[intCol].Value != null)
                                    {
                                        if (objGrid.Rows[intRow].Cells[intCol].Value.ToString().Trim() != "")
                                        {
                                            lstRecords.Add("                <Parm>");
                                            lstRecords.Add("                    <Col>" + intCol + "</Col>");
                                            lstRecords.Add("                    <Row>" + intRow + "</Row>");
                                            lstRecords.Add("                    <Text>" + objGrid.Rows[intRow].Cells[intCol].Value.ToString() + "</Text>");
                                            lstRecords.Add("                </Parm>");
                                        }
                                    }

                                }
                            }
                            lstRecords.Add("            </Parms>");
                            break;

                    }

                    // now are there any rules?
                    for (int m = 0; m < lstRules.Count; m++)
                    {
                        // find the corresponding set of rules
                        if (lstRules[m].strFormName.ToUpper() == objTabPage.Name.ToUpper())
                        {
                            // found form, now look for field
                            if (lstRules[m].strFieldName.ToUpper() == objControl.Name.ToUpper())
                            {
                                // found the rules, output them
                                lstRecords.Add("            <Rules>");
                                for (int n = 0; n < lstRules[m].lstRules.Count; n++)
                                {
                                    lstRecords.Add("                <Rule>" + lstRules[m].lstRules[n] + "</Rule>");
                                }
                                lstRecords.Add("            </Rules>");
                            }
                        }
                    }

                }

                lstRecords.Add("        </Fields>");
                lstRecords.Add("    </Form>");

            }

            lstRecords.Add("</Forms>");

            // now write it out
            string strPath = Application.StartupPath + "\\formfiles";

            // see if a file has been specified
            string strFileName = argstrNewFileName;
            if (strFileName == "")
            {
                strFileName = strLoadedFileName;
                if (strFileName == "")
                {
                    strFileName = strFormGroupName;

                    if (strFileName == "")
                    {
                        strFileName = strFormGroup;
                    }
                }
                strFileName = strPath + "\\" + strFormGroup + ".frm";
            }

            clsCommonRtns.WriteFile(strFileName, lstRecords);

            // now reload the form
            LoadFromFile(strFileName, argtabForms);

            return intNumberRowsWritten;
        }



        /// <summary>
        /// GetMappedField - search the mapped array and find the mapped field based on the field name passed
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static string GetMappedField(string argstrFieldName)
        {
            string strMappedField = "";

            for (int i = 0; i < astrDelimitedToXML.Length; i++)
            {
                if (astrDelimitedToXML[i][0].ToUpper().Trim() == argstrFieldName.ToUpper().Trim())
                {
                    strMappedField = astrDelimitedToXML[i][1];
                    break;
                }
            }

            // if found return the field name, otherwise return the field case sensitized
            if (strMappedField == "")
            {
                // case sensitize it
                strMappedField = clsCommonRtns.ConvertCase(argstrFieldName);
            }

            return strMappedField;
        }




        /// <summary>
        /// GetFieldRules - get the rules for the passed field and form
        /// </summary>
        /// <param name="argstrFieldName"></param>
        /// <param name="argstrForm"></param>
        /// <returns></returns>
        public static List<string> GetFieldRules(string argstrForm, string argstrFieldName, string argstrEvent)
        {
            List<string> lstFieldRules = new List<string>();

            for (int n = 0; n < lstRules.Count; n++)
            {
                if (argstrForm.ToUpper() == lstRules[n].strFormName)
                {
                    if (argstrFieldName.ToUpper() == lstRules[n].strFieldName.ToUpper())
                    {
                        lstFieldRules = lstRules[n].lstRules;
                        break;
                    }
                }

            }

            return lstFieldRules;
        }



        /// <summary>
        /// SetFieldRules - set the rules for the passed field and form
        /// </summary>
        /// <param name="argstrFieldName"></param>
        /// <param name="argstrForm"></param>
        /// <returns></returns>
        public static void SetFieldRules(string argstrForm, string argstrFieldName, string argstrEvent, string argstrRules)
        {
            int intIndex = -1;
            List<string> lstFieldRules = new List<string>();

            // find the field
            for (int n = 0; n < lstRules.Count; n++)
            {
                if (argstrForm.ToUpper() == lstRules[n].strFormName)
                {
                    if (argstrFieldName.ToUpper() == lstRules[n].strFieldName.ToUpper())
                    {
                        intIndex = n;
                        break;
                    }
                }

            }

            // find any rules?
            if (intIndex >= 0)
            {
                lstRules[intIndex].lstRules.Clear();
            }
            else
            {
                clsFormInputRtns.udtRules udtRule = new clsFormInputRtns.udtRules();
                udtRule.strFieldName = argstrFieldName;
                udtRule.strFormName = argstrForm;
                List<string> lstTempRules = new List<string>();
                udtRule.lstRules = lstTempRules;
                lstRules.Add(udtRule);
                intIndex = lstRules.Count - 1;
            }

            string[] astrRules = argstrRules.Split('\n');
            for (int m = 0; m < astrRules.Length; m++)
            {
                string strRule = astrRules[m].Trim().ToUpper();

                lstRules[intIndex].lstRules.Add(strRule);

            }

            return;
        }





        /// <summary>
        /// GetField - will return the control on the tabform for the passed form and field name
        /// </summary>
        /// <param name="argstrForm"></param>
        /// <param name="argstrFieldName"></param>
        /// <returns></returns>
        public static Control GetField(string argstrForm, string argstrFieldName)
        {
            Control objField = null;

            // find the tabpage
            for (int n = 0; n < tabForms.TabPages.Count; n++)
            {
                if (tabForms.TabPages[n].Name.ToUpper() == argstrForm.ToUpper())
                {
                    PictureBox objPallet = (PictureBox) tabForms.TabPages[n].Controls[0];

                    // now find the control on the pallet
                    for (int m = 0; m < objPallet.Controls.Count; m++)
                    {
                        if (objPallet.Controls[m].Name.ToUpper() == argstrFieldName.ToUpper())
                        {
                            objField = objPallet.Controls[m];
                            break;
                        }
                    }

                }
            }

            return objField;

        }




        /// <summary>
        /// GetFieldValue - will return the value for the control on the tabform for the passed form and field name
        /// </summary>
        /// <param name="argstrForm"></param>
        /// <param name="argstrFieldName"></param>
        /// <returns></returns>
        public static string GetFieldValue(string argstrForm, string argstrFieldName)
        {
            Control objField = GetField(argstrForm, argstrFieldName);

            if (objField != null)
            {
                return objField.Text;
            }
            else
            {
                return "";
            }

        }




        /// <summary>
        /// SetField - will set the value of the control on the tabform for the passed form, field name and value
        /// </summary>
        /// <param name="argstrForm"></param>
        /// <param name="argstrFieldName"></param>
        /// <param name="argstrValue"></param>
        /// <returns></returns>
        public static bool SetField(string argstrForm, string argstrFieldName, string argstrValue)
        {
            bool bolStatus = false;

            // find the tabpage
            for (int n = 0; n < tabForms.TabPages.Count; n++)
            {
                if (tabForms.TabPages[n].Name.ToUpper() == argstrForm.ToUpper())
                {
                    PictureBox objPallet = (PictureBox)tabForms.TabPages[n].Controls[0];

                    // now find the control on the pallet
                    for (int m = 0; m < objPallet.Controls.Count; m++)
                    {
                        if (objPallet.Controls[m].Name.ToUpper() == argstrFieldName.ToUpper())
                        {
                            string strControlType = objPallet.Controls[m].GetType().Name.ToUpper();
                            if (strControlType == "CHECKBOX")
                            {
                                CheckBox objCheckBox = (CheckBox)objPallet.Controls[m];
                                objCheckBox.Checked = argstrValue == "Y"? true:false;
                            }
                            else
                            {
                                objPallet.Controls[m].Text = argstrValue;
                                bolStatus = true;
                            }
                            break;
                        }
                    }

                }
            }

            return bolStatus;

        }





        private static SqlConnection OpenConnection(string argstrConnectionString)
        {

            SqlConnection objConnection = null;

            try
            {
                objConnection = new SqlConnection(argstrConnectionString);
            }
            catch (SqlException e)
            {
                // error in opening a connection
            }

            return objConnection;

        }


        public static Bitmap CreateBitmapFromPallet(PictureBox argobjPallet)
        {
            Bitmap objBitmap = new Bitmap((Bitmap) argobjPallet.Image);
            Graphics objGraphic = Graphics.FromImage(objBitmap);

            float fltTextTop = 0;
            float fltTextLeft = 0;
            float fltTextWidth = 0;
            float fltTextHeight = 0;
            string strValue = "";
            string strFont = "";
            float fltFontSize = 0;
            float fltFactorX = 1.0F;  // 1.35F;
            float fltFactorY = 1.0F;  // 1.20F;
            Pen objPen = new Pen(Brushes.Black);

            // get the controls
            Control.ControlCollection objControls = argobjPallet.Controls;

            // walk the controls and draw to the image
            for (int n = 0; n < objControls.Count; n++)
            {
                string strType = objControls[n].GetType().Name.ToUpper();

                switch (strType)
                {
                    case "TEXTBOX":
                    case "LABEL":
                        fltTextLeft = objControls[n].Left * fltFactorX;
                        fltTextTop = objControls[n].Top * fltFactorY;
                        fltTextHeight = objControls[n].Height * fltFactorY;
                        fltTextWidth = objControls[n].Width * fltFactorX;
                        fltFontSize = objControls[n].Font.Size;
                        strFont = objControls[n].Font.Name;
                        strValue = objControls[n].Text;  //fltTextLeft + ":" + fltTextTop; // 
                        objGraphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        objGraphic.DrawString(strValue, new Font(strFont, fltFontSize), Brushes.Black, new PointF(fltTextLeft, fltTextTop));
                        objGraphic.DrawRectangle(objPen, fltTextLeft - 2, fltTextTop - 2, fltTextWidth, fltTextHeight);
                        break;

                    case "CHECKBOX":
                        fltTextLeft = objControls[n].Left * fltFactorX;
                        fltTextTop = objControls[n].Top * fltFactorY;
                        fltTextHeight = objControls[n].Height * fltFactorY;
                        fltTextWidth = objControls[n].Width * fltFactorX;
                        fltFontSize = objControls[n].Font.Size;
                        strFont = objControls[n].Font.Name;
                        CheckBox objCheckBox = (CheckBox)objControls[n];
                        if (objCheckBox.Checked)
                        {
                            strValue = "X";
                        }
                        else
                        {
                            strValue = "";
                        }
                        objGraphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        objGraphic.DrawString(strValue, new Font(strFont, fltFontSize), Brushes.Black, new PointF(fltTextLeft, fltTextTop));
                        objGraphic.DrawRectangle(objPen, fltTextLeft - 1, fltTextTop - 2, fltTextWidth, fltTextHeight);
                        break;

                }

            }

            return objBitmap;
        }


    }
}
