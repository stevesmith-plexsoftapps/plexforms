﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlexForms
{
    public class clsSalesOrder
    {

        private TabControl tabForms = null;
        private string strSelectedForm = "";
        private string strEvent = "";
        private Control objSelectedControl = null;
        private TabPage objSelectedTabPage = null;
        private PictureBox objSelectedPallet = null;
        private InputPads objInputPads = new InputPads();

        public string ProcessRules(TabControl argtabForms, string argstrForm, Control argobjControl, string argstrEvent)
        {
            string strResult = "";

            // set the global objects
            tabForms = argtabForms;
            strSelectedForm = argstrForm;
            objSelectedTabPage = tabForms.TabPages[strSelectedForm];
            if (objSelectedTabPage != null)
            {
                objSelectedPallet = (PictureBox)objSelectedTabPage.Controls[0];
            }
            if (argobjControl != null)
            {
                objSelectedControl = argobjControl;
            }
            strEvent = argstrEvent;

            // process for the specified form
            switch (strSelectedForm.ToUpper())
            {
                case "CARPETBUSINESS":
                    // form group level
                    strResult = ProcessCarpetBusiness();
                    break;

                case "CUSTOMER INFORMATION":
                    // Sales order
                    strResult = ProcessCustomerInformation();
                    break;

                case "SALESORDER":
                    // Sales order
                    strResult = ProcessSalesOrder();
                    break;

                case "INVOICE":
                    // Sales order
                    strResult = ProcessInvoice();
                    break;

                case "PURCHASEORDER":
                    // Sales order
                    strResult = ProcessPurchaseOrder();
                    break;

            }

            return strResult;

        }



        /// <summary>
        /// Form level rules
        /// </summary>
        /// <returns></returns>
        private string ProcessCarpetBusiness()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "LOAD":
                    // load the dropdowns
                    int intLoad = 0;
                    break;

                case "GETFOCUS":
                    break;

                case "LOSTFOCUS":
                    break;

                case "ENTER":
                    break;

                case "EMAIL":
                    strResult = ProcessSales_Email();
                    break;
            }

            return strResult;

        }




        /// <summary>
        /// Sales Order page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessSalesOrder()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    strResult = ProcessSalesOrder_GetFocus();
                    break;

                case "LOSTFOCUS":
                    strResult = ProcessSalesOrder_LostFocus();
                    break;

                case "ENTER":
                    break;

                case "EMAIL":
                    strResult = ProcessSales_Email();
                    break;

            }

            return strResult;
        }


        /// <summary>
        /// ProcessSalesOrder_GetFocus - Sales Order page rules when a control gets focus
        /// </summary>
        /// <returns></returns>
        private string ProcessSalesOrder_GetFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // for text controls, set the pad to keypad
            if (strControlType == "TEXTBOX")
            {
                strPad = "KEYPAD";
            }

            // see what control it is
            switch (strControlName)
            {
                case "SEND_EMAIL":
                    //// get the email address
                    //clsCommonRtns.Display("Emailing order, please wait...");
                    //string strTo = objSelectedPallet.Controls["EMAIL_ADDRESS"].Text;
                    //string strSubject = "Carpet Order " + objSelectedPallet.Controls["ORDER_DATE"].Text;
                    //string strMessage = "Here is the order for " + objSelectedPallet.Controls["CUSTOMER_NAME"].Text;
                    //if (clsPlexPrint.EmailForm(objSelectedPallet, strTo, strSubject, strMessage))
                    //{
                    //    clsCommonRtns.Display("Success!");
                    //    MessageBox.Show("Email Sent");

                    //}
                    //else
                    //{
                    //    clsCommonRtns.Display("Sending of email failed!");
                    //    MessageBox.Show("An error occurred in sending email");
                    //}
                    strPad = "";
                    break;

            }

            if (clsFormInputRtns.strInputStyle != "TOUCH")
            {
                strPad = "";
            }

            if (strPad != "")
            {
                strResult = "";
                // call the inputpad routines
                objInputPads.SetInputPad(strPad);
                objInputPads.FieldSelected = objSelectedControl;
                objInputPads.CharPosition = 0;
                objInputPads.ShowDialog();
                strResult = objInputPads.Value;
            }

            return strResult;

        }



        /// <summary>
        /// ProcessSalesOrder_LostFocus - Sales Order page rules when a control loses focus
        /// </summary>
        /// <returns></returns>
        private string ProcessSalesOrder_LostFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = objSelectedControl.Name.ToUpper();
            string strControlType = objSelectedControl.GetType().Name.ToUpper();
            string strControlValue = objSelectedControl.Text.ToUpper();
            string strPad = "";

            // see which control
            // see what control it is
            switch (strControlName)
            {
                case "CUSTOMER_NAME":
                    clsFormInputRtns.SetField("INVOICE", "CUSTOMER_NAME", strControlValue);
                    break;

                case "ADDRESS":
                    clsFormInputRtns.SetField("INVOICE", "ADDRESS", strControlValue);
                    break;

                case "CITY":
                    clsFormInputRtns.SetField("INVOICE", "CITY", strControlValue);
                    break;

                case "STATE":
                    clsFormInputRtns.SetField("INVOICE", "STATE", strControlValue);
                    break;

                case "ZIPCODE":
                    clsFormInputRtns.SetField("INVOICE", "ZIPCODE", strControlValue);
                    break;

                case "EMAIL_ADDRESS":
                    break;

            }

            return strResult;
        }



        /// <summary>
        /// Invoice page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessInvoice()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    break;

                case "LOSTFOCUS":
                    break;

                case "ENTER":
                    break;

                case "EMAIL":
                    //strResult = ProcessSales_Email();
                    break;

            }

            return strResult;
        }



        /// <summary>
        /// ProcessCustomerInformation page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessCustomerInformation()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    strResult = ProcessCustomerInformation_GetFocus();
                    break;

                case "LOSTFOCUS":
                    strResult = ProcessCustomerInformation_LostFocus();
                    break;

                case "ENTER":
                    break;

                case "EMAIL":
                    //strResult = ProcessSales_Email("CUSTOMER INFORMATION");
                    break;

            }

            return strResult;
        }



        /// <summary>
        /// ProcessCustomerInformation_GetFocus - Sales Order page rules when a control gets focus
        /// </summary>
        /// <returns></returns>
        private string ProcessCustomerInformation_GetFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = "";
            string strControlType = "";
            string strControlValue = "";
            if (objSelectedControl != null)
            {
                strControlName = objSelectedControl.Name.ToUpper();
                strControlType = objSelectedControl.GetType().Name.ToUpper();
                strControlValue = objSelectedControl.Text.ToUpper();
            }
            string strPad = "";

            // for text controls, set the pad to keypad
            if (strControlType == "TEXTBOX")
            {
                strPad = "KEYPAD";
            }

            // see what control it is
            switch (strControlName)
            {
                case "SEND_EMAIL":
                    strPad = "";
                    break;

            }

            if (clsFormInputRtns.strInputStyle != "TOUCH")
            {
                strPad = "";
            }

            if (strPad != "")
            {
                strResult = "";
                // call the inputpad routines
                objInputPads.SetInputPad(strPad);
                objInputPads.FieldSelected = objSelectedControl;
                objInputPads.CharPosition = 0;
                objInputPads.ShowDialog();
                strResult = objInputPads.Value;
            }

            return strResult;

        }



        /// <summary>
        /// ProcessCustomerInformation_LostFocus - Sales Order page rules when a control loses focus
        /// </summary>
        /// <returns></returns>
        private string ProcessCustomerInformation_LostFocus()
        {
            string strResult = "";
            Control objControl = new Control();
            string strControlName = "";
            string strControlType = "";
            string strControlValue = "";
            if (objSelectedControl != null)
            {
                strControlName = objSelectedControl.Name.ToUpper();
                strControlType = objSelectedControl.GetType().Name.ToUpper();
                strControlValue = objSelectedControl.Text.ToUpper();
            }
            string strPad = "";

            // see which control
            // see what control it is
            switch (strControlName)
            {
                case "CUSTOMER_NAME":
                    clsFormInputRtns.SetField("INVOICE", "CUSTOMER_NAME", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "CUSTOMER_NAME", strControlValue);
                    break;

                case "ADDRESS":
                    clsFormInputRtns.SetField("INVOICE", "ADDRESS", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "ADDRESS", strControlValue);
                    break;

                case "CITY":
                    clsFormInputRtns.SetField("INVOICE", "CITY", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "CITY", strControlValue);
                    break;

                case "STATE":
                    clsFormInputRtns.SetField("INVOICE", "STATE", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "STATE", strControlValue);
                    break;

                case "ZIPCODE":
                    clsFormInputRtns.SetField("INVOICE", "ZIPCODE", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "ZIPCODE", strControlValue);
                    break;

                case "EMAIL_ADDRESS":
                    //clsFormInputRtns.SetField("INVOICE", "EMAIL_ADDRESS", strControlValue);
                    clsFormInputRtns.SetField("SALESORDER", "EMAIL_ADDRESS", strControlValue);
                    break;

            }

            return strResult;
        }


        /// <summary>
        /// ProcessPurchaseOrder page rules
        /// </summary>
        /// <returns></returns>
        private string ProcessPurchaseOrder()
        {
            string strResult = "";

            switch (strEvent.ToUpper())
            {
                case "GETFOCUS":
                    break;

                case "LOSTFOCUS":
                    break;

                case "ENTER":
                    break;

                case "EMAIL":
                    //strResult = ProcessSales_Email("PURCHASEORDER");
                    break;

            }

            return strResult;
        }




        /// <summary>
        /// Email - email the form 
        /// </summary>
        /// <returns></returns>
        private string ProcessSales_Email()
        {
            string strResult = "SUCCESS";

            // get the email address
            clsCommonRtns.Display("Emailing order, please wait...");
            string strTo = clsFormInputRtns.GetField("CUSTOMER INFORMATION", "EMAIL_ADDRESS").Text;
            string strSubject = "Carpet Order " + clsFormInputRtns.GetField("SALESORDER","ORDER_DATE").Text;
            string strMessage = "Here is the order for " + clsFormInputRtns.GetField("CUSTOMER INFORMATION", "CUSTOMER_NAME").Text;

            // return the to, subject and message
            strResult = strTo + "~" + strSubject + "~" + strMessage;

            return strResult;

        }





    }
}
