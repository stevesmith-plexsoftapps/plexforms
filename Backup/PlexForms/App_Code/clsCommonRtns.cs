using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;


namespace PlexForms
{
    class clsCommonRtns
    {
        public static Version objVersion = new Version(5, 0, 1, 4);
        public static Properties.Settings objSettings = new Properties.Settings();

        public static string strFieldDelimiter = "|";
        public static string strPadFieldDelimiter = "||||||||||||";
        public static char chrFieldDelimiter = '|';
        public static string strRecordDelimiter = "^";
        public static char chrRecordDelimiter = '^';
        public static string strValueDelimiter = "~";
        public static char chrValueDelimiter = '~';
        public static string strFormsPath = "";
        public static Control objStatus = null;

        public struct udtConfigSettings
        {
            public string strKey;
            public string strValue;
            public string strOriginalValue;

        }
        public static List<udtConfigSettings> lstConfigSettings = new List<udtConfigSettings>();
        public static udtConfigSettings objConfigSetting = new udtConfigSettings();
        public static List<string> lstConfigRecords = new List<string>();

        // public form objects
        //public static clsForms objForms = null;         // collection of form objects
        //public static clsForm objSelectedForm;

        public static bool bolLogToSystem = true;
        public static bool bolLogging = true;
        public static String strLoggingFlag = "";
        public static bool bolWriteToLog = true;
        public static String strOutFileName = "";
        public static String strINIFileName = "";
        public static DateTime dteStart = new DateTime();
        public static DateTime dteEnd = new DateTime();
        public static System.Windows.Forms.OpenFileDialog objOpenDialog = new System.Windows.Forms.OpenFileDialog();
        public static System.Windows.Forms.SaveFileDialog objSaveDialog = new System.Windows.Forms.SaveFileDialog();
        public static System.Windows.Forms.FolderBrowserDialog objFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
        public static string strLastPath = "";
        public static string strLastImagePath = "";
        public static string strLastFormPath = "";
        public static string strCurrentPath = "";
        public static string strAppPath = "";

//        public static Properties.Settings objSettings = new Properties.Settings();
        //public static clsDataManager objDM = new clsDataManager();
        //public static clsPlexForms objPlexForm = new clsPlexForms();
        public static string strConnection = "";

        public static string strHost = "";
        public static string strDatabase = "";
        public static string strDBMS = "";
        public static string strUID = "";
        public static string strPWD = "";
        public static string strIntegratedSecurity = "False";

        // list of forms open
        


        //*********************************************
        //* Convert the passed value to a decimal
        //*********************************************
        public static decimal ConvertToDecimal(String argstrValue)
        {
            decimal decValue;

            try
            {
                decValue = Convert.ToDecimal(argstrValue);
            }
            catch (Exception objException)
            {
                decValue = 0;
            }

            return decValue;

        }

        //*********************************************
        //* Convert the passed value to a date
        //*********************************************
        public static DateTime ConvertToDate(String argstrValue)
        {
            DateTime dteValue;

            try
            {
                dteValue = Convert.ToDateTime(argstrValue);
            }
            catch (Exception objException)
            {
                dteValue = DateTime.Today;
            }

            return dteValue;

        }



        //*********************************************
        //* Convert the passed value to a string
        //*********************************************
        public static string ConvertToString(object argobjValue)
        {
            string strValue = "";

            if (argobjValue == null)
            {
                strValue = "";
            }
            else 
            {
                strValue = argobjValue.ToString();
            }

            return strValue;

        }



        //*********************************************
        //* Convert the passed value to a double
        //*********************************************
        public static double ConvertToDouble(String argstrValue)
        {
            double dblValue;

            try
            {
                dblValue = Convert.ToDouble(argstrValue);
            }
            catch (Exception objException)
            {
                dblValue = 0.00;
            }

            return dblValue;

        }

        //*********************************************
        //* Convert the passed value to an int
        //*********************************************
        public static int ConvertToInt(String argstrValue)
        {
            int intValue;

            try
            {
                intValue = Convert.ToInt32(argstrValue);
            }
            catch (Exception objException)
            {
                intValue = 0;
            }

            return intValue;

        }


        // ******************************************************************
        // Indicates whether the supplied date string is a valid date
        //
        // argstrDate: The date to be checked as a string.
        //
        // Return Value: Returns true if valid date else false.
        // ******************************************************************
        public static bool IsDate(String argstrDate)
        {
            DateTime dteDate;

            // check it...
            try
            {
                dteDate = Convert.ToDateTime(argstrDate);
                return true;
            }
            catch (System.Exception objException)
            {
                return false;
            }

        }


        // ******************************************************************
        // Indicates whether the supplied value is numeric
        //
        // argstrValue: The value to be checked
        //
        // Return Value: Returns true if numeric false if not
        // ******************************************************************
        public static bool IsNumeric(String argstrValue)
        {
            Decimal decValue = 0;

            // check it...
            try
            {
                decValue = Convert.ToDecimal(argstrValue);
                return true;
            }
            catch (Exception objException)
            {
                return false;
            }

        }


        //*********************************************
        //* Start the clock
        //*********************************************
        public static void Start()
        {
            dteStart = DateTime.Now;

            Display("Starting at " + dteStart.ToLongTimeString());

            return;
        }


        //*********************************************
        //* Stop the clock and send the elapsed.
        //*********************************************
        public static double Stop()
        {
            double dblTotalSeconds = 0.00;

            dteEnd = DateTime.Now;

            TimeSpan timDuration = dteEnd.Subtract(dteStart);

            dblTotalSeconds = (timDuration.Hours * 3600) + (timDuration.Minutes * 60) + timDuration.Seconds +
                                   (Convert.ToDouble(timDuration.Milliseconds) / 1000);

            Display("Ending at " + dteEnd.ToLongTimeString() + ", elapsed=" + dblTotalSeconds + " secs.");

            return dblTotalSeconds;
        }


        //*********************************************
        //* Display a message to the console.
        //*********************************************
        public static void Display(String argstrMessage)
        {

            if (objStatus != null)
            {
                if (argstrMessage.Length < 1)
                {
                    objStatus.Text = "Ready";
                }
                else
                {
                    objStatus.Text = argstrMessage;
                }
            }

            return;
        }


        //*********************************************
        //* Display a message to the console with no
        //* crlf
        //*********************************************
        public static void DisplayNoCRLF(String argstrMessage)
        {

            if (bolLogging)
            {
                Console.Write(argstrMessage);
            }

            return;
        }


        //*********************************************
        //* Read a line from the console (keyboard)
        //*********************************************
        public static String ReadLine()
        {
            String strValue = "";

            if (bolLogging)
            {
                strValue = Console.ReadLine();
            }

            return strValue;
        }


        //*********************************************
        //* Read a directory and return a list of files
        //* that match the filter
        //*********************************************
        public static String[] ReadFolder(String argstrFolder, String argstrFilter)
        {
            String[] strFiles = {""};

            strFiles = Directory.GetFiles(@argstrFolder, argstrFilter);

            return strFiles;
        }


        //*********************************************
        //* Log to the output file
        //*********************************************
        public static void Log(String argstrMessage)
        {
            System.IO.StreamWriter objStreamWriter;

            if (bolLogging)
            {
                if (bolWriteToLog)
                {
                    // open the output file 
                    try
                    {
                        objStreamWriter = new System.IO.StreamWriter(strOutFileName, true);
                        objStreamWriter.WriteLine(argstrMessage);
                        objStreamWriter.Close();
                    }
                    catch (Exception objException)
                    {
                        Console.WriteLine("Unable to open file " + strOutFileName + ": " + objException.ToString());
                        bolWriteToLog = false;
                        return;
                    }
                }
                else
                {
                    Console.WriteLine(argstrMessage);
                }
            }

            return;
        }


        //*********************************************
        //* Clear the log file
        //*********************************************
        public static void ClearLog()
        {
            System.IO.StreamWriter objStreamWriter;

            // open the output file 
            try
            {
                objStreamWriter = new System.IO.StreamWriter(strOutFileName);
                objStreamWriter.Close();
            }
            catch (Exception objException)
            {
                bolWriteToLog = false;
                return;
            }

            return;
        }

        //*********************************************
        //* Retrieve a value for a passed key form a 
        //* namevalue string
        //*********************************************
        public static String GetValueForKey(String argstrNameValuePairs, String argstrKey)
        {
            String strValue = "";
            int intStart = 0;
            int intEnd = 0;
            int intLength = 0;

            // look for the key...
            intStart = argstrNameValuePairs.ToUpper().IndexOf(argstrKey.ToUpper() + "=");
            if (intStart >= 0)
            {
                intEnd = argstrNameValuePairs.IndexOf("~", intStart);
                if (intEnd > intStart)
                {
                    intLength = intEnd - intStart;
                    strValue = argstrNameValuePairs.Substring(intStart, intLength);
                    // get the value past the =
                    intStart = strValue.IndexOf("=");
                    if (intStart > 0)
                    {
                        strValue = strValue.Substring(intStart + 1);
                    }
                }
            }

            return strValue;
        }





        //*********************************************
        //* Read a file in and return an array of records
        //*********************************************
        public static List<string> ReadFile(String argstrFileName)
        {
            List<string> lstRecords = new List<string>();

            lstRecords = ReadFile(argstrFileName, 0);

            return lstRecords;
        }


        //*********************************************
        //* Read a file in and return an array of records
        //*********************************************
        public static List<string> ReadFile(String argstrFileName,int argintNumberOfLines)
        {
            String strFileName = "";
            List<string> lstRecords = new List<string>();
            int intNumberOfLinesRead = 0;

            strFileName = argstrFileName;

            // clear the array

            // Check to see if the file exists
            if (System.IO.File.Exists(strFileName))
            {
                // Open the file and read the records.
                try
                {
                    using (System.IO.StreamReader objStreamReader = System.IO.File.OpenText(strFileName))
                    {
                        String strBuffer = "";
                        while ((strBuffer = objStreamReader.ReadLine()) != null)
                        {
                            if (strBuffer.Trim().Length > 0)
                            {
                                intNumberOfLinesRead++;
                                lstRecords.Add(strBuffer);
                                if (argintNumberOfLines > 0 && intNumberOfLinesRead >= argintNumberOfLines)
                                {
                                    break;
                                }
                            }

                        }

                        objStreamReader.Close();
                    }
                }
                catch (Exception objException)
                {
                    clsCommonRtns.Log("Error in read of file " + strFileName + ":" + objException.ToString() + "\n");
                    lstRecords.Clear();
                }
            }
            else
            {
                clsCommonRtns.Log("File " + strFileName + " not found");
            }

            return lstRecords;

        }





        //*********************************************
        //* Save a file from an array of records
        //*********************************************
        public static int WriteFile(string argstrFileName, List<string> arglstRecords)
        {
            String strFileName = "";
            bool bolOverWrite = false;
            int intRecordsWritten = 0;

            strFileName = argstrFileName;

            bolOverWrite = true;

            // Check to see if the file exists
            if (System.IO.File.Exists(strFileName))
            {
                // overwrite it?
                bolOverWrite = true;
            }

            if (bolOverWrite)
            {
                // Open the file and write the records.
                try
                {
                    using (System.IO.FileStream objFileStream = System.IO.File.Create(strFileName))
                    {
                        for (int n = 0; n < arglstRecords.Count; n++)
                        {
                            char[] achrChars = arglstRecords[n].ToCharArray();

                            for (int m = 0; m < achrChars.Length; m++)
                            {
                                objFileStream.WriteByte((byte)achrChars[m]);
                            }

                            objFileStream.WriteByte((byte)'\r');
                            objFileStream.WriteByte((byte)'\n');

                            intRecordsWritten++;
                        }

                        objFileStream.Close();
                    }
                }
                catch (Exception objException)
                {
                    clsCommonRtns.Log("Error in write of file " + strFileName + ":" + objException.ToString() + "\n");
                    intRecordsWritten = -1;
                }
            }

            return intRecordsWritten;

        }



        //********************************************************************
        //* Routine:        GetFile
        //* Description:    Allows for the selection of a file
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string GetFile()
        {

            return GetFile("", "", "O");

        }
        
    
        //********************************************************************
        //* Routine:        GetFile
        //* Description:    Allows for the selection of a file
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string GetFile(string argstrExtension, string argstrFilter, string argstrOpenSave)
        {
            string strFileName = "";

            if (argstrOpenSave.Substring(0, 1) == "S")
            {
                if (argstrFilter.Length > 0)
                {
                    objSaveDialog.Filter = argstrFilter;
                }
                else
                {
                    objSaveDialog.Filter = "Form Files (*.frm)|*.frm|All files (*.*)|*.*";
                }
                objSaveDialog.FilterIndex = 1;
                objSaveDialog.DefaultExt = argstrExtension;
                objSaveDialog.FileName = "*.*";
                if (strLastPath.Length == 0)
                {
                    strLastPath = strCurrentPath;
                }
                objSaveDialog.InitialDirectory = strLastPath;
                objSaveDialog.ShowDialog();
                strFileName = objSaveDialog.FileName;
            }
            else
            {
                if (argstrFilter.Length > 0)
                {
                    objOpenDialog.Filter = argstrFilter;
                }
                else
                {
                    objOpenDialog.Filter = "Form Files (*.frm)|*.frm|All files (*.*)|*.*";
                }
                objOpenDialog.FilterIndex = 1;
                objOpenDialog.DefaultExt = argstrExtension;
                objOpenDialog.FileName = "*.*";
                if (strLastPath.Length == 0)
                {
                    strLastPath = strCurrentPath;
                }
                objOpenDialog.InitialDirectory = strLastPath;
                objOpenDialog.ShowDialog();
                strFileName = objOpenDialog.FileName;
                int intOffset = strFileName.LastIndexOf('\\');
                if (intOffset > 0)
                {
                    strLastPath = strFileName.Substring(0, intOffset);
                }
            }

            return strFileName;
        }




        //********************************************************************
        //* Routine:        GetDirectoryPath
        //* Description:    Allows for the selection of a folder
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string GetDirectoryPath()
        {
         
            if (strLastPath.Length == 0)
            {
                strLastPath = strCurrentPath;
            }

            objFolderDialog.SelectedPath = strLastPath;

            objFolderDialog.ShowDialog();

            if (objFolderDialog.SelectedPath != "")
            {
                strLastPath = objFolderDialog.SelectedPath;
                return strLastPath;
            }
            else
            {
                return "";
            }

        }







        //********************************************************************
        //* Routine:        EvaluateExpression
        //* Description:    Evaluates an expression
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string EvaluateExpression(string argstrExpression)
        {
            string strResult = "";

            return strResult;
        }



        //********************************************************************
        //* Routine:        FormatDate
        //* Description:    Formats a date
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string FormatDate(string argstrDate)
        {
            string strResult = "";

            return strResult;
        }


        //********************************************************************
        //* Routine:        FormatTime
        //* Description:    Formats a time
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string FormatTime(string argstrTime)
        {
            string strResult = "";

            return strResult;
        }



        //********************************************************************
        //* Routine:        FormatCurrentTime
        //* Description:    Formats the current time
        //* Arguments:      None
        //* Returns:        None
        //********************************************************************
        public static string FormatCurrentTime()
        {
            string strResult = "";
            DateTime dteCurrentTime = DateTime.Now;

            strResult = dteCurrentTime.Hour.ToString().PadLeft(2, '0') + ":" + 
                        dteCurrentTime.Minute.ToString().PadLeft(2,'0') + ":" +
                        dteCurrentTime.Second.ToString().PadLeft(2, '0') + ":" +
                        dteCurrentTime.Millisecond.ToString().PadLeft(3, '0');
           
            return strResult;
        }



        //*********************************************
        //* Convert the passed value to a cased value
        //*********************************************
        public static string ConvertCase(String argstrValue)
        {
            string strValue = "";
            bool bolUpper = true;

            for (int n = 0; n < argstrValue.Length; n++)
            {
                string strChar = argstrValue.Substring(n, 1).ToLower();
                if (strChar == "_")
                {
                    // set upper
                    bolUpper = true;
                }
                else
                {
                    if (bolUpper)
                    {
                        strChar = strChar.ToUpper();
                        bolUpper = false;
                    }
                    strValue = strValue + strChar;
                }

            }

            return strValue;

        }




        public static List<string> GetDirectoryFiles(string argstrPath)
        {
            List<string> lstFiles = GetDirectoryFiles(argstrPath, "*.*");

            return lstFiles;
        }



        public static List<string> GetDirectoryFiles(string argstrPath, string argstrFilter)
        {
            List<string> lstFiles = new List<string>();

            lstFiles.AddRange(Directory.GetFiles(argstrPath, argstrFilter));

            return lstFiles;
        }



        public static List<string> GetAllDirectoryFiles(string argstrPath)
        {
            List<string> lstFiles = new List<string>();
            Stack<string> lstStack = new Stack<string>();

            // Add initial directory.
            lstStack.Push(argstrPath);

            // Continue while there are directories to process
            while (lstStack.Count > 0)
            {
                // Get the top directory
                string strDirectory = lstStack.Pop();

                try
                {
                    // Add all files at this directory to the result List.
                    lstFiles.AddRange(Directory.GetFiles(strDirectory, "*.*"));

                    // Add all directories at this directory.
                    foreach (string strSubDirectory in Directory.GetDirectories(strDirectory))
                    {
                        lstStack.Push(strSubDirectory);
                    }
                }
                catch
                {
                    // Could not open the directory
                }
            }
            return lstFiles;
        }


        //*********************************************
        //* Read the config file in and load the 
        //* configuration records
        //*********************************************
        public static int ReadConfigFile()
        {
            String strFileName = "";
            int intNumberOfLinesRead = 0;
            string strKey = "";
            string strValue = "";
            bool bolUserSettings = false;
            bool bolSetting = false;
            int intOffset = 0;
            int intOffset2 = 0;
            string strBuffer = "";

            strCurrentPath = Application.ExecutablePath;
            // remove the exe name
            intOffset = strCurrentPath.LastIndexOf("\\");
            if (intOffset >= 0)
            {
                strCurrentPath = strCurrentPath.Substring(0, intOffset);
            }

            strFileName = strCurrentPath + "\\plexforms.config";

            // clear the array
            lstConfigSettings.Clear();

            lstConfigRecords = ReadFile(strFileName);

            for (int n = 0; n < lstConfigRecords.Count; n++)
            {
                strBuffer = lstConfigRecords[n];

                if (strBuffer.Trim().Length > 0)
                {
                    intNumberOfLinesRead++;
                    if (strBuffer.ToUpper().IndexOf("<USERSETTINGS") >= 0)
                    {
                        bolUserSettings = true;
                    }
                    if (strBuffer.ToUpper().IndexOf("</USERSETTINGS") >= 0)
                    {
                        bolUserSettings = false;
                    }
                    if (bolUserSettings)
                    {
                        if (strBuffer.ToUpper().IndexOf("<SETTING") >= 0)
                        {
                            //  get the key
                            bolSetting = true;
                            intOffset = strBuffer.ToUpper().IndexOf("NAME=");
                            if (intOffset > 0)
                            {
                                intOffset += 5;
                                intOffset2 = strBuffer.ToUpper().IndexOf(" ", intOffset);
                                if (intOffset2 > 0)
                                {
                                    strKey = strBuffer.Substring(intOffset, (intOffset2 - intOffset));
                                    // now remove any quotes
                                    strKey = strKey.Replace("\"","");
                                }
                            }
                        }
                        if (strBuffer.ToUpper().IndexOf("</SETTING") >= 0)
                        {
                            bolSetting = false;
                            // add to the config list
                            udtConfigSettings objConfigSetting = new udtConfigSettings();
                            objConfigSetting.strKey = strKey;
                            objConfigSetting.strValue = strValue;
                            objConfigSetting.strOriginalValue = strValue;
                            lstConfigSettings.Add(objConfigSetting);
                            strKey = "";
                            strValue = "";
                        }
                        if (strBuffer.ToUpper().IndexOf("<VALUE") >= 0)
                        {
                            intOffset = strBuffer.IndexOf(">") + 1;
                            intOffset2 = strBuffer.IndexOf("<", intOffset);
                            strValue = strBuffer.Substring(intOffset, (intOffset2 - intOffset));
                        }
                    }
                }
            }

            return lstConfigSettings.Count;

        }


        //*********************************************
        //* Find the config key and return the value
        //*********************************************
        public static string GetSetting(string argstrKey)
        {
            String strValue = "";

            for (int n=0;n<lstConfigSettings.Count;n++)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[n];

                if (objConfigSetting.strKey.ToUpper().Equals(argstrKey.ToUpper()))
                {
                    strValue = objConfigSetting.strValue;
                    break;
                }
            }

            return strValue;
        }




        //*********************************************
        //* Find the config key and set the value to the passed value
        //*********************************************
        public static string SetSetting(string argstrKey, string argstrValue)
        {
            String strValue = "";

            for (int n = 0; n < lstConfigSettings.Count; n++)
            {
                udtConfigSettings objConfigSetting = lstConfigSettings[n];

                if (objConfigSetting.strKey.ToUpper().Equals(argstrKey.ToUpper()))
                {
                    objConfigSetting.strValue = argstrValue;
                    lstConfigSettings[n] = objConfigSetting;
                    break;
                }
            }

            return strValue;
        }



        //*********************************************
        //* Write the config file out
        //*********************************************
        public static int WriteConfigFile()
        {
            String strFileName = "";
            int intOffset = 0;
            List<string> lstRecords = new List<string>();
            string strBuffer = "";
            bool bolUserSection = false;
            bool bolSettings = false;
            bool bolSettingsWritten = false;

            strCurrentPath = Application.ExecutablePath;
            // remove the exe name
            intOffset = strCurrentPath.LastIndexOf("\\");
            if (intOffset >= 0)
            {
                strCurrentPath = strCurrentPath.Substring(0, intOffset);
            }

            strFileName = strCurrentPath + "\\plexforms.config";

            lstRecords.Clear();

            // walk the config records and replace the values
            for (int n = 0; n < lstConfigRecords.Count; n++)
            {
                // go until the usersettings section is found
                strBuffer = lstConfigRecords[n];
                if (strBuffer.ToUpper().IndexOf("<USERSETTINGS") >= 0)
                {
                    bolUserSection = true;
                }

                if (strBuffer.ToUpper().IndexOf("</USERSETTINGS") >= 0)
                {
                    bolUserSection = false;
                }

                if (bolUserSection)
                {
                    // output the usersettings tag
                    if (bolSettingsWritten)
                    {
                        // done
                    }
                    else
                    {
                        lstRecords.Add(strBuffer);
                        for (int m = 0; m < lstConfigSettings.Count; m++)
                        {
                            lstRecords.Add("<setting name=\"" + lstConfigSettings[m].strKey + "\" serializeAs=\"String\">");
                            lstRecords.Add("    <value>" + lstConfigSettings[m].strValue + "</value>");
                            lstRecords.Add("</setting>");
                        }
                        bolSettingsWritten = true;
                    }
                }
                else
                {
                    lstRecords.Add(strBuffer);
                }

            }

            // now write out the new file
            WriteFile(strFileName, lstRecords);

            return lstConfigSettings.Count;

        }



        public static string GetEXEName()
        {
            string strEXEName = "";

            // return the EXE name of the running application
            strEXEName = Application.ExecutablePath;

            int intOffset = strEXEName.LastIndexOf('\\');
            if (intOffset >= 0)
            {
                strEXEName = strEXEName.Substring(intOffset + 1);
            }

            // remove the .exe
            strEXEName = strEXEName.ToUpper().Replace(".EXE", "");

            return strEXEName;

        }


    }
}
